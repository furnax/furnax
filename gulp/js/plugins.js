/* 
 *
 * ZEPTO PLUGINS USED BY FURNAX
 * COMPILE WITH: http://refresh-sf.com/yui/
 *
 * File is searchable with "Furnax Plugin 01" etc.
 *
 *
 * Plugins list:
 *  - Furnax Plugin 01 - CORE: forked from zepto detect, used to identify devices
 *  - Furnax Plugin 02 - CORE: forked from zepto touch. edited to help fast clicks and scroll lock for iOS 7
 *  - Furnax Plugin 03 - OPTIONAL, but needed if there is any input field: scrollTo used to smooth scroll movements.
 *  - Furnax Plugin 04 - OPTIONAL: geolocator function to easily create maps and reverse geocoding info. with an extra lib for geolocation marker (usage: var GeoMarker = new GeolocationMarker(singlemap)
 *  - Furnax Plugin 05 - OPTIONAL: split time in chunks, can be used to avoid need for dropdowns
 *  - Furnax Plugin 06 - OPTIONAL, but needed for pickadate: makes zepto compatible with jquery data
 *  - Furnax Plugin 07 - OPTIONAL: forked from pickdate.js is a date and time picker
 *  - Furnax Plugin 08 - OPTIONAL: listjs to generate sort, filter, lists of items
 *  - Furnax Plugin 09 - OPTIONAL: furnax drag and drop component, cross browser (even WP)
 */
/* jshint undef: false, unused: false, browser: true, jquery: true, devel: true, asi: true */
/* global zepto */
//     Furnax Plugin 01
/**
 * Detect
 * https://github.com/madrobby/zepto/blob/master/src/detect.js#files
 */
;(function($) {
  function detect(ua) {
    var os = this.os = {},
      browser = this.browser = {},
      webkit = ua.match(/Web[kK]it[\/]{0,1}([\d.]+)/),
      android = ua.match(/(Android);?[\s\/]+([\d.]+)?/),
      ipad = ua.match(/(iPad).*OS\s([\d_]+)/),
      ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/),
      iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/),
      webos = ua.match(/(webOS|hpwOS)[\s\/]([\d.]+)/),
      touchpad = webos && ua.match(/TouchPad/),
      kindle = ua.match(/Kindle\/([\d.]+)/),
      silk = ua.match(/Silk\/([\d._]+)/),
      blackberry = ua.match(/(BlackBerry).*Version\/([\d.]+)/),
      bb10 = ua.match(/(BB10).*Version\/([\d.]+)/),
      rimtabletos = ua.match(/(RIM\sTablet\sOS)\s([\d.]+)/),
      playbook = ua.match(/PlayBook/),
      chrome = ua.match(/Chrome\/([\d.]+)/) || ua.match(/CriOS\/([\d.]+)/),
      firefox = ua.match(/Firefox\/([\d.]+)/),
      ie = ua.match(/MSIE\s([\d.]+)/) || ua.match(/Trident\/[\d](?=[^\?]+).*rv:([0-9.].)/),
      /* MSIE detect needs to be updated */
      safari = webkit && ua.match(/Mobile\//) && !chrome,
      webview = ua.match(/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/) && !chrome;

    if (browser.webkit === true) browser.version = webkit[1];

    if (android) {
      os.android = true;
      os.version = android[2];
    }
    if (iphone && !ipod) {
      os.ios = os.iphone = true;
      os.version = iphone[2].replace(/_/g, '.');
    }
    if (ipad) {
      os.ios = os.ipad = true;
      os.version = ipad[2].replace(/_/g, '.');
    }
    if (ipod) {
      os.ios = os.ipod = true;
      os.version = ipod[3] ? ipod[3].replace(/_/g, '.') : null;
    }
    if (webos) {
      os.webos = true;
      os.version = webos[2];
    }
    if (touchpad) os.touchpad = true;
    if (blackberry) {
      os.blackberry = true;
      os.version = blackberry[2];
    }
    if (bb10) {
      os.bb10 = true;
      os.version = bb10[2];
    }
    if (rimtabletos) {
      os.rimtabletos = true;
      os.version = rimtabletos[2];
    }
    if (playbook) {
      browser.playbook = true;
    }
    if (kindle) {
      os.kindle = true;
      os.version = kindle[1];
    }
    if (silk) {
      browser.silk = true;
      browser.version = silk[1];
    }
    if (!silk && os.android && ua.match(/Kindle Fire/)) {
      browser.silk = true;
    }
    if (chrome) {
      browser.chrome = true;
      browser.version = chrome[1];
    }
    if (firefox) {
      browser.firefox = true;
      browser.version = firefox[1];
    }
    if (safari && (ua.match(/Safari/) || !!os.ios)) {
      browser.safari = true;
    }
    if (webview) {
      browser.webview = true;
    }

    os.tablet = !!(ipad || playbook || (android && !ua.match(/Mobile/)) ||
      (firefox && ua.match(/Tablet/)) || (ie && !ua.match(/Phone/) && ua.match(/Touch/)));
    os.phone = !!(!os.tablet && !os.ipod && (android || iphone || webos || blackberry || bb10 ||
      (chrome && ua.match(/Android/)) || (chrome && ua.match(/CriOS\/([\d.]+)/)) ||
      (firefox && ua.match(/Mobile/)) || (ie && ua.match(/Touch/))));
  }

  detect.call($, navigator.userAgent);

})(window.jQuery || window.Zepto, window, document);


//     Furnax Plugin 02

//     Zepto.js TOUCHES
//     (c) 2010-2014 Thomas Fuchs
//     Zepto.js may be freely distributed under the MIT license.

;(function($){
  var touch = {},
    touchTimeout, swipeTimeout, longTapTimeout,
    longTapDelay = 750,
    gesture

  function swipeDirection(x1, x2, y1, y2) {
    return Math.abs(x1 - x2) >=
      Math.abs(y1 - y2) ? (x1 - x2 > 0 ? 'Left' : 'Right') : (y1 - y2 > 0 ? 'Up' : 'Down')
  }

  function longTap() {
    longTapTimeout = null
    if (touch.last) {
      touch.el.trigger('longTap')
      touch = {}
    }
  }

  function cancelLongTap() {
    if (longTapTimeout) clearTimeout(longTapTimeout)
    longTapTimeout = null
  }

  function cancelAll() {
    if (touchTimeout) clearTimeout(touchTimeout)
    if (swipeTimeout) clearTimeout(swipeTimeout)
    if (longTapTimeout) clearTimeout(longTapTimeout)
    touchTimeout = swipeTimeout = longTapTimeout = null
    touch = {}
  }

  function isPrimaryTouch(event){
    return (event.pointerType == 'touch' ||
      event.pointerType == event.MSPOINTER_TYPE_TOUCH)
      && event.isPrimary
  }

  function isPointerEventType(e, type){
    return (e.type == 'pointer'+type ||
      e.type.toLowerCase() == 'mspointer'+type)
  }

  $(document).ready(function(){
    var now, delta, deltaX = 0, deltaY = 0, firstTouch, _isPointerType

    if ('MSGesture' in window) {
      gesture = new MSGesture()
      gesture.target = document.body
    }

    $(document)
      .bind('MSGestureEnd', function(e){
        var swipeDirectionFromVelocity =
          e.velocityX > 1 ? 'Right' : e.velocityX < -1 ? 'Left' : e.velocityY > 1 ? 'Down' : e.velocityY < -1 ? 'Up' : null;
        if (swipeDirectionFromVelocity) {
          touch.el.trigger('swipe')
          touch.el.trigger('swipe'+ swipeDirectionFromVelocity)
        }
      })
      .on('touchstart MSPointerDown pointerdown', function(e){
        if((_isPointerType = isPointerEventType(e, 'down')) &&
          !isPrimaryTouch(e)) return
        firstTouch = _isPointerType ? e : e.touches[0]
        if (e.touches && e.touches.length === 1 && touch.x2) {
          // Clear out touch movement data if we have it sticking around
          // This can occur if touchcancel doesn't fire due to preventDefault, etc.
          touch.x2 = undefined
          touch.y2 = undefined
        }
        now = Date.now()
        delta = now - (touch.last || now)
        touch.el = $('tagName' in firstTouch.target ?
          firstTouch.target : firstTouch.target.parentNode)
        touchTimeout && clearTimeout(touchTimeout)
        touch.x1 = firstTouch.pageX
        touch.y1 = firstTouch.pageY
        if (delta > 0 && delta <= 250) touch.isDoubleTap = true
        touch.last = now
        longTapTimeout = setTimeout(longTap, longTapDelay)
        // adds the current touch contact for IE gesture recognition
        if (gesture && _isPointerType) gesture.addPointer(e.pointerId);

        /*
         *      FURNAX EDIT 1/2: try to avoid iOs7 BOUNCE EFFECT that screws with views scroll positions
         */

        furnax.scrollContext = $(firstTouch.target).closest(".view, #navbar, #tabbar, .scrollable, .picker__wrap, .sidemenu #tabbar")[0];

        if (furnax.scrollContext) {

          furnax.scrollContext.allowUp = (furnax.scrollContext.scrollTop > 0);
          furnax.scrollContext.allowDown = (furnax.scrollContext.scrollTop < furnax.scrollContext.scrollHeight - furnax.scrollContext.clientHeight);
          furnax.scrollContext.prevTop = furnax.scrollContext.scrollTop;
          furnax.scrollContext.prevBot = null;
          furnax.scrollContext.lastY = e.pageY;

        } else {

          furnax.scrollContext = false;

        }

        // windows phone will still overscroll. adding a   -ms-touch-action: none; property to the touch target helps
        //        $(firstTouch.target).addClass("nodrag");


        /*
         *      END OF EDIT 1/2
         */


      })
      .on('touchmove MSPointerMove pointermove', function(e){
        if((_isPointerType = isPointerEventType(e, 'move')) && !isPrimaryTouch(e)) return
        firstTouch = _isPointerType ? e : e.touches[0]
        cancelLongTap()
        touch.x2 = firstTouch.pageX
        touch.y2 = firstTouch.pageY

        deltaX += Math.abs(touch.x1 - touch.x2)
        deltaY += Math.abs(touch.y1 - touch.y2)

        /*
         *      FURNAX EDIT 2/2: try to avoid iOs7 BOUNCE EFFECT that screws with views scroll positions
         */



      // prevent body scrolling
        if ($.os.ios && furnax.scrollContext && !$(firstTouch.target).hasClass("needsclick") && !$(firstTouch.target).hasClass("scrollable") && !$(firstTouch.target).hasClass("draggable") && $(firstTouch.target).attr("type") !== "checkbox" && !$.os.android) {




          var up = (e.pageY > furnax.scrollContext.lastY),
            down = !up;

          furnax.scrollContext.lastY = e.pageY;

          if (furnax.scrollContext && (((up && furnax.scrollContext.allowUp) || (down && furnax.scrollContext.allowDown)) && !$("body").hasClass("with-keyboard"))) {
            // will scroll
            e.stopPropagation();
            cancelAll();
            
          } else if ( (up && !furnax.scrollContext.allowUp) || (down && !furnax.scrollContext.allowDown) ||  firstTouch.target === document.body) {
          
            // will not scroll
            e.preventDefault();

            // however, we emit event for other to know
            
            if (!furnax.scrollContext.notifyOverscroll) {
              // fire event
              $(furnax.scrollContext).trigger("overscroll", up ? "up" : "down");              
              furnax.scrollContext.notifyOverscroll = false;
            }
                        
            // re-enable only after a while, to avoid too many
            
            setTimeout( function() {
              furnax.scrollContext.notifyOverscroll = true;
            }, 500);

          }


        }

        /*
         *      END OF EDIT 2/2
         */
      })
      .on('touchend MSPointerUp pointerup', function(e){
        if((_isPointerType = isPointerEventType(e, 'up')) &&
          !isPrimaryTouch(e)) return
        cancelLongTap()

        // swipe
        if ((touch.x2 && Math.abs(touch.x1 - touch.x2) > 30) ||
            (touch.y2 && Math.abs(touch.y1 - touch.y2) > 30)) {

          swipeTimeout = setTimeout(function() {
            touch.el && touch.el.trigger('swipe')
            touch.el && touch.el.trigger('swipe' + (swipeDirection(touch.x1, touch.x2, touch.y1, touch.y2)))
            touch = {}
          }, 0)

        // normal tap
        } else if ('last' in touch) {
          // don't fire tap when delta position changed by more than 30 pixels,
          // for instance when moving to a point and back to origin
          if (deltaX < 30 && deltaY < 30) {
            // trigger universal 'tap' with the option to cancelTouch()
            // (cancelTouch cancels processing of single vs double taps for faster 'tap' response)
            var event = $.Event('tap')
            event.cancelTouch = cancelAll
            touch.el && touch.el.trigger(event)

            // trigger double tap immediately
            if (touch.isDoubleTap) {
              if (touch.el) touch.el.trigger('doubleTap')
              touch = {}
            }

            // trigger single tap after 250ms of inactivity
//            else {
//              touchTimeout = setTimeout(function(){
//                touchTimeout = null
//                if (touch.el) touch.el.trigger('singleTap')
//                touch = {}
//              }, 250)
//            }
          } else {
            touch = {}
          }
        }
        deltaX = deltaY = 0
      })
      // when the browser window loses focus,
      // for example when a modal dialog is shown,
      // cancel all ongoing events
      .on('touchcancel MSPointerCancel pointercancel', cancelAll)

    // scrolling the window indicates intention of the user
    // to scroll, not tap or swipe, so cancel all ongoing events
    $(window).on('scroll', cancelAll)
  })

  ;['swipe', 'swipeLeft', 'swipeRight', 'swipeUp', 'swipeDown',
    'doubleTap', 'tap', 'singleTap', 'longTap'].forEach(function(eventName){
    $.fn[eventName] = function(callback){ return this.on(eventName, callback) }
  })
})(Zepto);


//     Furnax Plugin 03

/**
 * scrollTo
 * https://github.com/suprMax/ZeptoScroll
 */
(function($) {
  var DEFAULTS = {
    endY: $.os.android ? 1 : 0,
    duration: 200,
    updateRate: 15
  };

  var interpolate = function(source, target, shift) {
    return (source + (target - source) * shift);
  };

  var easing = function(pos) {
    return (-Math.cos(pos * Math.PI) / 2) + 0.5;
  };

  var scroll = function(settings) {
    var options = $.extend({}, DEFAULTS, settings);

    if (options.duration === 0) {
      window.scrollTo(0, options.endY);
      if (typeof options.callback === 'function') options.callback();
      return;
    }

    var startY = window.pageYOffset,
      startT = Date.now(),
      finishT = startT + options.duration;

    var animate = function() {
      var now = Date.now(),
        shift = (now > finishT) ? 1 : (now - startT) / options.duration;

      window.scrollTo(0, interpolate(startY, options.endY, easing(shift)));

      if (now < finishT) {
        setTimeout(animate, options.updateRate);
      } else {
        if (typeof options.callback === 'function') options.callback();
      }
    };

    animate();
  };

  var scrollNode = function(settings) {
    var options = $.extend({}, DEFAULTS, settings);

    if (options.duration === 0) {
      this.scrollTop = options.endY;
      if (typeof options.callback === 'function') options.callback();
      return;
    }

    var startY = this.scrollTop,
      startT = Date.now(),
      finishT = startT + options.duration,
      _this = this;

    var animate = function() {
      var now = Date.now(),
        shift = (now > finishT) ? 1 : (now - startT) / options.duration;

      _this.scrollTop = interpolate(startY, options.endY, easing(shift));

      if (now < finishT) {
        setTimeout(animate, options.updateRate);
      } else {
        if (typeof options.callback === 'function') options.callback();
      }
    };

    animate();
  };

  $.scrollTo = scroll;

  $.fn.scrollTo = function() {
    if (this.length) {
      var args = arguments;
      this.forEach(function(elem, index) {
        scrollNode.apply(elem, args);
      });
    }
  };
}(Zepto));


//     Furnax Plugin 04

/**
 * GeoLocator
 * https://github.com/onury/geolocator
 */

var geolocator = (function() {

  'use strict';

  /*-------- PRIVATE PROPERTIES & FIELDS --------*/

  /* Storage for the callback function to be executed when the location is successfully fetched. */
  var onSuccess,
    /* Storage for the callback function to be executed when the location could not be fetched due to an error. */
    onError,
    /* HTML element ID for the Google Maps. */
    mCanvasId,
    /* Google Maps URL. */
    googleLoaderURL = 'https://www.google.com/jsapi',
    /* Array of source services that provide location-by-IP information. */
    ipGeoSources = [

      {
        url: 'http://freegeoip.net/json/',
        cbParam: 'callback'
      }, // 0
      {
        url: 'http://www.geoplugin.net/json.gp',
        cbParam: 'jsoncallback'
      }, // 1
      {
        url: 'http://geoiplookup.wikimedia.org/',
        cbParam: ''
      } // 2
    ],
    /* The index of the current IP source service. */
    ipGeoSourceIndex = 0; // default (freegeoip)

  /*-------- PRIVATE METHODS --------*/

  /*  Non-blocking method for loading scripts dynamically.
   */
  function loadScript(url, callback, type) {
    var script = document.createElement('script');
    script.type = (type === undefined) ? 'text/javascript' : type;

    if (typeof callback === 'function') {
      if (script.readyState) {
        script.onreadystatechange = function() {
          if (script.readyState === 'loaded' || script.readyState === 'complete') {
            script.onreadystatechange = null;
            callback();
          }
        };
      } else {
        script.onload = function() {
          callback();
        };
      }
    }

    script.src = url;
    //document.body.appendChild(script);
    document.getElementsByTagName('head')[0].appendChild(script);
  }

  /*  Loads Google Maps API and executes the callback function when done.
   */
  function loadGoogleMaps(callback) {
    function loadMaps() {
      if (geolocator.__glcb) {
        delete geolocator.__glcb;
      }
      google.load('maps', '3', {
        other_params: 'sensor=false',
        callback: callback
      });
    }
    if (window.google !== undefined && google.maps !== undefined) {
      if (callback) {
        callback();
      }
    } else {
      if (window.google !== undefined && google.loader !== undefined) {
        loadMaps();
      } else {
        geolocator.__glcb = loadMaps;
        loadScript(googleLoaderURL + '?callback=geolocator.__glcb');
      }
    }
  }

  /*  Draws the map from the fetched geo information.
   */
  function drawMap(elemId, mapOptions, infoContent) {
    var map, marker, infowindow,
      elem = document.getElementById(elemId);
    if (elem) {
      map = new google.maps.Map(elem, mapOptions);
      marker = new google.maps.Marker({
        position: mapOptions.center,
        map: map
      });
      infowindow = new google.maps.InfoWindow();
      infowindow.setContent(infoContent);
      //infowindow.open(map, marker);
      google.maps.event.addListener(marker, 'tap', function() {
        infowindow.open(map, marker);
      });
      geolocator.location.map = {
        canvas: elem,
        map: map,
        options: mapOptions,
        marker: marker,
        infoWindow: infowindow
      };
    } else {
      geolocator.location.map = null;
    }
  }

  /*  Runs a reverse-geo lookup for the specified lat-lon coords.
   */
  function reverseGeoLookup(latlng, callback) {
    var geocoder = new google.maps.Geocoder();

    function onReverseGeo(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        if (callback) {
          callback(results);
        }
      }
    }
    geocoder.geocode({
      'latLng': latlng
    }, onReverseGeo);
  }

  /*  Fetches additional details (from the reverse-geo result) for the address property of the location object.
   */
  function fetchDetailsFromLookup(data) {
    if (data.length > 0) {
      var i, comp, comps = data[0].address_components;
      geolocator.location.formattedAddress = data[0].formatted_address;

      for (i = 0; i < comps.length; i += 1) {
        comp = comps[i];
        if (comp.types.indexOf('route') >= 0) {
          geolocator.location.address.street = comp.long_name;
        } else if (comp.types.indexOf('neighborhood') >= 0) {
          geolocator.location.address.neighborhood = comp.long_name;
        } else if (comp.types.indexOf('sublocality') >= 0) {
          geolocator.location.address.town = comp.long_name;
        } else if (comp.types.indexOf('locality') >= 0) {
          geolocator.location.address.city = comp.long_name;
        } else if (comp.types.indexOf('administrative_area_level_1') >= 0) {
          geolocator.location.address.region = comp.long_name;
        } else if (comp.types.indexOf('country') >= 0) {
          geolocator.location.address.country = comp.long_name;
          geolocator.location.address.countryCode = comp.short_name;
        } else if (comp.types.indexOf('postal_code') >= 0) {
          geolocator.location.address.postalCode = comp.long_name;
        } else if (comp.types.indexOf('street_number') >= 0) {
          geolocator.location.address.streetNumber = comp.long_name;
        }
      }
    }
  }

  /*  Finalizes the location object via reverse-geocoding and draws the map (if required).
   */
  function finalize(coords) {
    var latlng = new google.maps.LatLng(coords.latitude, coords.longitude);

    function onGeoLookup(data) {
      if (data.length > 0) {
        fetchDetailsFromLookup(data);
      }

      var zoom = geolocator.location.ipGeoSource === null ? 14 : 7, //zoom out if we got the lcoation from IP.
        mapOptions = {
          zoom: zoom,
          center: latlng,
          mapTypeId: 'roadmap'
        };
      drawMap(mCanvasId, mapOptions, data[0].formatted_address);
      if (onSuccess) {
        onSuccess.call(null, geolocator.location);
      }
    }
    reverseGeoLookup(latlng, onGeoLookup);
  }

  /*  Gets the geo-position via HTML5 geolocation (if supported).
   */
  function getPosition(fallbackToIP, html5Options) {
    geolocator.location = null;

    function fallback(errMsg) {
      var ipsIndex = fallbackToIP === true ? 0 : (typeof fallbackToIP === 'number' ? fallbackToIP : -1);
      if (ipsIndex >= 0) {
        geolocator.locateByIP(onSuccess, onError, ipsIndex, mCanvasId);
      } else {
        if (onError) {
          onError.call(null, errMsg);
        }
      }
    }

    function geoSuccess(position) {
      geolocator.location = {
        ipGeoSource: null,
        coords: position.coords,
        timestamp: (new Date()).getTime(), //overwrite timestamp (Safari-Mac and iOS devices use different epoch; so better use this).
        address: {}
      };
      finalize(geolocator.location.coords);
    }

    function geoError(error) {
      // switch (error.code) {
      // case error.PERMISSION_DENIED:
      //     break;
      // case error.POSITION_UNAVAILABLE:
      //     break;
      // case error.TIMEOUT:
      //     break;
      // case error.UNKNOWN_ERROR:
      //     break;
      // }
      fallback(error.message);
    }

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, html5Options);
    } else { // not supported
      fallback('geolocation is not supported.');
    }
  }

  /*  Builds the location object from the source data.
   */
  function buildLocation(sourceIndex, data) {
    switch (sourceIndex) {
      case 0: // freegeoip
        geolocator.location = {
          coords: {
            latitude: data.latitude,
            longitude: data.longitude
          },
          address: {
            city: data.city,
            country: data.country_name,
            countryCode: data.country_code,
            region: data.region_name
          }
        };
        break;
      case 1: // geoplugin
        geolocator.location = {
          coords: {
            latitude: data.geoplugin_latitude,
            longitude: data.geoplugin_longitude
          },
          address: {
            city: data.geoplugin_city,
            country: data.geoplugin_countryName,
            countryCode: data.geoplugin_countryCode,
            region: data.geoplugin_regionName
          }
        };
        break;
      case 2: // Wikimedia
        geolocator.location = {
          coords: {
            latitude: data.lat,
            longitude: data.lon
          },
          address: {
            city: data.city,
            country: '',
            countryCode: data.country,
            region: ''
          }
        };
        break;
    }
    if (geolocator.location) {
      geolocator.location.coords.accuracy = null;
      geolocator.location.coords.altitude = null;
      geolocator.location.coords.altitudeAccuracy = null;
      geolocator.location.coords.heading = null;
      geolocator.location.coords.speed = null;
      geolocator.location.timestamp = new Date().getTime();
      geolocator.location.ipGeoSource = ipGeoSources[sourceIndex];
      geolocator.location.ipGeoSource.data = data;
    }
  }

  /*  The callback that is executed when the location data is fetched from the source.
   */
  function onGeoSourceCallback(data) {
    var initialized = false;
    geolocator.location = null;
    delete geolocator.__ipscb;

    function gLoadCallback() {
      if (ipGeoSourceIndex === 2) { // Wikimedia
        if (window.Geo !== undefined) {
          buildLocation(ipGeoSourceIndex, window.Geo);
          delete window.Geo;
          initialized = true;
        }
      } else {
        if (data !== undefined) {
          buildLocation(ipGeoSourceIndex, data);
          initialized = true;
        }
      }

      if (initialized === true) {
        finalize(geolocator.location.coords);
      } else {
        if (onError) {
          onError('Could not get location.');
        }
      }
    }

    loadGoogleMaps(gLoadCallback);
  }

  /*  Loads the (jsonp) source. If the source does not support json-callbacks;
   *  the callback is executed dynamically when the source is loaded completely.
   */
  function loadIpGeoSource(source) {
    if (source.cbParam === undefined || source.cbParam === null || source.cbParam === '') {
      loadScript(source.url, onGeoSourceCallback);
    } else {
      loadScript(source.url + '?' + source.cbParam + '=geolocator.__ipscb'); //ip source callback
    }
  }

  return {

    /*-------- PUBLIC PROPERTIES --------*/

    /* The recent location information fetched as an object.
     */
    location: null,

    /*-------- PUBLIC METHODS --------*/

    /* Gets the geo-location by requesting user permission.
     */
    locate: function(successCallback, errorCallback, fallbackToIP, html5Options, mapCanvasId) {
      onSuccess = successCallback;
      onError = errorCallback;
      mCanvasId = mapCanvasId;

      function gLoadCallback() {
        getPosition(fallbackToIP, html5Options);
        loadPosMarkerCode();
      }
      loadGoogleMaps(gLoadCallback);
    },
    /* Gets the geo-location from the user IP.
     */
    locateByIP: function(successCallback, errorCallback, sourceIndex, mapCanvasId) {
      ipGeoSourceIndex = (sourceIndex === undefined ||
        (sourceIndex < 0 || sourceIndex >= ipGeoSources.length)) ? 0 : sourceIndex;
      onSuccess = successCallback;
      onError = errorCallback;
      mCanvasId = mapCanvasId;
      geolocator.__ipscb = onGeoSourceCallback;
      loadIpGeoSource(ipGeoSources[ipGeoSourceIndex]);
    }
  };
}());


/* extra lib for geolocation marker */

function loadPosMarkerCode() {
  (function() {
    var b = !0,
      d = null,
      e;

    function g(a, c, f) {
      var h = {
        clickable: !1,
        cursor: "pointer",
        draggable: !1,
        flat: b,
        icon: {
          url: "https://google-maps-utility-library-v3.googlecode.com/svn/trunk/geolocationmarker/images/gpsloc.png",
          size: new google.maps.Size(34, 34),
          scaledSize: new google.maps.Size(17, 17),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(8, 8)
        },
        optimized: !1,
        position: new google.maps.LatLng(0, 0),
        title: "Current location",
        zIndex: 2
      };
      c && (h = k(h, c));
      c = {
        clickable: !1,
        radius: 0,
        strokeColor: "1bb6ff",
        strokeOpacity: 0.4,
        fillColor: "61a0bf",
        fillOpacity: 0.4,
        strokeWeight: 1,
        zIndex: 1
      };
      f && (c = k(c, f));
      this.b = new google.maps.Marker(h);
      this.a = new google.maps.Circle(c);
      this.map = this.position = this.accuracy = d;
      this.set("minimum_accuracy", d);
      this.set("position_options", {
        enableHighAccuracy: b,
        maximumAge: 1E3
      });
      this.a.bindTo("map", this.b);
      a && this.c(a)
    }
    g.prototype = new google.maps.MVCObject;
    g.prototype.set = function(a, c) {
      if (/^(?:position|accuracy)$/i.test(a)) throw "'" + a + "' is a read-only property.";
      /map/i.test(a) ? this.c(c) : google.maps.MVCObject.prototype.set.apply(this, arguments)
    };
    e = g.prototype;
    e.b = d;
    e.a = d;
    e.h = function() {
      return this.map
    };
    e.f = function() {
      return this.get("position_options")
    };
    e.n = function(a) {
      this.set("position_options", a)
    };
    e.i = function() {
      return this.position
    };
    e.g = function() {
      return this.position ? this.a.getBounds() : d
    };
    e.j = function() {
      return this.accuracy
    };
    e.d = function() {
      return this.get("minimum_accuracy")
    };
    e.m = function(a) {
      this.set("minimum_accuracy", a)
    };
    e.e = -1;
    e.c = function(a) {
      this.map = a;
      this.notify("map");
      a ? l(this) : (this.b.unbind("position"), this.a.unbind("center"), this.a.unbind("radius"), this.position = this.accuracy = d, navigator.geolocation.clearWatch(this.e), this.e = -1, this.b.setMap(a))
    };
    e.l = function(a) {
      this.b.setOptions(k({}, a))
    };
    e.k = function(a) {
      this.a.setOptions(k({}, a))
    };

    function l(a) {
      navigator.geolocation && (a.e = navigator.geolocation.watchPosition(function(c) {
        a: {
          var f = new google.maps.LatLng(c.coords.latitude, c.coords.longitude),
            h = a.b.getMap() == d;
          if (h) {
            if (a.d() != d && c.coords.accuracy > a.d()) break a;
            a.b.setMap(a.map);
            a.b.bindTo("position", a);
            a.a.bindTo("center", a, "position");
            a.a.bindTo("radius", a, "accuracy")
          }
          a.accuracy != c.coords.accuracy && google.maps.MVCObject.prototype.set.call(a, "accuracy", c.coords.accuracy);
          (h || a.position == d || !a.position.equals(f)) && google.maps.MVCObject.prototype.set.call(a,
            "position", f)
        }
      }, function(c) {
        google.maps.event.trigger(a, "geolocation_error", c)
      }, a.f()))
    }

    function k(a, c) {
      for (var f in c) m[f] !== b && (a[f] = c[f]);
      return a
    }
    var m = {
      map: b,
      position: b,
      radius: b
    };
    g.prototype.getAccuracy = g.prototype.j;
    g.prototype.getBounds = g.prototype.g;
    g.prototype.getMap = g.prototype.h;
    g.prototype.getMinimumAccuracy = g.prototype.d;
    g.prototype.getPosition = g.prototype.i;
    g.prototype.getPositionOptions = g.prototype.f;
    g.prototype.setCircleOptions = g.prototype.k;
    g.prototype.setMap = g.prototype.c;
    g.prototype.setMarkerOptions = g.prototype.l;
    g.prototype.setMinimumAccuracy = g.prototype.m;
    g.prototype.setPositionOptions = g.prototype.n;
    window.GeolocationMarker = g;
  })()
}


//     Furnax Plugin 05

//     Zepto.js Data
//     (c) 2010-2014 Thomas Fuchs
//     Zepto.js may be freely distributed under the MIT license.

// The following code is heavily inspired by jQuery's $.fn.data()

;
(function($) {
  var data = {},
    dataAttr = $.fn.data,
    camelize = $.camelCase,
    exp = $.expando = 'Zepto' + (+new Date()),
    emptyArray = []

  // Get value from node:
  // 1. first try key as given,
  // 2. then try camelized key,
  // 3. fall back to reading "data-*" attribute.
  function getData(node, name) {
    var id = node[exp],
      store = id && data[id]
    if (name === undefined) return store || setData(node)
    else {
      if (store) {
        if (name in store) return store[name]
        var camelName = camelize(name)
        if (camelName in store) return store[camelName]
      }
      return dataAttr.call($(node), name)
    }
  }

  // Store value under camelized key on node
  function setData(node, name, value) {
    var id = node[exp] || (node[exp] = ++$.uuid),
      store = data[id] || (data[id] = attributeData(node))
    if (name !== undefined) store[camelize(name)] = value
    return store
  }

  // Read all "data-*" attributes from a node
  function attributeData(node) {
    var store = {}
    $.each(node.attributes || emptyArray, function(i, attr) {
      if (attr.name.indexOf('data-') == 0)
        store[camelize(attr.name.replace('data-', ''))] =
        $.zepto.deserializeValue(attr.value)
    })
    return store
  }

  $.fn.data = function(name, value) {
    return value === undefined ?
      // set multiple values via object
      $.isPlainObject(name) ?
      this.each(function(i, node) {
        $.each(name, function(key, value) {
          setData(node, key, value)
        })
      }) :
      // get value from first element
      this.length == 0 ? undefined : getData(this[0], name) :
      // set value on all elements
      this.each(function() {
        setData(this, name, value)
      })
  }

  $.fn.removeData = function(names) {
    if (typeof names == 'string') names = names.split(/\s+/)
    return this.each(function() {
      var id = this[exp],
        store = id && data[id]
      if (store) $.each(names || store, function(key) {
        delete store[names ? camelize(this) : key]
      })
    })
  }

  // Generate extended `remove` and `empty` functions
  ;
  ['remove', 'empty'].forEach(function(methodName) {
    var origFn = $.fn[methodName]
    $.fn[methodName] = function() {
      var elements = this.find('*')
      if (methodName === 'remove') elements = elements.add(this)
      elements.removeData()
      return origFn.call(this)
    }
  })
})(window.jQuery || window.Zepto, window, document);


//     Furnax Plugin 06


/**
 * Split time in chunks function
 **/

function makeTimeIntervals(startTime, endTime, increment) {
  startTime = startTime.toString().split(':');
  endTime = endTime.toString().split(':');
  increment = parseInt(increment, 10);

  var pad = function(n) {
      return (n < 10) ? '0' + n.toString() : n;
    },
    startHr = parseInt(startTime[0], 10),
    startMin = parseInt(startTime[1], 10),
    endHr = parseInt(endTime[0], 10),
    endMin = parseInt(endTime[1], 10),
    currentHr = startHr,
    currentMin = startMin,
    previous = currentHr + ':' + pad(currentMin),
    current = '',
    r = [];

  do {
    currentMin += increment;
    if ((currentMin % 60) === 0 || currentMin > 60) {
      currentMin = (currentMin === 60) ? 0 : currentMin - 60;
      currentHr += 1;
    }
    current = currentHr + ':' + pad(currentMin);
    r.push(previous + ' - ' + current);
    previous = current;
  } while (currentHr * 60 + currentMin <= endHr * 60 + endMin - increment);

  return r;
}




//     Furnax Plugin 07

/*!
 * pickadate.js v3.4.0, 2014/02/15
 * By Amsul, http://amsul.ca
 * Hosted on http://amsul.github.io/pickadate.js
 * Licensed under MIT
 */

(function(factory) {

  // Register as an anonymous module.
  if (typeof define === 'function' && define.amd)
    define('picker', ['Zepto'], factory)

  // Or using browser globals.
  else this.Picker = factory(Zepto)

}(function($) {

  var $document = $(document)


  /**
   * The picker constructor that creates a blank picker.
   */
  function PickerConstructor(ELEMENT, NAME, COMPONENT, OPTIONS) {

      // If there's no element, return the picker constructor.
      if (!ELEMENT) return PickerConstructor


      var
      // The state of the picker.
        STATE = {
          id: ELEMENT.id || 'P' + Math.abs(~~(Math.random() * new Date()))
        },


        // Merge the defaults and options passed.
        SETTINGS = COMPONENT ? $.extend(true, {}, COMPONENT.defaults, OPTIONS) : OPTIONS || {},


        // Merge the default classes with the settings classes.
        CLASSES = $.extend({}, PickerConstructor.klasses(), SETTINGS.klass),


        // The element node wrapper into a Zepto object.
        $ELEMENT = $(ELEMENT),


        // Pseudo picker constructor.
        PickerInstance = function() {
          return this.start()
        },


        // The picker prototype.
        P = PickerInstance.prototype = {

          constructor: PickerInstance,

          $node: $ELEMENT,


          /**
           * Initialize everything
           */
          start: function() {

            // If it's already started, do nothing.
            if (STATE && STATE.start) return P


            // Update the picker states.
            STATE.methods = {}
            STATE.start = true
            STATE.open = false
            STATE.type = ELEMENT.type


            // Confirm focus state, convert into text input to remove UA stylings,
            // and set as readonly to prevent keyboard popup.
            ELEMENT.autofocus = ELEMENT == document.activeElement
            ELEMENT.type = 'text'
            ELEMENT.readOnly = !SETTINGS.editable
            ELEMENT.id = ELEMENT.id || STATE.id


            // Create a new picker component with the settings.
            P.component = new COMPONENT(P, SETTINGS)


            // Create the picker root with a holder and then prepare it.
            P.$root = $(PickerConstructor._.node('div', createWrappedComponent(), CLASSES.picker, 'id="' + ELEMENT.id + '_root"'))
            prepareElementRoot()


            // If there's a format for the hidden input element, create the element.
            if (SETTINGS.formatSubmit) {
              prepareElementHidden()
            }


            // Prepare the input element.
            prepareElement()


            // Insert the root as specified in the settings.
            if (SETTINGS.container) $(SETTINGS.container).append(P.$root)
            else $ELEMENT.after(P.$root)


            // Bind the default component and settings events.
            P.on({
              start: P.component.onStart,
              render: P.component.onRender,
              stop: P.component.onStop,
              open: P.component.onOpen,
              close: P.component.onClose,
              set: P.component.onSet
            }).on({
              start: SETTINGS.onStart,
              render: SETTINGS.onRender,
              stop: SETTINGS.onStop,
              open: SETTINGS.onOpen,
              close: SETTINGS.onClose,
              set: SETTINGS.onSet
            })


            // If the element has autofocus, open the picker.
            if (ELEMENT.autofocus) {
              P.open()
            }


            // Trigger queued the "start" and "render" events.
            return P.trigger('start').trigger('render')
          }, //start


          /**
           * Render a new picker
           */
          render: function(entireComponent) {

            // Insert a new component holder in the root or box.
            if (entireComponent) P.$root.html(createWrappedComponent())
            else P.$root.find('.' + CLASSES.box).html(P.component.nodes(STATE.open))

            // Trigger the queued "render" events.
            return P.trigger('render')
          }, //render


          /**
           * Destroy everything
           */
          stop: function() {

            // If it's already stopped, do nothing.
            if (!STATE.start) return P

            // Then close the picker.
            P.close()

            // Remove the hidden field.
            if (P._hidden) {
              P._hidden.parentNode.removeChild(P._hidden)
            }

            // Remove the root.
            P.$root.remove()

            // Remove the input class, remove the stored data, and unbind
            // the events (after a tick for IE - see `P.close`).
            $ELEMENT.removeClass(CLASSES.input).removedata(NAME)
            setTimeout(function() {
              $ELEMENT.off('.' + STATE.id)
            }, 0)

            // Restore the element state
            ELEMENT.type = STATE.type
            ELEMENT.readOnly = false

            // Trigger the queued "stop" events.
            P.trigger('stop')

            // Reset the picker states.
            STATE.methods = {}
            STATE.start = false

            return P
          }, //stop


          /*
           * Open up the picker
           */
          open: function(dontGiveFocus) {

            // If it's already open, do nothing.
            if (STATE.open) return P

            // Add the "active" class.
            $ELEMENT.addClass(CLASSES.active)
            aria(ELEMENT, 'expanded', true)

            // Add the "opened" class to the picker root.
            P.$root.addClass(CLASSES.opened)
              /*
               * furnax edit 01
               */
            $("#content").addClass("no-touches")
            $("html").addClass("picker-visible")
              /*
               * end furnax edit 01
               */
            aria(P.$root[0], 'hidden', false)

            // If we have to give focus, bind the element and doc events.
            if (dontGiveFocus !== false) {

              // Set it as open.
              STATE.open = true

              // Pass focus to the element's Zepto object.
              $ELEMENT.trigger('focus')

              // Bind the document events.
              $document.on('tap.' + STATE.id + ' focusin.' + STATE.id, function(event) {

                var target = event.target

                // If the target of the event is not the element, close the picker picker.
                // * Don't worry about clicks or focusins on the root because those don't bubble up.
                //   Also, for Firefox, a click on an `option` element bubbles up directly
                //   to the doc. So make sure the target wasn't the doc.
                // * In Firefox stopPropagation() doesn't prevent right-click events from bubbling,
                //   which causes the picker to unexpectedly close when right-clicking it. So make
                //   sure the event wasn't a right-click.
                if (target != ELEMENT && target != document && event.which != 3) {

                  // If the target was the holder that covers the screen,
                  // keep the element focused to maintain tabindex.
                  P.close(target === P.$root.children()[0])
                }

              }).on('keydown.' + STATE.id, function(event) {

                var
                // Get the keycode.
                  keycode = event.keyCode,

                  // Translate that to a selection change.
                  keycodeToMove = P.component.key[keycode],

                  // Grab the target.
                  target = event.target


                // On escape, close the picker and give focus.
                if (keycode == 27) {
                  P.close(true)
                }


                // Check if there is a key movement or "enter" keypress on the element.
                else if (target == ELEMENT && (keycodeToMove || keycode == 13)) {

                  // Prevent the default action to stop page movement.
                  event.preventDefault()

                  // Trigger the key movement action.
                  if (keycodeToMove) {
                    PickerConstructor._.trigger(P.component.key.go, P, [PickerConstructor._.trigger(keycodeToMove)])
                  }

                  // On "enter", if the highlighted item isn't disabled, set the value and close.
                  else if (!P.$root.find('.' + CLASSES.highlighted).hasClass(CLASSES.disabled)) {
                    P.set('select', P.component.item.highlight).close()
                  }
                }


                // If the target is within the root and "enter" is pressed,
                // prevent the default action and trigger a click on the target instead.
                else if ($.contains(P.$root[0], target) && keycode == 13) {
                  event.preventDefault()
                  target.click()
                }
              })
            }

            // Trigger the queued "open" events.
            return P.trigger('open')
          }, //open


          /**
           * Close the picker
           */
          close: function(giveFocus) {

            // If we need to give focus, do it before changing states.
            if (giveFocus) {
              // ....ah yes! It would've been incomplete without a crazy workaround for IE :|
              // The focus is triggered *after* the close has completed - causing it
              // to open again. So unbind and rebind the event at the next tick.
              $ELEMENT.off('focus.' + STATE.id).trigger('focus')
              setTimeout(function() {
                $ELEMENT.on('focus.' + STATE.id, focusToOpen)
              }, 0)
            }

            // Remove the "active" class.
            $ELEMENT.removeClass(CLASSES.active)
            aria(ELEMENT, 'expanded', false)

            // Remove the "opened" and "focused" class from the picker root.
            P.$root.removeClass(CLASSES.opened + ' ' + CLASSES.focused)
              /*
               * furnax edit 02
               */
            $("#content").removeClass("no-touches")
            setTimeout(function() {
              $("html").removeClass("picker-visible")
            }, 1200);
            /*
             * end furnax edit 02
             */

            aria(P.$root[0], 'hidden', true)
            aria(P.$root[0], 'selected', false)

            // If it's already closed, do nothing more.
            if (!STATE.open) return P

            // Set it as closed.
            STATE.open = false

            // Unbind the document events.
            $document.off('.' + STATE.id)

            // Trigger the queued "close" events.
            return P.trigger('close')
          }, //close


          /**
           * Clear the values
           */
          clear: function() {
            return P.set('clear')
          }, //clear


          /**
           * Set something
           */
          set: function(thing, value, options) {

            var thingItem, thingValue,
              thingIsObject = $.isPlainObject(thing),
              thingObject = thingIsObject ? thing : {}

            // Make sure we have usable options.
            options = thingIsObject && $.isPlainObject(value) ?  value : options || {}

            if (thing) {

              // If the thing isn't an object, make it one.
              if (!thingIsObject) {
                thingObject[thing] = value
              }

              // Go through the things of items to set.
              for (thingItem in thingObject) {

                // Grab the value of the thing.
                thingValue = thingObject[thingItem]

                // First, if the item exists and there's a value, set it.
                if (thingItem in P.component.item) {
                  P.component.set(thingItem, thingValue, options)
                }

                // Then, check to update the element value and broadcast a change.
                if (thingItem == 'select' || thingItem == 'clear') {
                  $ELEMENT.val(thingItem == 'clear' ?
                    '' : P.get(thingItem, SETTINGS.format)
                  ).trigger('change')
                }
              }

              // Render a new picker.
              P.render()
            }

            // When the method isn't muted, trigger queued "set" events and pass the `thingObject`.
            return options.muted ? P : P.trigger('set', thingObject)
          }, //set


          /**
           * Get something
           */
          get: function(thing, format) {

            // Make sure there's something to get.
            thing = thing || 'value'

            // If a picker state exists, return that.
            if (STATE[thing] != null) {
              return STATE[thing]
            }

            // Return the value, if that.
            if (thing == 'value') {
              return ELEMENT.value
            }

            // Check if a component item exists, return that.
            if (thing in P.component.item) {
              if (typeof format == 'string') {
                return PickerConstructor._.trigger(
                  P.component.formats.toString,
                  P.component, [format, P.component.get(thing)]
                )
              }
              return P.component.get(thing)
            }
          }, //get



          /**
           * Bind events on the things.
           */
          on: function(thing, method) {

            var thingName, thingMethod,
              thingIsObject = $.isPlainObject(thing),
              thingObject = thingIsObject ? thing : {}

            if (thing) {

              // If the thing isn't an object, make it one.
              if (!thingIsObject) {
                thingObject[thing] = method
              }

              // Go through the things to bind to.
              for (thingName in thingObject) {

                // Grab the method of the thing.
                thingMethod = thingObject[thingName]

                // Make sure the thing methods collection exists.
                STATE.methods[thingName] = STATE.methods[thingName] || []

                // Add the method to the relative method collection.
                STATE.methods[thingName].push(thingMethod)
              }
            }

            return P
          }, //on



          /**
           * Unbind events on the things.
           */
          off: function() {
            var i, thingName,
              names = arguments;
            for (i = 0, namesCount = names.length; i < namesCount; i += 1) {
              thingName = names[i]
              if (thingName in STATE.methods) {
                delete STATE.methods[thingName]
              }
            }
            return P
          },


          /**
           * Fire off method events.
           */
          trigger: function(name, data) {
              var methodList = STATE.methods[name]
              if (methodList) {
                methodList.map(function(method) {
                  PickerConstructor._.trigger(method, P, [data])
                })
              }
              return P
            } //trigger
        } //PickerInstance.prototype


      /**
       * Wrap the picker holder components together.
       */
      function createWrappedComponent() {

          // Create a picker wrapper holder
          return PickerConstructor._.node('div',

              // Create a picker wrapper node
              PickerConstructor._.node('div',

                // Create a picker frame
                PickerConstructor._.node('div',

                  // Create a picker box node
                  PickerConstructor._.node('div',

                    // Create the components nodes.
                    P.component.nodes(STATE.open),

                    // The picker box class
                    CLASSES.box
                  ),

                  // Picker wrap class
                  CLASSES.wrap
                ),

                // Picker frame class
                CLASSES.frame
              ),

              // Picker holder class
              CLASSES.holder
            ) //endreturn
        } //createWrappedComponent



      /**
       * Prepare the input element with all bindings.
       */
      function prepareElement() {

        $ELEMENT.

        // Store the picker data by component name.
        data(NAME, P).

        // Add the "input" class name.
        addClass(CLASSES.input).

        // If there's a `data-value`, update the value of the element.
        val($ELEMENT.data('value') ?
          P.get('select', SETTINGS.format) :
          ELEMENT.value
        ).

        // On focus/click, open the picker and adjust the root "focused" state.
        on('focus.' + STATE.id + ' tap.' + STATE.id, focusToOpen)


        // Only bind keydown events if the element isn't editable.
        if (!SETTINGS.editable) {

          // Handle keyboard event based on the picker being opened or not.
          $ELEMENT.on('keydown.' + STATE.id, function(event) {

            var keycode = event.keyCode,

              // Check if one of the delete keys was pressed.
              isKeycodeDelete = /^(8|46)$/.test(keycode)

            // For some reason IE clears the input value on "escape".
            if (keycode == 27) {
              P.close()
              return false
            }

            // Check if `space` or `delete` was pressed or the picker is closed with a key movement.
            if (keycode == 32 || isKeycodeDelete || !STATE.open && P.component.key[keycode]) {

              // Prevent it from moving the page and bubbling to doc.
              event.preventDefault()
              event.stopPropagation()

              // If `delete` was pressed, clear the values and close the picker.
              // Otherwise open the picker.
              if (isKeycodeDelete) {
                P.clear().close()
              } else {
                P.open()
              }
            }
          })
        }


        // Update the aria attributes.
        aria(ELEMENT, {
          haspopup: true,
          expanded: false,
          readonly: false,
          owns: ELEMENT.id + '_root' + (P._hidden ? ' ' + P._hidden.id : '')
        })
      }


      /**
       * Prepare the root picker element with all bindings.
       */
      function prepareElementRoot() {

        P.$root.

        on({

          // When something within the root is focused, stop from bubbling
          // to the doc and remove the "focused" state from the root.
          focusin: function(event) {
            P.$root.removeClass(CLASSES.focused)
            aria(P.$root[0], 'selected', false)
            event.stopPropagation()
          },

          // When something within the root holder is clicked, stop it
          // from bubbling to the doc.
          'tap': function(event) {

            var target = event.target

            // Make sure the target isn't the root holder so it can bubble up.
            if (target != P.$root.children()[0]) {

              event.stopPropagation()

              // * For mousedown events, cancel the default action in order to
              //   prevent cases where focus is shifted onto external elements
              //   when using things like Zepto mobile or MagnificPopup (ref: #249 & #120).
              //   Also, for Firefox, don't prevent action on the `option` element.
              if (event.type == 'tap' && !$(target).is('input, textarea') && target.nodeName != 'OPTION') {
                event.preventDefault();
                event.stopPropagation();

                // Re-focus onto the element so that users can click away
                // from elements focused within the picker.
                ELEMENT.focus()
              }
            }
          }
        }).

        // If there's a click on an actionable element, carry out the actions.
        on('tap', '[data-pick], [data-nav], [data-clear]', function(event) {
            event.preventDefault();
            event.stopPropagation();
            var me = this;
            var $target = $(me),
              targetData = $target.data(),
              targetDisabled = $target.hasClass(CLASSES.navDisabled) || $target.hasClass(CLASSES.disabled),

              // * For IE, non-focusable elements can be active elements as well
              //   (http://stackoverflow.com/a/2684561).
              activeElement = document.activeElement
            activeElement = activeElement && (activeElement.type || activeElement.href) && activeElement

            // If it's disabled or nothing inside is actively focused, re-focus the element.
            if (targetDisabled || activeElement && !$.contains(P.$root[0], activeElement)) {
              ELEMENT.focus()
            }

            // If something is superficially changed, update the `highlight` based on the `nav`.
            if (targetData.nav && !targetDisabled) {
              P.set('highlight', P.component.item.highlight, {
                nav: targetData.nav
              })
            }

            // If something is picked, set `select` then close with focus.
            else if (PickerConstructor._.isInteger(targetData.pick) && !targetDisabled) {
              P.set('select', targetData.pick).close(true)
            }

            // If a "clear" button is pressed, empty the values and close with focus.
            else if (targetData.clear) {
              P.clear().close(true)
            }
          }) //P.$root

        aria(P.$root[0], 'hidden', true)
      }


      /**
       * Prepare the hidden input element along with all bindings.
       */
      function prepareElementHidden() {

        var id = [
          typeof SETTINGS.hiddenPrefix == 'string' ? SETTINGS.hiddenPrefix : '',
          typeof SETTINGS.hiddenSuffix == 'string' ? SETTINGS.hiddenSuffix : '_submit'
        ]

        P._hidden = $(
          '<input ' +
          'type=hidden ' +

          // Create the name and ID by using the original
          // input's with a prefix and suffix.
          'name="' + id[0] + ELEMENT.name + id[1] + '"' +
          'id="' + id[0] + ELEMENT.id + id[1] + '"' +

          // If the element has a value, set the hidden value as well.
          (
            $ELEMENT.data('value') || ELEMENT.value ?
            ' value="' + P.get('select', SETTINGS.formatSubmit) + '"' :
            ''
          ) +
          '>'
        )[0]

        $ELEMENT.

        // If the value changes, update the hidden input with the correct format.
        on('change.' + STATE.id, function() {
          P._hidden.value = ELEMENT.value ?
            P.get('select', SETTINGS.formatSubmit) :
            ''
        }).

        // Insert the hidden input after the element.
        after(P._hidden)
      }


      // Separated for IE
      function focusToOpen(event) {

        // Stop the event from propagating to the doc.
        event.stopPropagation()

        // If it's a focus event, add the "focused" class to the root.
        if (event.type == 'focus') {
          P.$root.addClass(CLASSES.focused)
          aria(P.$root[0], 'selected', true)
        }

        // And then finally open the picker.
        P.open()
      }


      // Return a new picker instance.
      return new PickerInstance()
    } //PickerConstructor



  /**
   * The default classes and prefix to use for the HTML classes.
   */
  PickerConstructor.klasses = function(prefix) {
      prefix = prefix || 'picker'
      return {

        picker: prefix,
        opened: prefix + '--opened',
        focused: prefix + '--focused',

        input: prefix + '__input',
        active: prefix + '__input--active',

        holder: prefix + '__holder',

        frame: prefix + '__frame',
        wrap: prefix + '__wrap',

        box: prefix + '__box'
      }
    } //PickerConstructor.klasses



  /**
   * PickerConstructor helper methods.
   */
  PickerConstructor._ = {

      /**
       * Create a group of nodes. Expects:
       * `
          {
              min:    {Integer},
              max:    {Integer},
              i:      {Integer},
              node:   {String},
              item:   {Function}
          }
       * `
       */
      group: function(groupObject) {

        var
        // Scope for the looped object
          loopObjectScope,

          // Create the nodes list
          nodesList = '',

          // The counter starts from the `min`
          counter = PickerConstructor._.trigger(groupObject.min, groupObject)


        // Loop from the `min` to `max`, incrementing by `i`
        for (; counter <= PickerConstructor._.trigger(groupObject.max, groupObject, [counter]); counter += groupObject.i) {

          // Trigger the `item` function within scope of the object
          loopObjectScope = PickerConstructor._.trigger(groupObject.item, groupObject, [counter])

          // Splice the subgroup and create nodes out of the sub nodes
          nodesList += PickerConstructor._.node(
            groupObject.node,
            loopObjectScope[0], // the node
            loopObjectScope[1], // the classes
            loopObjectScope[2] // the attributes
          )
        }

        // Return the list of nodes
        return nodesList
      }, //group


      /**
       * Create a dom node string
       */
      node: function(wrapper, item, klass, attribute) {

        // If the item is false-y, just return an empty string
        if (!item) return ''

        // If the item is an array, do a join
        item = $.isArray(item) ? item.join('') : item

        // Check for the class
        klass = klass ? ' class="' + klass + '"' : ''

        // Check for any attributes
        attribute = attribute ? ' ' + attribute : ''

        // Return the wrapped item
        return '<' + wrapper + klass + attribute + '>' + item + '</' + wrapper + '>'
      }, //node


      /**
       * Lead numbers below 10 with a zero.
       */
      lead: function(number) {
        return (number < 10 ? '0' : '') + number
      },


      /**
       * Trigger a function otherwise return the value.
       */
      trigger: function(callback, scope, args) {
        return typeof callback == 'function' ? callback.apply(scope, args || []) : callback
      },


      /**
       * If the second character is a digit, length is 2 otherwise 1.
       */
      digits: function(string) {
        return (/\d/).test(string[1]) ? 2 : 1
      },


      /**
       * Tell if something is a date object.
       */
      isDate: function(value) {
        return {}.toString.call(value).indexOf('Date') > -1 && this.isInteger(value.getDate())
      },


      /**
       * Tell if something is an integer.
       */
      isInteger: function(value) {
        return {}.toString.call(value).indexOf('Number') > -1 && value % 1 === 0
      },


      /**
       * Create ARIA attribute strings.
       */
      ariaAttr: ariaAttr
    } //PickerConstructor._



  /**
   * Extend the picker with a component and defaults.
   */
  PickerConstructor.extend = function(name, Component) {

      // Extend Zepto.
      $.fn[name] = function(options, action) {

        // Grab the component data.
        var componentData = this.data(name)

        // If the picker is requested, return the data object.
        if (options == 'picker') {
          return componentData
        }

        // If the component data exists and `options` is a string, carry out the action.
        if (componentData && typeof options == 'string') {
          PickerConstructor._.trigger(componentData[options], componentData, [action])
          return this
        }

        // Otherwise go through each matched element and if the component
        // doesn't exist, create a new picker using `this` element
        // and merging the defaults and options with a deep copy.
        return this.each(function() {
          var $this = $(this)
          if (!$this.data(name)) {
            new PickerConstructor(this, name, Component, options)
          }
        })
      }

      // Set the defaults.
      $.fn[name].defaults = Component.defaults
    } //PickerConstructor.extend



  function aria(element, attribute, value) {
    if ($.isPlainObject(attribute)) {
      for (var key in attribute) {
        ariaSet(element, key, attribute[key])
      }
    } else {
      ariaSet(element, attribute, value)
    }
  }

  function ariaSet(element, attribute, value) {
    element.setAttribute(
      (attribute == 'role' ? '' : 'aria-') +  attribute,
      value
    )
  }

  function ariaAttr(attribute, data) {
    if (!$.isPlainObject(attribute)) {
      attribute = { 
        attribute: data
      }
    }
    data = ''
    for (var key in attribute) {
      var attr = (key == 'role' ? '' : 'aria-') +  key,
        attrVal = attribute[key]
      data += attrVal == null ? '' : attr + '="' + attribute[key] + '"'
    }
    return data
  }



  // Expose the picker constructor.
  return PickerConstructor


}));




/*!
 * Date picker for pickadate.js v3.4.0
 * http://amsul.github.io/pickadate.js/date.htm
 */

(function(factory) {

  // Register as an anonymous module.
  if (typeof define == 'function' && define.amd)
    define(['picker', 'Zepto'], factory)

  // Or using browser globals.
  else factory(Picker, Zepto)

}(function(Picker, $) {


  /**
   * Globals and constants
   */
  var DAYS_IN_WEEK = 7,
    WEEKS_IN_CALENDAR = 6,
    _ = Picker._



  /**
   * The date picker constructor
   */
  function DatePicker(picker, settings) {

      var calendar = this,
        elementValue = picker.$node[0].value,
        elementDataValue = picker.$node.data('value'),
        valueString = elementDataValue || elementValue,
        formatString = elementDataValue ? settings.formatSubmit : settings.format,
        isRTL = function() {
          return getComputedStyle(picker.$root[0]).direction === 'rtl'
        }

      calendar.settings = settings
      calendar.$node = picker.$node

      // The queue of methods that will be used to build item objects.
      calendar.queue = {
        min: 'measure create',
        max: 'measure create',
        now: 'now create',
        select: 'parse create validate',
        highlight: 'parse navigate create validate',
        view: 'parse create validate viewset',
        disable: 'deactivate',
        enable: 'activate'
      }

      // The component's item object.
      calendar.item = {}

      calendar.item.disable = (settings.disable || []).slice(0)
      calendar.item.enable = -(function(collectionDisabled) {
        return collectionDisabled[0] === true ? collectionDisabled.shift() : -1
      })(calendar.item.disable)

      calendar.
      set('min', settings.min).
      set('max', settings.max).
      set('now')

      // When there's a value, set the `select`, which in turn
      // also sets the `highlight` and `view`.
      if (valueString) {
        calendar.set('select', valueString, {
          format: formatString,
          fromValue: !!elementValue
        })
      }

      // If there's no value, default to highlighting "today".
      else {
        calendar.
        set('select', null).
        set('highlight', calendar.item.now)
      }


      // The keycode to movement mapping.
      calendar.key = {
        40: 7, // Down
        38: -7, // Up
        39: function() {
          return isRTL() ? -1 : 1
        }, // Right
        37: function() {
          return isRTL() ? 1 : -1
        }, // Left
        go: function(timeChange) {
          var highlightedObject = calendar.item.highlight,
            targetDate = new Date(highlightedObject.year, highlightedObject.month, highlightedObject.date + timeChange)
          calendar.set(
            'highlight', [targetDate.getFullYear(), targetDate.getMonth(), targetDate.getDate()], {
              interval: timeChange
            }
          )
          this.render()
        }
      }


      // Bind some picker events.
      picker.
      on('render', function() {
        picker.$root.find('.' + settings.klass.selectMonth).on('change', function() {
          var value = this.value
          if (value) {
            picker.set('highlight', [picker.get('view').year, value, picker.get('highlight').date])
            picker.$root.find('.' + settings.klass.selectMonth).trigger('focus')
          }
        })
        picker.$root.find('.' + settings.klass.selectYear).on('change', function() {
          var value = this.value
          if (value) {
            picker.set('highlight', [value, picker.get('view').month, picker.get('highlight').date])
            picker.$root.find('.' + settings.klass.selectYear).trigger('focus')
          }
        })
      }).
      on('open', function() {
        picker.$root.find('button, select').attr('disabled', false)
      }).
      on('close', function() {
        picker.$root.find('button, select').attr('disabled', true)
      })

    } //DatePicker


  /**
   * Set a datepicker item object.
   */
  DatePicker.prototype.set = function(type, value, options) {

      var calendar = this,
        calendarItem = calendar.item

      // If the value is `null` just set it immediately.
      if (value === null) {
        calendarItem[type] = value
        return calendar
      }

      // Otherwise go through the queue of methods, and invoke the functions.
      // Update this as the time unit, and set the final value as this item.
      // * In the case of `enable`, keep the queue but set `disable` instead.
      //   And in the case of `flip`, keep the queue but set `enable` instead.
      calendarItem[(type == 'enable' ? 'disable' : type == 'flip' ? 'enable' : type)] = calendar.queue[type].split(' ').map(function(method) {
        value = calendar[method](type, value, options)
        return value
      }).pop()

      // Check if we need to cascade through more updates.
      if (type == 'select') {
        calendar.set('highlight', calendarItem.select, options)
      } else if (type == 'highlight') {
        calendar.set('view', calendarItem.highlight, options)
      } else if (type.match(/^(flip|min|max|disable|enable)$/)) {
        if (calendarItem.select && calendar.disabled(calendarItem.select)) {
          calendar.set('select', calendarItem.select, options)
        }
        if (calendarItem.highlight && calendar.disabled(calendarItem.highlight)) {
          calendar.set('highlight', calendarItem.highlight, options)
        }
      }

      return calendar
    } //DatePicker.prototype.set


  /**
   * Get a datepicker item object.
   */
  DatePicker.prototype.get = function(type) {
      return this.item[type]
    } //DatePicker.prototype.get


  /**
   * Create a picker date object.
   */
  DatePicker.prototype.create = function(type, value, options) {

      var isInfiniteValue,
        calendar = this

      // If there's no value, use the type as the value.
      value = value === undefined ? type : value


      // If it's infinity, update the value.
      if (value == -Infinity || value == Infinity) {
        isInfiniteValue = value
      }

      // If it's an object, use the native date object.
      else if ($.isPlainObject(value) && _.isInteger(value.pick)) {
        value = value.obj
      }

      // If it's an array, convert it into a date and make sure
      // that it's a valid date – otherwise default to today.
      else if ($.isArray(value)) {
        value = new Date(value[0], value[1], value[2])
        value = _.isDate(value) ? value : calendar.create().obj
      }

      // If it's a number or date object, make a normalized date.
      else if (_.isInteger(value) || _.isDate(value)) {
        value = calendar.normalize(new Date(value), options)
      }

      // If it's a literal true or any other case, set it to now.
      else /*if ( value === true )*/ {
        value = calendar.now(type, value, options)
      }

      // Return the compiled object.
      return {
        year: isInfiniteValue || value.getFullYear(),
        month: isInfiniteValue || value.getMonth(),
        date: isInfiniteValue || value.getDate(),
        day: isInfiniteValue || value.getDay(),
        obj: isInfiniteValue || value,
        pick: isInfiniteValue || value.getTime()
      }
    } //DatePicker.prototype.create


  /**
   * Create a range limit object using an array, date object,
   * literal "true", or integer relative to another time.
   */
  DatePicker.prototype.createRange = function(from, to) {

      var calendar = this,
        createDate = function(date) {
          if (date === true || $.isArray(date) || _.isDate(date)) {
            return calendar.create(date)
          }
          return date
        }

      // Create objects if possible.
      if (!_.isInteger(from)) {
        from = createDate(from)
      }
      if (!_.isInteger(to)) {
        to = createDate(to)
      }

      // Create relative dates.
      if (_.isInteger(from) && $.isPlainObject(to)) {
        from = [to.year, to.month, to.date + from];
      } else if (_.isInteger(to) && $.isPlainObject(from)) {
        to = [from.year, from.month, from.date + to];
      }

      return {
        from: createDate(from),
        to: createDate(to)
      }
    } //DatePicker.prototype.createRange


  /**
   * Check if a date unit falls within a date range object.
   */
  DatePicker.prototype.withinRange = function(range, dateUnit) {
    range = this.createRange(range.from, range.to)
    return dateUnit.pick >= range.from.pick && dateUnit.pick <= range.to.pick
  }


  /**
   * Check if two date range objects overlap.
   */
  DatePicker.prototype.overlapRanges = function(one, two) {

    var calendar = this

    // Convert the ranges into comparable dates.
    one = calendar.createRange(one.from, one.to)
    two = calendar.createRange(two.from, two.to)

    return calendar.withinRange(one, two.from) ||  calendar.withinRange(one, two.to) ||
      calendar.withinRange(two, one.from) || calendar.withinRange(two, one.to)
  }


  /**
   * Get the date today.
   */
  DatePicker.prototype.now = function(type, value, options) {
    value = new Date()
    if (options && options.rel) {
      value.setDate(value.getDate() + options.rel)
    }
    return this.normalize(value, options)
  }


  /**
   * Navigate to next/prev month.
   */
  DatePicker.prototype.navigate = function(type, value, options) {

      var targetDateObject,
        targetYear,
        targetMonth,
        targetDate,
        isTargetArray = $.isArray(value),
        isTargetObject = $.isPlainObject(value),
        viewsetObject = this.item.view
        /*,
                safety = 100*/


      if (isTargetArray || isTargetObject) {

        if (isTargetObject) {
          targetYear = value.year
          targetMonth = value.month
          targetDate = value.date
        } else {
          targetYear = +value[0]
          targetMonth = +value[1]
          targetDate = +value[2]
        }

        // If we're navigating months but the view is in a different
        // month, navigate to the view's year and month.
        if (options && options.nav && viewsetObject && viewsetObject.month !== targetMonth) {
          targetYear = viewsetObject.year
          targetMonth = viewsetObject.month
        }

        // Figure out the expected target year and month.
        targetDateObject = new Date(targetYear, targetMonth + (options && options.nav ? options.nav : 0), 1)
        targetYear = targetDateObject.getFullYear()
        targetMonth = targetDateObject.getMonth()

        // If the month we're going to doesn't have enough days,
        // keep decreasing the date until we reach the month's last date.
        while ( /*safety &&*/ new Date(targetYear, targetMonth, targetDate).getMonth() !== targetMonth) {
          targetDate -= 1
            /*safety -= 1
            if ( !safety ) {
                throw 'Fell into an infinite loop while navigating to ' + new Date( targetYear, targetMonth, targetDate ) + '.'
            }*/
        }

        value = [targetYear, targetMonth, targetDate]
      }

      return value
    } //DatePicker.prototype.navigate


  /**
   * Normalize a date by setting the hours to midnight.
   */
  DatePicker.prototype.normalize = function(value /*, options*/ ) {
    value.setHours(0, 0, 0, 0)
    return value
  }


  /**
   * Measure the range of dates.
   */
  DatePicker.prototype.measure = function(type, value /*, options*/ ) {

      var calendar = this

      // If it's anything false-y, remove the limits.
      if (!value) {
        value = type == 'min' ? -Infinity : Infinity
      }

      // If it's an integer, get a date relative to today.
      else if (_.isInteger(value)) {
        value = calendar.now(type, value, {
          rel: value
        })
      }

      return value
    } ///DatePicker.prototype.measure


  /**
   * Create a viewset object based on navigation.
   */
  DatePicker.prototype.viewset = function(type, dateObject /*, options*/ ) {
    return this.create([dateObject.year, dateObject.month, 1])
  }


  /**
   * Validate a date as enabled and shift if needed.
   */
  DatePicker.prototype.validate = function(type, dateObject, options) {

      var calendar = this,

        // Keep a reference to the original date.
        originalDateObject = dateObject,

        // Make sure we have an interval.
        interval = options && options.interval ? options.interval : 1,

        // Check if the calendar enabled dates are inverted.
        isFlippedBase = calendar.item.enable === -1,

        // Check if we have any enabled dates after/before now.
        hasEnabledBeforeTarget, hasEnabledAfterTarget,

        // The min & max limits.
        minLimitObject = calendar.item.min,
        maxLimitObject = calendar.item.max,

        // Check if we've reached the limit during shifting.
        reachedMin, reachedMax,

        // Check if the calendar is inverted and at least one weekday is enabled.
        hasEnabledWeekdays = isFlippedBase && calendar.item.disable.filter(function(value) {

          // If there's a date, check where it is relative to the target.
          if ($.isArray(value)) {
            var dateTime = calendar.create(value).pick
            if (dateTime < dateObject.pick) hasEnabledBeforeTarget = true
            else if (dateTime > dateObject.pick) hasEnabledAfterTarget = true
          }

          // Return only integers for enabled weekdays.
          return _.isInteger(value)
        }).length
        /*,

                safety = 100*/



      // Cases to validate for:
      // [1] Not inverted and date disabled.
      // [2] Inverted and some dates enabled.
      // [3] Not inverted and out of range.
      //
      // Cases to **not** validate for:
      // • Navigating months.
      // • Not inverted and date enabled.
      // • Inverted and all dates disabled.
      // • ..and anything else.
      if (!options || !options.nav)
        if (
          /* 1 */
          (!isFlippedBase && calendar.disabled(dateObject)) ||
          /* 2 */
          (isFlippedBase && calendar.disabled(dateObject) && (hasEnabledWeekdays || hasEnabledBeforeTarget || hasEnabledAfterTarget)) ||
          /* 3 */
          (!isFlippedBase && (dateObject.pick <= minLimitObject.pick || dateObject.pick >= maxLimitObject.pick))
        ) {


          // When inverted, flip the direction if there aren't any enabled weekdays
          // and there are no enabled dates in the direction of the interval.
          if (isFlippedBase && !hasEnabledWeekdays && ((!hasEnabledAfterTarget && interval > 0) || (!hasEnabledBeforeTarget && interval < 0))) {
            interval *= -1
          }


          // Keep looping until we reach an enabled date.
          while ( /*safety &&*/ calendar.disabled(dateObject)) {

            /*safety -= 1
            if ( !safety ) {
                throw 'Fell into an infinite loop while validating ' + dateObject.obj + '.'
            }*/


            // If we've looped into the next/prev month with a large interval, return to the original date and flatten the interval.
            if (Math.abs(interval) > 1 && (dateObject.month < originalDateObject.month || dateObject.month > originalDateObject.month)) {
              dateObject = originalDateObject
              interval = interval > 0 ? 1 : -1
            }


            // If we've reached the min/max limit, reverse the direction, flatten the interval and set it to the limit.
            if (dateObject.pick <= minLimitObject.pick) {
              reachedMin = true
              interval = 1
              dateObject = calendar.create([minLimitObject.year, minLimitObject.month, minLimitObject.date - 1])
            } else if (dateObject.pick >= maxLimitObject.pick) {
              reachedMax = true
              interval = -1
              dateObject = calendar.create([maxLimitObject.year, maxLimitObject.month, maxLimitObject.date + 1])
            }


            // If we've reached both limits, just break out of the loop.
            if (reachedMin && reachedMax) {
              break
            }


            // Finally, create the shifted date using the interval and keep looping.
            dateObject = calendar.create([dateObject.year, dateObject.month, dateObject.date + interval])
          }

        } //endif


        // Return the date object settled on.
      return dateObject
    } //DatePicker.prototype.validate


  /**
   * Check if a date is disabled.
   */
  DatePicker.prototype.disabled = function(dateToVerify) {

      var
        calendar = this,

        // Filter through the disabled dates to check if this is one.
        isDisabledMatch = calendar.item.disable.filter(function(dateToDisable) {

          // If the date is a number, match the weekday with 0index and `firstDay` check.
          if (_.isInteger(dateToDisable)) {
            return dateToVerify.day === (calendar.settings.firstDay ? dateToDisable : dateToDisable - 1) % 7
          }

          // If it's an array or a native JS date, create and match the exact date.
          if ($.isArray(dateToDisable) || _.isDate(dateToDisable)) {
            return dateToVerify.pick === calendar.create(dateToDisable).pick
          }

          // If it's an object, match a date within the "from" and "to" range.
          if ($.isPlainObject(dateToDisable)) {
            return calendar.withinRange(dateToDisable, dateToVerify)
          }
        })

      // If this date matches a disabled date, confirm it's not inverted.
      isDisabledMatch = isDisabledMatch.length && !isDisabledMatch.filter(function(dateToDisable) {
        return $.isArray(dateToDisable) && dateToDisable[3] == 'inverted' ||
          $.isPlainObject(dateToDisable) && dateToDisable.inverted
      }).length

      // Check the calendar "enabled" flag and respectively flip the
      // disabled state. Then also check if it's beyond the min/max limits.
      return calendar.item.enable === -1 ? !isDisabledMatch : isDisabledMatch ||
        dateToVerify.pick < calendar.item.min.pick ||
        dateToVerify.pick > calendar.item.max.pick

    } //DatePicker.prototype.disabled


  /**
   * Parse a string into a usable type.
   */
  DatePicker.prototype.parse = function(type, value, options) {

      var calendar = this,
        parsingObject = {},
        monthIndex

      if (!value || _.isInteger(value) || $.isArray(value) || _.isDate(value) || $.isPlainObject(value) && _.isInteger(value.pick)) {
        return value
      }

      // We need a `.format` to parse the value with.
      if (!(options && options.format)) {
        options = options || {}
        options.format = calendar.settings.format
      }

      // Calculate the month index to adjust with.
      monthIndex = typeof value == 'string' && !options.fromValue ?  1 : 0

      // Convert the format into an array and then map through it.
      calendar.formats.toArray(options.format).map(function(label) {

        var
        // Grab the formatting label.
          formattingLabel = calendar.formats[label],

          // The format length is from the formatting label function or the
          // label length without the escaping exclamation (!) mark.
          formatLength = formattingLabel ? _.trigger(formattingLabel, calendar, [value, parsingObject]) : label.replace(/^!/, '').length

        // If there's a format label, split the value up to the format length.
        // Then add it to the parsing object with appropriate label.
        if (formattingLabel) {
          parsingObject[label] = value.substr(0, formatLength)
        }

        // Update the value as the substring from format length to end.
        value = value.substr(formatLength)
      })

      // If it's parsing a user provided month value, compensate for month 0index.
      return [
        parsingObject.yyyy || parsingObject.yy, +(parsingObject.mm || parsingObject.m) - monthIndex,
        parsingObject.dd || parsingObject.d
      ]
    } //DatePicker.prototype.parse


  /**
   * Various formats to display the object in.
   */
  DatePicker.prototype.formats = (function() {

      // Return the length of the first word in a collection.
      function getWordLengthFromCollection(string, collection, dateObject) {

        // Grab the first word from the string.
        var word = string.match(/\w+/)[0]

        // If there's no month index, add it to the date object
        if (!dateObject.mm && !dateObject.m) {
          dateObject.m = collection.indexOf(word)
        }

        // Return the length of the word.
        return word.length
      }

      // Get the length of the first word in a string.
      function getFirstWordLength(string) {
        return string.match(/\w+/)[0].length
      }

      return {

        d: function(string, dateObject) {

          // If there's string, then get the digits length.
          // Otherwise return the selected date.
          return string ? _.digits(string) : dateObject.date
        },
        dd: function(string, dateObject) {

          // If there's a string, then the length is always 2.
          // Otherwise return the selected date with a leading zero.
          return string ? 2 : _.lead(dateObject.date)
        },
        ddd: function(string, dateObject) {

          // If there's a string, then get the length of the first word.
          // Otherwise return the short selected weekday.
          return string ? getFirstWordLength(string) : this.settings.weekdaysShort[dateObject.day]
        },
        dddd: function(string, dateObject) {

          // If there's a string, then get the length of the first word.
          // Otherwise return the full selected weekday.
          return string ? getFirstWordLength(string) : this.settings.weekdaysFull[dateObject.day]
        },
        m: function(string, dateObject) {

          // If there's a string, then get the length of the digits
          // Otherwise return the selected month with 0index compensation.
          return string ? _.digits(string) : dateObject.month + 1
        },
        mm: function(string, dateObject) {

          // If there's a string, then the length is always 2.
          // Otherwise return the selected month with 0index and leading zero.
          return string ? 2 : _.lead(dateObject.month + 1)
        },
        mmm: function(string, dateObject) {

          var collection = this.settings.monthsShort

          // If there's a string, get length of the relevant month from the short
          // months collection. Otherwise return the selected month from that collection.
          return string ? getWordLengthFromCollection(string, collection, dateObject) : collection[dateObject.month]
        },
        mmmm: function(string, dateObject) {

          var collection = this.settings.monthsFull

          // If there's a string, get length of the relevant month from the full
          // months collection. Otherwise return the selected month from that collection.
          return string ? getWordLengthFromCollection(string, collection, dateObject) : collection[dateObject.month]
        },
        yy: function(string, dateObject) {

          // If there's a string, then the length is always 2.
          // Otherwise return the selected year by slicing out the first 2 digits.
          return string ? 2 : ('' + dateObject.year).slice(2)
        },
        yyyy: function(string, dateObject) {

          // If there's a string, then the length is always 4.
          // Otherwise return the selected year.
          return string ? 4 : dateObject.year
        },

        // Create an array by splitting the formatting string passed.
        toArray: function(formatString) {
          return formatString.split(/(d{1,4}|m{1,4}|y{4}|yy|!.)/g)
        },

        // Format an object into a string using the formatting options.
        toString: function(formatString, itemObject) {
          var calendar = this
          return calendar.formats.toArray(formatString).map(function(label) {
            return _.trigger(calendar.formats[label], calendar, [0, itemObject]) || label.replace(/^!/, '')
          }).join('')
        }
      }
    })() //DatePicker.prototype.formats




  /**
   * Check if two date units are the exact.
   */
  DatePicker.prototype.isDateExact = function(one, two) {

    var calendar = this

    // When we're working with weekdays, do a direct comparison.
    if (
      (_.isInteger(one) && _.isInteger(two)) ||
      (typeof one == 'boolean' && typeof two == 'boolean')
    ) {
      return one === two
    }

    // When we're working with date representations, compare the "pick" value.
    if (
      (_.isDate(one) || $.isArray(one)) &&
      (_.isDate(two) || $.isArray(two))
    ) {
      return calendar.create(one).pick === calendar.create(two).pick
    }

    // When we're working with range objects, compare the "from" and "to".
    if ($.isPlainObject(one) && $.isPlainObject(two)) {
      return calendar.isDateExact(one.from, two.from) && calendar.isDateExact(one.to, two.to)
    }

    return false
  }


  /**
   * Check if two date units overlap.
   */
  DatePicker.prototype.isDateOverlap = function(one, two) {

    var calendar = this

    // When we're working with a weekday index, compare the days.
    if (_.isInteger(one) && (_.isDate(two) || $.isArray(two))) {
      return one === calendar.create(two).day + 1
    }
    if (_.isInteger(two) && (_.isDate(one) || $.isArray(one))) {
      return two === calendar.create(one).day + 1
    }

    // When we're working with range objects, check if the ranges overlap.
    if ($.isPlainObject(one) && $.isPlainObject(two)) {
      return calendar.overlapRanges(one, two)
    }

    return false
  }


  /**
   * Flip the "enabled" state.
   */
  DatePicker.prototype.flipEnable = function(val) {
    var itemObject = this.item
    itemObject.enable = val || (itemObject.enable == -1 ? 1 : -1)
  }


  /**
   * Mark a collection of dates as "disabled".
   */
  DatePicker.prototype.deactivate = function(type, datesToDisable) {

      var calendar = this,
        disabledItems = calendar.item.disable.slice(0)


      // If we're flipping, that's all we need to do.
      if (datesToDisable == 'flip') {
        calendar.flipEnable()
      } else if (datesToDisable === false) {
        calendar.flipEnable(1)
        disabledItems = []
      } else if (datesToDisable === true) {
        calendar.flipEnable(-1)
        disabledItems = []
      }

      // Otherwise go through the dates to disable.
      else {

        datesToDisable.map(function(unitToDisable) {

          var matchFound

          // When we have disabled items, check for matches.
          // If something is matched, immediately break out.
          for (var index = 0; index < disabledItems.length; index += 1) {
            if (calendar.isDateExact(unitToDisable, disabledItems[index])) {
              matchFound = true
              break
            }
          }

          // If nothing was found, add the validated unit to the collection.
          if (!matchFound) {
            if (
              _.isInteger(unitToDisable) ||
              _.isDate(unitToDisable) ||
              $.isArray(unitToDisable) ||
              ($.isPlainObject(unitToDisable) && unitToDisable.from && unitToDisable.to)
            ) {
              disabledItems.push(unitToDisable)
            }
          }
        })
      }

      // Return the updated collection.
      return disabledItems
    } //DatePicker.prototype.deactivate


  /**
   * Mark a collection of dates as "enabled".
   */
  DatePicker.prototype.activate = function(type, datesToEnable) {

      var calendar = this,
        disabledItems = calendar.item.disable,
        disabledItemsCount = disabledItems.length

      // If we're flipping, that's all we need to do.
      if (datesToEnable == 'flip') {
        calendar.flipEnable()
      } else if (datesToEnable === true) {
        calendar.flipEnable(1)
        disabledItems = []
      } else if (datesToEnable === false) {
        calendar.flipEnable(-1)
        disabledItems = []
      }

      // Otherwise go through the disabled dates.
      else {

        datesToEnable.map(function(unitToEnable) {

          var matchFound,
            disabledUnit,
            index,
            isExactRange

          // Go through the disabled items and try to find a match.
          for (index = 0; index < disabledItemsCount; index += 1) {

            disabledUnit = disabledItems[index]

            // When an exact match is found, remove it from the collection.
            if (calendar.isDateExact(disabledUnit, unitToEnable)) {
              matchFound = disabledItems[index] = null
              isExactRange = true
              break
            }

            // When an overlapped match is found, add the "inverted" state to it.
            else if (calendar.isDateOverlap(disabledUnit, unitToEnable)) {
              if ($.isPlainObject(unitToEnable)) {
                unitToEnable.inverted = true
                matchFound = unitToEnable
              } else if ($.isArray(unitToEnable)) {
                matchFound = unitToEnable
                if (!matchFound[3]) matchFound.push('inverted')
              } else if (_.isDate(unitToEnable)) {
                matchFound = [unitToEnable.getFullYear(), unitToEnable.getMonth(), unitToEnable.getDate(), 'inverted']
              }
              break
            }
          }

          // If a match was found, remove a previous duplicate entry.
          if (matchFound)
            for (index = 0; index < disabledItemsCount; index += 1) {
              if (calendar.isDateExact(disabledItems[index], unitToEnable)) {
                disabledItems[index] = null
                break
              }
            }

          // In the event that we're dealing with an exact range of dates,
          // make sure there are no "inverted" dates because of it.
          if (isExactRange)
            for (index = 0; index < disabledItemsCount; index += 1) {
              if (calendar.isDateOverlap(disabledItems[index], unitToEnable)) {
                disabledItems[index] = null
                break
              }
            }

          // If something is still matched, add it into the collection.
          if (matchFound) {
            disabledItems.push(matchFound)
          }
        })
      }

      // Return the updated collection.
      return disabledItems.filter(function(val) {
        return val != null
      })
    } //DatePicker.prototype.activate


  /**
   * Create a string for the nodes in the picker.
   */
  DatePicker.prototype.nodes = function(isOpen) {

      var
        calendar = this,
        settings = calendar.settings,
        calendarItem = calendar.item,
        nowObject = calendarItem.now,
        selectedObject = calendarItem.select,
        highlightedObject = calendarItem.highlight,
        viewsetObject = calendarItem.view,
        disabledCollection = calendarItem.disable,
        minLimitObject = calendarItem.min,
        maxLimitObject = calendarItem.max,


        // Create the calendar table head using a copy of weekday labels collection.
        // * We do a copy so we don't mutate the original array.
        tableHead = (function(collection) {

          // If the first day should be Monday, move Sunday to the end.
          if (settings.firstDay) {
            collection.push(collection.shift())
          }

          // Create and return the table head group.
          return _.node(
              'thead',
              _.node(
                'tr',
                _.group({
                  min: 0,
                  max: DAYS_IN_WEEK - 1,
                  i: 1,
                  node: 'th',
                  item: function(counter) {
                    return [
                      collection[counter],
                      settings.klass.weekdays
                    ]
                  }
                })
              )
            ) //endreturn
        })((settings.showWeekdaysFull ? settings.weekdaysFull : settings.weekdaysShort).slice(0)), //tableHead


        // Create the nav for next/prev month.
        createMonthNav = function(next) {

          // Otherwise, return the created month tag.
          return _.node(
              'div',
              ' ',
              settings.klass['nav' + (next ? 'Next' : 'Prev')] + (

                // If the focused month is outside the range, disabled the button.
                (next && viewsetObject.year >= maxLimitObject.year && viewsetObject.month >= maxLimitObject.month) ||
                (!next && viewsetObject.year <= minLimitObject.year && viewsetObject.month <= minLimitObject.month) ?
                ' ' + settings.klass.navDisabled : ''
              ),
              'data-nav=' + (next || -1)
            ) //endreturn
        }, //createMonthNav


        // Create the month label.
        createMonthLabel = function(monthsCollection) {

          // If there are months to select, add a dropdown menu.
          if (settings.selectMonths) {

            return _.node('select', _.group({
              min: 0,
              max: 11,
              i: 1,
              node: 'option',
              item: function(loopedMonth) {

                return [

                  // The looped month and no classes.
                  monthsCollection[loopedMonth], 0,

                  // Set the value and selected index.
                  'value=' + loopedMonth +
                  (viewsetObject.month == loopedMonth ? ' selected' : '') +
                  (
                    (
                      (viewsetObject.year == minLimitObject.year && loopedMonth < minLimitObject.month) ||
                      (viewsetObject.year == maxLimitObject.year && loopedMonth > maxLimitObject.month)
                    ) ?
                    ' disabled' : ''
                  )
                ]
              }
            }), settings.klass.selectMonth, isOpen ? '' : 'disabled')
          }

          // If there's a need for a month selector
          return _.node('div', monthsCollection[viewsetObject.month], settings.klass.month)
        }, //createMonthLabel


        // Create the year label.
        createYearLabel = function() {

          var focusedYear = viewsetObject.year,

            // If years selector is set to a literal "true", set it to 5. Otherwise
            // divide in half to get half before and half after focused year.
            numberYears = settings.selectYears === true ? 5 : ~~(settings.selectYears / 2)

          // If there are years to select, add a dropdown menu.
          if (numberYears) {

            var
              minYear = minLimitObject.year,
              maxYear = maxLimitObject.year,
              lowestYear = focusedYear - numberYears,
              highestYear = focusedYear + numberYears

            // If the min year is greater than the lowest year, increase the highest year
            // by the difference and set the lowest year to the min year.
            if (minYear > lowestYear) {
              highestYear += minYear - lowestYear
              lowestYear = minYear
            }

            // If the max year is less than the highest year, decrease the lowest year
            // by the lower of the two: available and needed years. Then set the
            // highest year to the max year.
            if (maxYear < highestYear) {

              var availableYears = lowestYear - minYear,
                neededYears = highestYear - maxYear

              lowestYear -= availableYears > neededYears ? neededYears : availableYears
              highestYear = maxYear
            }

            return _.node('select', _.group({
              min: lowestYear,
              max: highestYear,
              i: 1,
              node: 'option',
              item: function(loopedYear) {
                return [

                  // The looped year and no classes.
                  loopedYear, 0,

                  // Set the value and selected index.
                  'value=' + loopedYear + (focusedYear == loopedYear ? ' selected' : '')
                ]
              }
            }), settings.klass.selectYear, isOpen ? '' : 'disabled')
          }

          // Otherwise just return the year focused
          return _.node('div', focusedYear, settings.klass.year)
        } //createYearLabel


      // Create and return the entire calendar.
      return _.node(
          'div',
          createMonthNav() + createMonthNav(1) +
          createMonthLabel(settings.showMonthsShort ? settings.monthsShort : settings.monthsFull) +
          createYearLabel(),
          settings.klass.header
        ) + _.node(
          'table',
          tableHead +
          _.node(
            'tbody',
            _.group({
              min: 0,
              max: WEEKS_IN_CALENDAR - 1,
              i: 1,
              node: 'tr',
              item: function(rowCounter) {

                // If Monday is the first day and the month starts on Sunday, shift the date back a week.
                var shiftDateBy = settings.firstDay && calendar.create([viewsetObject.year, viewsetObject.month, 1]).day === 0 ? -7 : 0

                return [
                    _.group({
                      min: DAYS_IN_WEEK * rowCounter - viewsetObject.day + shiftDateBy + 1, // Add 1 for weekday 0index
                      max: function() {
                        return this.min + DAYS_IN_WEEK - 1
                      },
                      i: 1,
                      node: 'td',
                      item: function(targetDate) {

                        // Convert the time date from a relative date to a target date.
                        targetDate = calendar.create([viewsetObject.year, viewsetObject.month, targetDate + (settings.firstDay ? 1 : 0)])

                        var isSelected = selectedObject && selectedObject.pick == targetDate.pick,
                          isHighlighted = highlightedObject && highlightedObject.pick == targetDate.pick,
                          isDisabled = disabledCollection && calendar.disabled(targetDate) || targetDate.pick < minLimitObject.pick || targetDate.pick > maxLimitObject.pick

                        return [
                            _.node(
                              'div',
                              targetDate.date, (function(klasses) {

                                // Add the `infocus` or `outfocus` classes based on month in view.
                                klasses.push(viewsetObject.month == targetDate.month ? settings.klass.infocus : settings.klass.outfocus)

                                // Add the `today` class if needed.
                                if (nowObject.pick == targetDate.pick) {
                                  klasses.push(settings.klass.now)
                                }

                                // Add the `selected` class if something's selected and the time matches.
                                if (isSelected) {
                                  klasses.push(settings.klass.selected)
                                }

                                // Add the `highlighted` class if something's highlighted and the time matches.
                                if (isHighlighted) {
                                  klasses.push(settings.klass.highlighted)
                                }

                                // Add the `disabled` class if something's disabled and the object matches.
                                if (isDisabled) {
                                  klasses.push(settings.klass.disabled)
                                }

                                return klasses.join(' ')
                              })([settings.klass.day]),
                              'data-pick=' + targetDate.pick + ' ' + _.ariaAttr({
                                role: 'button',
                                controls: calendar.$node[0].id,
                                checked: isSelected && calendar.$node.val() === _.trigger(
                                  calendar.formats.toString,
                                  calendar, [settings.format, targetDate]
                                ) ? true : null,
                                activedescendant: isHighlighted ? true : null,
                                disabled: isDisabled ? true : null
                              })
                            )
                          ] //endreturn
                      }
                    })
                  ] //endreturn
              }
            })
          ),
          settings.klass.table
        ) +

        // * For Firefox forms to submit, make sure to set the buttons' `type` attributes as "button".
        _.node(
          'div',
          _.node('button', settings.today, settings.klass.buttonToday, 'type=button data-pick=' + nowObject.pick + (isOpen ? '' : ' disabled')) +
          _.node('button', settings.clear, settings.klass.buttonClear, 'type=button data-clear=1' + (isOpen ? '' : ' disabled')),
          settings.klass.footer
        ) //endreturn
    } //DatePicker.prototype.nodes




  /**
   * The date picker defaults.
   */
  DatePicker.defaults = (function(prefix) {

    return {

      // Months and weekdays
      monthsFull: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      weekdaysFull: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      weekdaysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],

      // Today and clear
      today: 'Today',
      clear: 'Clear',

      // The format to show on the `input` element
      format: 'd mmmm, yyyy',

      // Classes
      klass: {

        table: prefix + 'table',

        header: prefix + 'header',

        navPrev: prefix + 'nav--prev',
        navNext: prefix + 'nav--next',
        navDisabled: prefix + 'nav--disabled',

        month: prefix + 'month',
        year: prefix + 'year',

        selectMonth: prefix + 'select--month',
        selectYear: prefix + 'select--year',

        weekdays: prefix + 'weekday',

        day: prefix + 'day',
        disabled: prefix + 'day--disabled',
        selected: prefix + 'day--selected',
        highlighted: prefix + 'day--highlighted',
        now: prefix + 'day--today',
        infocus: prefix + 'day--infocus',
        outfocus: prefix + 'day--outfocus',

        footer: prefix + 'footer',

        buttonClear: prefix + 'button--clear',
        buttonToday: prefix + 'button--today'
      }
    }
  })(Picker.klasses().picker + '__')




  /**
   * Extend the picker to add the date picker.
   */
  Picker.extend('pickadate', DatePicker)


}));




/*!
 * Time picker for pickadate.js v3.4.0
 * http://amsul.github.io/pickadate.js/time.htm
 */

(function(factory) {

  // Register as an anonymous module.
  if (typeof define == 'function' && define.amd)
    define(['picker', 'Zepto'], factory)

  // Or using browser globals.
  else factory(Picker, Zepto)

}(function(Picker, $) {


  /**
   * Globals and constants
   */
  var HOURS_IN_DAY = 24,
    MINUTES_IN_HOUR = 60,
    HOURS_TO_NOON = 12,
    MINUTES_IN_DAY = HOURS_IN_DAY * MINUTES_IN_HOUR,
    _ = Picker._



  /**
   * The time picker constructor
   */
  function TimePicker(picker, settings) {

      var clock = this,
        elementValue = picker.$node[0].value,
        elementDataValue = picker.$node.data('value'),
        valueString = elementDataValue || elementValue,
        formatString = elementDataValue ? settings.formatSubmit : settings.format

      clock.settings = settings
      clock.$node = picker.$node

      // The queue of methods that will be used to build item objects.
      clock.queue = {
        interval: 'i',
        min: 'measure create',
        max: 'measure create',
        now: 'now create',
        select: 'parse create validate',
        highlight: 'parse create validate',
        view: 'parse create validate',
        disable: 'deactivate',
        enable: 'activate'
      }

      // The component's item object.
      clock.item = {}

      clock.item.interval = settings.interval || 30
      clock.item.disable = (settings.disable || []).slice(0)
      clock.item.enable = -(function(collectionDisabled) {
        return collectionDisabled[0] === true ? collectionDisabled.shift() : -1
      })(clock.item.disable)

      clock.
      set('min', settings.min).
      set('max', settings.max).
      set('now')

      // When there's a value, set the `select`, which in turn
      // also sets the `highlight` and `view`.
      if (valueString) {
        clock.set('select', valueString, {
          format: formatString,
          fromValue: !!elementValue
        })
      }

      // If there's no value, default to highlighting "today".
      else {
        clock.
        set('select', null).
        set('highlight', clock.item.now)
      }

      // The keycode to movement mapping.
      clock.key = {
        40: 1, // Down
        38: -1, // Up
        39: 1, // Right
        37: -1, // Left
        go: function(timeChange) {
          clock.set(
            'highlight',
            clock.item.highlight.pick + timeChange * clock.item.interval, {
              interval: timeChange * clock.item.interval
            }
          )
          this.render()
        }
      }


      // Bind some picker events.
      picker.
        /*        on( 'render', function() {
                  
        //            need to change the holder, our css demands the scroll on __wrap
        //            var $pickerHolder = picker.$root.children(),
        //                $viewset = $pickerHolder.find( '.' + settings.klass.viewset )

                    var $pickerHolder = picker.$root.find("." + Picker.klasses().wrap);
                    console.log($pickerHolder);
                    var $viewset = $pickerHolder.find( '.' + settings.klass.viewset );

                    if ( $viewset.length ) {
                        setTimeout( function() { $pickerHolder[ 0 ].scrollTop = ~~$viewset.position().top - ( $viewset[ 0 ].clientHeight * 2 ) }, 400);
                      // need to do this on .picker__wrap because of FURNAX changes to CSS
                    }
                }). */
      on('open', function() {
        picker.$root.find('button').attr('disable', false)
      }).
      on('close', function() {
        picker.$root.find('button').attr('disable', true)
      })

    } //TimePicker


  /**
   * Set a timepicker item object.
   */
  TimePicker.prototype.set = function(type, value, options) {

      var clock = this,
        clockItem = clock.item

      // If the value is `null` just set it immediately.
      if (value === null) {
        clockItem[type] = value
        return clock
      }

      // Otherwise go through the queue of methods, and invoke the functions.
      // Update this as the time unit, and set the final value as this item.
      // * In the case of `enable`, keep the queue but set `disable` instead.
      //   And in the case of `flip`, keep the queue but set `enable` instead.
      clockItem[(type == 'enable' ? 'disable' : type == 'flip' ? 'enable' : type)] = clock.queue[type].split(' ').map(function(method) {
        value = clock[method](type, value, options)
        return value
      }).pop()

      // Check if we need to cascade through more updates.
      if (type == 'select') {
        clock.set('highlight', clockItem.select, options)
      } else if (type == 'highlight') {
        clock.set('view', clockItem.highlight, options)
      } else if (type == 'interval') {
        clock.
        set('min', clockItem.min, options).
        set('max', clockItem.max, options)
      } else if (type.match(/^(flip|min|max|disable|enable)$/)) {
        if (type == 'min') {
          clock.set('max', clockItem.max, options)
        }
        if (clockItem.select && clock.disabled(clockItem.select)) {
          clock.set('select', clockItem.select, options)
        }
        if (clockItem.highlight && clock.disabled(clockItem.highlight)) {
          clock.set('highlight', clockItem.highlight, options)
        }
      }

      return clock
    } //TimePicker.prototype.set


  /**
   * Get a timepicker item object.
   */
  TimePicker.prototype.get = function(type) {
      return this.item[type]
    } //TimePicker.prototype.get


  /**
   * Create a picker time object.
   */
  TimePicker.prototype.create = function(type, value, options) {

      var clock = this

      // If there's no value, use the type as the value.
      value = value === undefined ? type : value

      // If it's a date object, convert it into an array.
      if (_.isDate(value)) {
        value = [value.getHours(), value.getMinutes()]
      }

      // If it's an object, use the "pick" value.
      if ($.isPlainObject(value) && _.isInteger(value.pick)) {
        value = value.pick
      }

      // If it's an array, convert it into minutes.
      else if ($.isArray(value)) {
        value = +value[0] * MINUTES_IN_HOUR + (+value[1])
      }

      // If no valid value is passed, set it to "now".
      else if (!_.isInteger(value)) {
        value = clock.now(type, value, options)
      }

      // If we're setting the max, make sure it's greater than the min.
      if (type == 'max' && value < clock.item.min.pick) {
        value += MINUTES_IN_DAY
      }

      // If the value doesn't fall directly on the interval,
      // add one interval to indicate it as "passed".
      if (type != 'min' && type != 'max' && (value - clock.item.min.pick) % clock.item.interval !== 0) {
        value += clock.item.interval
      }

      // Normalize it into a "reachable" interval.
      value = clock.normalize(type, value, options)

      // Return the compiled object.
      return {

        // Divide to get hours from minutes.
        hour: ~~(HOURS_IN_DAY + value / MINUTES_IN_HOUR) % HOURS_IN_DAY,

        // The remainder is the minutes.
        mins: (MINUTES_IN_HOUR + value % MINUTES_IN_HOUR) % MINUTES_IN_HOUR,

        // The time in total minutes.
        time: (MINUTES_IN_DAY + value) % MINUTES_IN_DAY,

        // Reference to the "relative" value to pick.
        pick: value
      }
    } //TimePicker.prototype.create


  /**
   * Create a range limit object using an array, date object,
   * literal "true", or integer relative to another time.
   */
  TimePicker.prototype.createRange = function(from, to) {

      var clock = this,
        createTime = function(time) {
          if (time === true || $.isArray(time) || _.isDate(time)) {
            return clock.create(time)
          }
          return time
        }

      // Create objects if possible.
      if (!_.isInteger(from)) {
        from = createTime(from)
      }
      if (!_.isInteger(to)) {
        to = createTime(to)
      }

      // Create relative times.
      if (_.isInteger(from) && $.isPlainObject(to)) {
        from = [to.hour, to.mins + (from * clock.settings.interval)];
      } else if (_.isInteger(to) && $.isPlainObject(from)) {
        to = [from.hour, from.mins + (to * clock.settings.interval)];
      }

      return {
        from: createTime(from),
        to: createTime(to)
      }
    } //TimePicker.prototype.createRange


  /**
   * Check if a time unit falls within a time range object.
   */
  TimePicker.prototype.withinRange = function(range, timeUnit) {
    range = this.createRange(range.from, range.to)
    return timeUnit.pick >= range.from.pick && timeUnit.pick <= range.to.pick
  }


  /**
   * Check if two time range objects overlap.
   */
  TimePicker.prototype.overlapRanges = function(one, two) {

    var clock = this

    // Convert the ranges into comparable times.
    one = clock.createRange(one.from, one.to)
    two = clock.createRange(two.from, two.to)

    return clock.withinRange(one, two.from) ||  clock.withinRange(one, two.to) ||
      clock.withinRange(two, one.from) || clock.withinRange(two, one.to)
  }


  /**
   * Get the time relative to now.
   */
  TimePicker.prototype.now = function(type, value /*, options*/ ) {

      var interval = this.item.interval,
        date = new Date(),
        nowMinutes = date.getHours() * MINUTES_IN_HOUR + date.getMinutes(),
        isValueInteger = _.isInteger(value),
        isBelowInterval

      // Make sure "now" falls within the interval range.
      nowMinutes -= nowMinutes % interval

      // Check if the difference is less than the interval itself.
      isBelowInterval = value < 0 && interval * value + nowMinutes <= -interval

      // Add an interval because the time has "passed".
      nowMinutes += type == 'min' && isBelowInterval ? 0 : interval

      // If the value is a number, adjust by that many intervals.
      if (isValueInteger) {
        nowMinutes += interval * (
          isBelowInterval && type != 'max' ?
          value + 1 :
          value
        )
      }

      // Return the final calculation.
      return nowMinutes
    } //TimePicker.prototype.now


  /**
   * Normalize minutes to be "reachable" based on the min and interval.
   */
  TimePicker.prototype.normalize = function(type, value /*, options*/ ) {

      var interval = this.item.interval,
        minTime = this.item.min && this.item.min.pick || 0

      // If setting min time, don't shift anything.
      // Otherwise get the value and min difference and then
      // normalize the difference with the interval.
      value -= type == 'min' ? 0 : (value - minTime) % interval

      // Return the adjusted value.
      return value
    } //TimePicker.prototype.normalize


  /**
   * Measure the range of minutes.
   */
  TimePicker.prototype.measure = function(type, value, options) {

      var clock = this

      // If it's anything false-y, set it to the default.
      if (!value) {
        value = type == 'min' ? [0, 0] : [HOURS_IN_DAY - 1, MINUTES_IN_HOUR - 1]
      }

      // If it's a literal true, or an integer, make it relative to now.
      else if (value === true || _.isInteger(value)) {
        value = clock.now(type, value, options)
      }

      // If it's an object already, just normalize it.
      else if ($.isPlainObject(value) && _.isInteger(value.pick)) {
        value = clock.normalize(type, value.pick, options)
      }

      return value
    } ///TimePicker.prototype.measure


  /**
   * Validate an object as enabled.
   */
  TimePicker.prototype.validate = function(type, timeObject, options) {

      var clock = this,
        interval = options && options.interval ? options.interval : clock.item.interval

      // Check if the object is disabled.
      if (clock.disabled(timeObject)) {

        // Shift with the interval until we reach an enabled time.
        timeObject = clock.shift(timeObject, interval)
      }

      // Scope the object into range.
      timeObject = clock.scope(timeObject)

      // Do a second check to see if we landed on a disabled min/max.
      // In that case, shift using the opposite interval as before.
      if (clock.disabled(timeObject)) {
        timeObject = clock.shift(timeObject, interval * -1)
      }

      // Return the final object.
      return timeObject
    } //TimePicker.prototype.validate


  /**
   * Check if an object is disabled.
   */
  TimePicker.prototype.disabled = function(timeToVerify) {

      var clock = this,

        // Filter through the disabled times to check if this is one.
        isDisabledMatch = clock.item.disable.filter(function(timeToDisable) {

          // If the time is a number, match the hours.
          if (_.isInteger(timeToDisable)) {
            return timeToVerify.hour == timeToDisable
          }

          // If it's an array, create the object and match the times.
          if ($.isArray(timeToDisable) || _.isDate(timeToDisable)) {
            return timeToVerify.pick == clock.create(timeToDisable).pick
          }

          // If it's an object, match a time within the "from" and "to" range.
          if ($.isPlainObject(timeToDisable)) {
            return clock.withinRange(timeToDisable, timeToVerify)
          }
        })

      // If this time matches a disabled time, confirm it's not inverted.
      isDisabledMatch = isDisabledMatch.length && !isDisabledMatch.filter(function(timeToDisable) {
        return $.isArray(timeToDisable) && timeToDisable[2] == 'inverted' ||
          $.isPlainObject(timeToDisable) && timeToDisable.inverted
      }).length

      // If the clock is "enabled" flag is flipped, flip the condition.
      return clock.item.enable === -1 ? !isDisabledMatch : isDisabledMatch ||
        timeToVerify.pick < clock.item.min.pick ||
        timeToVerify.pick > clock.item.max.pick
    } //TimePicker.prototype.disabled


  /**
   * Shift an object by an interval until we reach an enabled object.
   */
  TimePicker.prototype.shift = function(timeObject, interval) {

      var clock = this,
        minLimit = clock.item.min.pick,
        maxLimit = clock.item.max.pick
        /*,
                safety = 1000*/

      interval = interval || clock.item.interval

      // Keep looping as long as the time is disabled.
      while ( /*safety &&*/ clock.disabled(timeObject)) {

        /*safety -= 1
        if ( !safety ) {
            throw 'Fell into an infinite loop while shifting to ' + timeObject.hour + ':' + timeObject.mins + '.'
        }*/

        // Increase/decrease the time by the interval and keep looping.
        timeObject = clock.create(timeObject.pick += interval)

        // If we've looped beyond the limits, break out of the loop.
        if (timeObject.pick <= minLimit || timeObject.pick >= maxLimit) {
          break
        }
      }

      // Return the final object.
      return timeObject
    } //TimePicker.prototype.shift


  /**
   * Scope an object to be within range of min and max.
   */
  TimePicker.prototype.scope = function(timeObject) {
      var minLimit = this.item.min.pick,
        maxLimit = this.item.max.pick
      return this.create(timeObject.pick > maxLimit ? maxLimit : timeObject.pick < minLimit ? minLimit : timeObject)
    } //TimePicker.prototype.scope


  /**
   * Parse a string into a usable type.
   */
  TimePicker.prototype.parse = function(type, value, options) {

      var hour, minutes, isPM, item, parseValue,
        clock = this,
        parsingObject = {}

      if (!value || _.isInteger(value) || $.isArray(value) || _.isDate(value) || $.isPlainObject(value) && _.isInteger(value.pick)) {
        return value
      }

      // We need a `.format` to parse the value with.
      if (!(options && options.format)) {
        options = options || {}
        options.format = clock.settings.format
      }

      // Convert the format into an array and then map through it.
      clock.formats.toArray(options.format).map(function(label) {

        var
          substring,

          // Grab the formatting label.
          formattingLabel = clock.formats[label],

          // The format length is from the formatting label function or the
          // label length without the escaping exclamation (!) mark.
          formatLength = formattingLabel ?
          _.trigger(formattingLabel, clock, [value, parsingObject]) :
          label.replace(/^!/, '').length

        // If there's a format label, split the value up to the format length.
        // Then add it to the parsing object with appropriate label.
        if (formattingLabel) {
          substring = value.substr(0, formatLength)
          parsingObject[label] = substring.match(/^\d+$/) ? +substring : substring
        }

        // Update the time value as the substring from format length to end.
        value = value.substr(formatLength)
      })

      // Grab the hour and minutes from the parsing object.
      for (item in parsingObject) {
        parseValue = parsingObject[item]
        if (_.isInteger(parseValue)) {
          if (item.match(/^(h|hh)$/i)) {
            hour = parseValue
            if (item == 'h' || item == 'hh') {
              hour %= 12
            }
          } else if (item == 'i') {
            minutes = parseValue
          }
        } else if (item.match(/^a$/i) && parseValue.match(/^p/i) && ('h' in parsingObject || 'hh' in parsingObject)) {
          isPM = true
        }
      }

      // Calculate it in minutes and return.
      return (isPM ? hour + 12 : hour) * MINUTES_IN_HOUR + minutes
    } //TimePicker.prototype.parse


  /**
   * Various formats to display the object in.
   */
  TimePicker.prototype.formats = {

      h: function(string, timeObject) {

        // If there's string, then get the digits length.
        // Otherwise return the selected hour in "standard" format.
        return string ? _.digits(string) : timeObject.hour % HOURS_TO_NOON || HOURS_TO_NOON
      },
      hh: function(string, timeObject) {

        // If there's a string, then the length is always 2.
        // Otherwise return the selected hour in "standard" format with a leading zero.
        return string ? 2 : _.lead(timeObject.hour % HOURS_TO_NOON || HOURS_TO_NOON)
      },
      H: function(string, timeObject) {

        // If there's string, then get the digits length.
        // Otherwise return the selected hour in "military" format as a string.
        return string ? _.digits(string) : '' + (timeObject.hour % 24)
      },
      HH: function(string, timeObject) {

        // If there's string, then get the digits length.
        // Otherwise return the selected hour in "military" format with a leading zero.
        return string ? _.digits(string) : _.lead(timeObject.hour % 24)
      },
      i: function(string, timeObject) {

        // If there's a string, then the length is always 2.
        // Otherwise return the selected minutes.
        return string ? 2 : _.lead(timeObject.mins)
      },
      a: function(string, timeObject) {

        // If there's a string, then the length is always 4.
        // Otherwise check if it's more than "noon" and return either am/pm.
        return string ? 4 : MINUTES_IN_DAY / 2 > timeObject.time % MINUTES_IN_DAY ? 'a.m.' : 'p.m.'
      },
      A: function(string, timeObject) {

        // If there's a string, then the length is always 2.
        // Otherwise check if it's more than "noon" and return either am/pm.
        return string ? 2 : MINUTES_IN_DAY / 2 > timeObject.time % MINUTES_IN_DAY ? 'AM' : 'PM'
      },

      // Create an array by splitting the formatting string passed.
      toArray: function(formatString) {
        return formatString.split(/(h{1,2}|H{1,2}|i|a|A|!.)/g)
      },

      // Format an object into a string using the formatting options.
      toString: function(formatString, itemObject) {
        var clock = this
        return clock.formats.toArray(formatString).map(function(label) {
          return _.trigger(clock.formats[label], clock, [0, itemObject]) || label.replace(/^!/, '')
        }).join('')
      }
    } //TimePicker.prototype.formats




  /**
   * Check if two time units are the exact.
   */
  TimePicker.prototype.isTimeExact = function(one, two) {

    var clock = this

    // When we're working with minutes, do a direct comparison.
    if (
      (_.isInteger(one) && _.isInteger(two)) ||
      (typeof one == 'boolean' && typeof two == 'boolean')
    ) {
      return one === two
    }

    // When we're working with time representations, compare the "pick" value.
    if (
      (_.isDate(one) || $.isArray(one)) &&
      (_.isDate(two) || $.isArray(two))
    ) {
      return clock.create(one).pick === clock.create(two).pick
    }

    // When we're working with range objects, compare the "from" and "to".
    if ($.isPlainObject(one) && $.isPlainObject(two)) {
      return clock.isTimeExact(one.from, two.from) && clock.isTimeExact(one.to, two.to)
    }

    return false
  }


  /**
   * Check if two time units overlap.
   */
  TimePicker.prototype.isTimeOverlap = function(one, two) {

    var clock = this

    // When we're working with an integer, compare the hours.
    if (_.isInteger(one) && (_.isDate(two) || $.isArray(two))) {
      return one === clock.create(two).hour
    }
    if (_.isInteger(two) && (_.isDate(one) || $.isArray(one))) {
      return two === clock.create(one).hour
    }

    // When we're working with range objects, check if the ranges overlap.
    if ($.isPlainObject(one) && $.isPlainObject(two)) {
      return clock.overlapRanges(one, two)
    }

    return false
  }


  /**
   * Flip the "enabled" state.
   */
  TimePicker.prototype.flipEnable = function(val) {
    var itemObject = this.item
    itemObject.enable = val || (itemObject.enable == -1 ? 1 : -1)
  }


  /**
   * Mark a collection of times as "disabled".
   */
  TimePicker.prototype.deactivate = function(type, timesToDisable) {

      var clock = this,
        disabledItems = clock.item.disable.slice(0)


      // If we're flipping, that's all we need to do.
      if (timesToDisable == 'flip') {
        clock.flipEnable()
      } else if (timesToDisable === false) {
        clock.flipEnable(1)
        disabledItems = []
      } else if (timesToDisable === true) {
        clock.flipEnable(-1)
        disabledItems = []
      }

      // Otherwise go through the times to disable.
      else {

        timesToDisable.map(function(unitToDisable) {

          var matchFound

          // When we have disabled items, check for matches.
          // If something is matched, immediately break out.
          for (var index = 0; index < disabledItems.length; index += 1) {
            if (clock.isTimeExact(unitToDisable, disabledItems[index])) {
              matchFound = true
              break
            }
          }

          // If nothing was found, add the validated unit to the collection.
          if (!matchFound) {
            if (
              _.isInteger(unitToDisable) ||
              _.isDate(unitToDisable) ||
              $.isArray(unitToDisable) ||
              ($.isPlainObject(unitToDisable) && unitToDisable.from && unitToDisable.to)
            ) {
              disabledItems.push(unitToDisable)
            }
          }
        })
      }

      // Return the updated collection.
      return disabledItems
    } //TimePicker.prototype.deactivate


  /**
   * Mark a collection of times as "enabled".
   */
  TimePicker.prototype.activate = function(type, timesToEnable) {

      var clock = this,
        disabledItems = clock.item.disable,
        disabledItemsCount = disabledItems.length

      // If we're flipping, that's all we need to do.
      if (timesToEnable == 'flip') {
        clock.flipEnable()
      } else if (timesToEnable === true) {
        clock.flipEnable(1)
        disabledItems = []
      } else if (timesToEnable === false) {
        clock.flipEnable(-1)
        disabledItems = []
      }

      // Otherwise go through the disabled times.
      else {

        timesToEnable.map(function(unitToEnable) {

          var matchFound,
            disabledUnit,
            index,
            isRangeMatched

          // Go through the disabled items and try to find a match.
          for (index = 0; index < disabledItemsCount; index += 1) {

            disabledUnit = disabledItems[index]

            // When an exact match is found, remove it from the collection.
            if (clock.isTimeExact(disabledUnit, unitToEnable)) {
              matchFound = disabledItems[index] = null
              isRangeMatched = true
              break
            }

            // When an overlapped match is found, add the "inverted" state to it.
            else if (clock.isTimeOverlap(disabledUnit, unitToEnable)) {
              if ($.isPlainObject(unitToEnable)) {
                unitToEnable.inverted = true
                matchFound = unitToEnable
              } else if ($.isArray(unitToEnable)) {
                matchFound = unitToEnable
                if (!matchFound[2]) matchFound.push('inverted')
              } else if (_.isDate(unitToEnable)) {
                matchFound = [unitToEnable.getFullYear(), unitToEnable.getMonth(), unitToEnable.getDate(), 'inverted']
              }
              break
            }
          }

          // If a match was found, remove a previous duplicate entry.
          if (matchFound)
            for (index = 0; index < disabledItemsCount; index += 1) {
              if (clock.isTimeExact(disabledItems[index], unitToEnable)) {
                disabledItems[index] = null
                break
              }
            }

          // In the event that we're dealing with an overlap of range times,
          // make sure there are no "inverted" times because of it.
          if (isRangeMatched)
            for (index = 0; index < disabledItemsCount; index += 1) {
              if (clock.isTimeOverlap(disabledItems[index], unitToEnable)) {
                disabledItems[index] = null
                break
              }
            }

          // If something is still matched, add it into the collection.
          if (matchFound) {
            disabledItems.push(matchFound)
          }
        })
      }

      // Return the updated collection.
      return disabledItems.filter(function(val) {
        return val != null
      })
    } //TimePicker.prototype.activate


  /**
   * The division to use for the range intervals.
   */
  TimePicker.prototype.i = function(type, value /*, options*/ ) {
    return _.isInteger(value) && value > 0 ? value : this.item.interval
  }


  /**
   * Create a string for the nodes in the picker.
   */
  TimePicker.prototype.nodes = function(isOpen) {

      var
        clock = this,
        settings = clock.settings,
        selectedObject = clock.item.select,
        highlightedObject = clock.item.highlight,
        viewsetObject = clock.item.view,
        disabledCollection = clock.item.disable

      return _.node(
        'ul',
        _.group({
          min: clock.item.min.pick,
          max: clock.item.max.pick,
          i: clock.item.interval,
          node: 'li',
          item: function(loopedTime) {
            loopedTime = clock.create(loopedTime)
            var timeMinutes = loopedTime.pick,
              isSelected = selectedObject && selectedObject.pick == timeMinutes,
              isHighlighted = highlightedObject && highlightedObject.pick == timeMinutes,
              isDisabled = disabledCollection && clock.disabled(loopedTime)
            return [
              _.trigger(clock.formats.toString, clock, [_.trigger(settings.formatLabel, clock, [loopedTime]) || settings.format, loopedTime]), (function(klasses) {

                if (isSelected) {
                  klasses.push(settings.klass.selected)
                }

                if (isHighlighted) {
                  klasses.push(settings.klass.highlighted)
                }

                if (viewsetObject && viewsetObject.pick == timeMinutes) {
                  klasses.push(settings.klass.viewset)
                }

                if (isDisabled) {
                  klasses.push(settings.klass.disabled)
                }

                return klasses.join(' ')
              })([settings.klass.listItem]),
              'data-pick=' + loopedTime.pick + ' ' + _.ariaAttr({
                role: 'button',
                controls: clock.$node[0].id,
                checked: isSelected && clock.$node.val() === _.trigger(
                  clock.formats.toString,
                  clock, [settings.format, loopedTime]
                ) ? true : null,
                activedescendant: isHighlighted ? true : null,
                disabled: isDisabled ? true : null
              })
            ]
          }
        }) +

        // * For Firefox forms to submit, make sure to set the button's `type` attribute as "button".
        _.node(
          'li',
          _.node(
            'button',
            settings.clear,
            settings.klass.buttonClear,
            'type=button data-clear=1' + (isOpen ? '' : ' disable')
          )
        ),
        settings.klass.list
      )
    } //TimePicker.prototype.nodes




  /* ==========================================================================
     Extend the picker to add the component with the defaults.
     ========================================================================== */

  TimePicker.defaults = (function(prefix) {

    return {

      // Clear
      clear: 'Clear',

      // The format to show on the `input` element
      format: 'h:i A',

      // The interval between each time
      interval: 30,

      // Classes
      klass: {

        picker: prefix + ' ' + prefix + '--time',
        holder: prefix + '__holder',

        list: prefix + '__list',
        listItem: prefix + '__list-item',

        disabled: prefix + '__list-item--disabled',
        selected: prefix + '__list-item--selected',
        highlighted: prefix + '__list-item--highlighted',
        viewset: prefix + '__list-item--viewset',
        now: prefix + '__list-item--now',

        buttonClear: prefix + '__button--clear'
      }
    }
  })(Picker.klasses().picker)




  /**
   * Extend the picker to add the time picker.
   */
  Picker.extend('pickatime', TimePicker)


}));




//     Furnax Plugin 08

/**
 * List.js
 * https://github.com/javve/list.js
 */


;
(function() {

  /**
   * Require the given path.
   *
   * @param {String} path
   * @return {Object} exports
   * @api public
   */

  function require(path, parent, orig) {
    var resolved = require.resolve(path);

    // lookup failed
    if (null == resolved) {
      orig = orig || path;
      parent = parent || 'root';
      var err = new Error('Failed to require "' + orig + '" from "' + parent + '"');
      err.path = orig;
      err.parent = parent;
      err.require = true;
      throw err;
    }

    var module = require.modules[resolved];

    // perform real require()
    // by invoking the module's
    // registered function
    if (!module._resolving && !module.exports) {
      var mod = {};
      mod.exports = {};
      mod.client = mod.component = true;
      module._resolving = true;
      module.call(this, mod.exports, require.relative(resolved), mod);
      delete module._resolving;
      module.exports = mod.exports;
    }

    return module.exports;
  }

  /**
   * Registered modules.
   */

  require.modules = {};

  /**
   * Registered aliases.
   */

  require.aliases = {};

  /**
   * Resolve `path`.
   *
   * Lookup:
   *
   *   - PATH/index.js
   *   - PATH.js
   *   - PATH
   *
   * @param {String} path
   * @return {String} path or null
   * @api private
   */

  require.resolve = function(path) {
    if (path.charAt(0) === '/') path = path.slice(1);

    var paths = [
      path,
      path + '.js',
      path + '.json',
      path + '/index.js',
      path + '/index.json'
    ];

    for (var i = 0; i < paths.length; i++) {
      var path = paths[i];
      if (require.modules.hasOwnProperty(path)) return path;
      if (require.aliases.hasOwnProperty(path)) return require.aliases[path];
    }
  };

  /**
   * Normalize `path` relative to the current path.
   *
   * @param {String} curr
   * @param {String} path
   * @return {String}
   * @api private
   */

  require.normalize = function(curr, path) {
    var segs = [];

    if ('.' != path.charAt(0)) return path;

    curr = curr.split('/');
    path = path.split('/');

    for (var i = 0; i < path.length; ++i) {
      if ('..' == path[i]) {
        curr.pop();
      } else if ('.' != path[i] && '' != path[i]) {
        segs.push(path[i]);
      }
    }

    return curr.concat(segs).join('/');
  };

  /**
   * Register module at `path` with callback `definition`.
   *
   * @param {String} path
   * @param {Function} definition
   * @api private
   */

  require.register = function(path, definition) {
    require.modules[path] = definition;
  };

  /**
   * Alias a module definition.
   *
   * @param {String} from
   * @param {String} to
   * @api private
   */

  require.alias = function(from, to) {
    if (!require.modules.hasOwnProperty(from)) {
      throw new Error('Failed to alias "' + from + '", it does not exist');
    }
    require.aliases[to] = from;
  };

  /**
   * Return a require function relative to the `parent` path.
   *
   * @param {String} parent
   * @return {Function}
   * @api private
   */

  require.relative = function(parent) {
    var p = require.normalize(parent, '..');

    /**
     * lastIndexOf helper.
     */

    function lastIndexOf(arr, obj) {
      var i = arr.length;
      while (i--) {
        if (arr[i] === obj) return i;
      }
      return -1;
    }

    /**
     * The relative require() itself.
     */

    function localRequire(path) {
      var resolved = localRequire.resolve(path);
      return require(resolved, parent, path);
    }

    /**
     * Resolve relative to the parent.
     */

    localRequire.resolve = function(path) {
      var c = path.charAt(0);
      if ('/' == c) return path.slice(1);
      if ('.' == c) return require.normalize(p, path);

      // resolve deps by returning
      // the dep in the nearest "deps"
      // directory
      var segs = parent.split('/');
      var i = lastIndexOf(segs, 'deps') + 1;
      if (!i) i = 0;
      path = segs.slice(0, i + 1).join('/') + '/deps/' + path;
      return path;
    };

    /**
     * Check if module is defined at `path`.
     */

    localRequire.exists = function(path) {
      return require.modules.hasOwnProperty(localRequire.resolve(path));
    };

    return localRequire;
  };
  require.register("component-classes/index.js", function(exports, require, module) {
    /**
     * Module dependencies.
     */

    var index = require('indexof');

    /**
     * Whitespace regexp.
     */

    var re = /\s+/;

    /**
     * toString reference.
     */

    var toString = Object.prototype.toString;

    /**
     * Wrap `el` in a `ClassList`.
     *
     * @param {Element} el
     * @return {ClassList}
     * @api public
     */

    module.exports = function(el) {
      return new ClassList(el);
    };

    /**
     * Initialize a new ClassList for `el`.
     *
     * @param {Element} el
     * @api private
     */

    function ClassList(el) {
      if (!el) throw new Error('A DOM element reference is required');
      this.el = el;
      this.list = el.classList;
    }

    /**
     * Add class `name` if not already present.
     *
     * @param {String} name
     * @return {ClassList}
     * @api public
     */

    ClassList.prototype.add = function(name) {
      // classList
      if (this.list) {
        this.list.add(name);
        return this;
      }

      // fallback
      var arr = this.array();
      var i = index(arr, name);
      if (!~i) arr.push(name);
      this.el.className = arr.join(' ');
      return this;
    };

    /**
     * Remove class `name` when present, or
     * pass a regular expression to remove
     * any which match.
     *
     * @param {String|RegExp} name
     * @return {ClassList}
     * @api public
     */

    ClassList.prototype.remove = function(name) {
      if ('[object RegExp]' == toString.call(name)) {
        return this.removeMatching(name);
      }

      // classList
      if (this.list) {
        this.list.remove(name);
        return this;
      }

      // fallback
      var arr = this.array();
      var i = index(arr, name);
      if (~i) arr.splice(i, 1);
      this.el.className = arr.join(' ');
      return this;
    };

    /**
     * Remove all classes matching `re`.
     *
     * @param {RegExp} re
     * @return {ClassList}
     * @api private
     */

    ClassList.prototype.removeMatching = function(re) {
      var arr = this.array();
      for (var i = 0; i < arr.length; i++) {
        if (re.test(arr[i])) {
          this.remove(arr[i]);
        }
      }
      return this;
    };

    /**
     * Toggle class `name`, can force state via `force`.
     *
     * For browsers that support classList, but do not support `force` yet,
     * the mistake will be detected and corrected.
     *
     * @param {String} name
     * @param {Boolean} force
     * @return {ClassList}
     * @api public
     */

    ClassList.prototype.toggle = function(name, force) {
      // classList
      if (this.list) {
        if ("undefined" !== typeof force) {
          if (force !== this.list.toggle(name, force)) {
            this.list.toggle(name); // toggle again to correct
          }
        } else {
          this.list.toggle(name);
        }
        return this;
      }

      // fallback
      if ("undefined" !== typeof force) {
        if (!force) {
          this.remove(name);
        } else {
          this.add(name);
        }
      } else {
        if (this.has(name)) {
          this.remove(name);
        } else {
          this.add(name);
        }
      }

      return this;
    };

    /**
     * Return an array of classes.
     *
     * @return {Array}
     * @api public
     */

    ClassList.prototype.array = function() {
      var str = this.el.className.replace(/^\s+|\s+$/g, '');
      var arr = str.split(re);
      if ('' === arr[0]) arr.shift();
      return arr;
    };

    /**
     * Check if class `name` is present.
     *
     * @param {String} name
     * @return {ClassList}
     * @api public
     */

    ClassList.prototype.has =
      ClassList.prototype.contains = function(name) {
        return this.list ? this.list.contains(name) : !!~index(this.array(), name);
      };

  });
  require.register("segmentio-extend/index.js", function(exports, require, module) {

    module.exports = function extend(object) {
      // Takes an unlimited number of extenders.
      var args = Array.prototype.slice.call(arguments, 1);

      // For each extender, copy their properties on our object.
      for (var i = 0, source; source = args[i]; i++) {
        if (!source) continue;
        for (var property in source) {
          object[property] = source[property];
        }
      }

      return object;
    };
  });
  require.register("component-indexof/index.js", function(exports, require, module) {
    module.exports = function(arr, obj) {
      if (arr.indexOf) return arr.indexOf(obj);
      for (var i = 0; i < arr.length; ++i) {
        if (arr[i] === obj) return i;
      }
      return -1;
    };
  });
  require.register("component-event/index.js", function(exports, require, module) {
    var bind = window.addEventListener ? 'addEventListener' : 'attachEvent',
      unbind = window.removeEventListener ? 'removeEventListener' : 'detachEvent',
      prefix = bind !== 'addEventListener' ? 'on' : '';

    /**
     * Bind `el` event `type` to `fn`.
     *
     * @param {Element} el
     * @param {String} type
     * @param {Function} fn
     * @param {Boolean} capture
     * @return {Function}
     * @api public
     */

    exports.bind = function(el, type, fn, capture) {
      el[bind](prefix + type, fn, capture || false);
      return fn;
    };

    /**
     * Unbind `el` event `type`'s callback `fn`.
     *
     * @param {Element} el
     * @param {String} type
     * @param {Function} fn
     * @param {Boolean} capture
     * @return {Function}
     * @api public
     */

    exports.unbind = function(el, type, fn, capture) {
      el[unbind](prefix + type, fn, capture || false);
      return fn;
    };
  });
  require.register("timoxley-to-array/index.js", function(exports, require, module) {
    /**
     * Convert an array-like object into an `Array`.
     * If `collection` is already an `Array`, then will return a clone of `collection`.
     *
     * @param {Array | Mixed} collection An `Array` or array-like object to convert e.g. `arguments` or `NodeList`
     * @return {Array} Naive conversion of `collection` to a new `Array`.
     * @api public
     */

    module.exports = function toArray(collection) {
      if (typeof collection === 'undefined') return []
      if (collection === null) return [null]
      if (collection === window) return [window]
      if (typeof collection === 'string') return [collection]
      if (isArray(collection)) return collection
      if (typeof collection.length != 'number') return [collection]
      if (typeof collection === 'function' && collection instanceof Function) return [collection]

      var arr = []
      for (var i = 0; i < collection.length; i++) {
        if (Object.prototype.hasOwnProperty.call(collection, i) || i in collection) {
          arr.push(collection[i])
        }
      }
      if (!arr.length) return []
      return arr
    }

    function isArray(arr) {
      return Object.prototype.toString.call(arr) === "[object Array]";
    }

  });
  require.register("javve-events/index.js", function(exports, require, module) {
    var events = require('event'),
      toArray = require('to-array');

    /**
     * Bind `el` event `type` to `fn`.
     *
     * @param {Element} el, NodeList, HTMLCollection or Array
     * @param {String} type
     * @param {Function} fn
     * @param {Boolean} capture
     * @api public
     */

    exports.bind = function(el, type, fn, capture) {
      el = toArray(el);
      for (var i = 0; i < el.length; i++) {
        events.bind(el[i], type, fn, capture);
      }
    };

    /**
     * Unbind `el` event `type`'s callback `fn`.
     *
     * @param {Element} el, NodeList, HTMLCollection or Array
     * @param {String} type
     * @param {Function} fn
     * @param {Boolean} capture
     * @api public
     */

    exports.unbind = function(el, type, fn, capture) {
      el = toArray(el);
      for (var i = 0; i < el.length; i++) {
        events.unbind(el[i], type, fn, capture);
      }
    };

  });
  require.register("javve-get-by-class/index.js", function(exports, require, module) {
    /**
     * Find all elements with class `className` inside `container`.
     * Use `single = true` to increase performance in older browsers
     * when only one element is needed.
     *
     * @param {String} className
     * @param {Element} container
     * @param {Boolean} single
     * @api public
     */

    module.exports = (function() {
      if (document.getElementsByClassName) {
        return function(container, className, single) {
          if (single) {
            return container.getElementsByClassName(className)[0];
          } else {
            return container.getElementsByClassName(className);
          }
        };
      } else if (document.querySelector) {
        return function(container, className, single) {
          className = '.' + className;
          if (single) {
            return container.querySelector(className);
          } else {
            return container.querySelectorAll(className);
          }
        };
      } else {
        return function(container, className, single) {
          var classElements = [],
            tag = '*';
          if (container == null) {
            container = document;
          }
          var els = container.getElementsByTagName(tag);
          var elsLen = els.length;
          var pattern = new RegExp("(^|\\s)" + className + "(\\s|$)");
          for (var i = 0, j = 0; i < elsLen; i++) {
            if (pattern.test(els[i].className)) {
              if (single) {
                return els[i];
              } else {
                classElements[j] = els[i];
                j++;
              }
            }
          }
          return classElements;
        };
      }
    })();

  });
  require.register("javve-get-attribute/index.js", function(exports, require, module) {
    /**
     * Return the value for `attr` at `element`.
     *
     * @param {Element} el
     * @param {String} attr
     * @api public
     */

    module.exports = function(el, attr) {
      var result = (el.getAttribute && el.getAttribute(attr)) || null;
      if (!result) {
        var attrs = el.attributes;
        var length = attrs.length;
        for (var i = 0; i < length; i++) {
          if (attr[i] !== undefined) {
            if (attr[i].nodeName === attr) {
              result = attr[i].nodeValue;
            }
          }
        }
      }
      return result;
    }
  });
  require.register("javve-natural-sort/index.js", function(exports, require, module) {
    /*
     * Natural Sort algorithm for Javascript - Version 0.7 - Released under MIT license
     * Author: Jim Palmer (based on chunking idea from Dave Koelle)
     */

    module.exports = function(a, b, options) {
      var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
        sre = /(^[ ]*|[ ]*$)/g,
        dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
        hre = /^0x[0-9a-f]+$/i,
        ore = /^0/,
        options = options || {},
        i = function(s) {
          return options.insensitive && ('' + s).toLowerCase() || '' + s
        },
        // convert all to strings strip whitespace
        x = i(a).replace(sre, '') || '',
        y = i(b).replace(sre, '') || '',
        // chunk/tokenize
        xN = x.replace(re, '\0$1\0').replace(/\0$/, '').replace(/^\0/, '').split('\0'),
        yN = y.replace(re, '\0$1\0').replace(/\0$/, '').replace(/^\0/, '').split('\0'),
        // numeric, hex or date detection
        xD = parseInt(x.match(hre)) || (xN.length != 1 && x.match(dre) && Date.parse(x)),
        yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null,
        oFxNcL, oFyNcL,
        mult = options.desc ? -1 : 1;
      // first try and sort Hex codes or Dates
      if (yD)
        if (xD < yD) return -1 * mult;
        else if (xD > yD) return 1 * mult;
      // natural sorting through split numeric strings and default strings
      for (var cLoc = 0, numS = Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
        // find floats not starting with '0', string or 0 if not defined (Clint Priest)
        oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
        oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
        // handle numeric vs string comparison - number < string - (Kyle Adams)
        if (isNaN(oFxNcL) !== isNaN(oFyNcL)) {
          return (isNaN(oFxNcL)) ? 1 : -1;
        }
        // rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
        else if (typeof oFxNcL !== typeof oFyNcL) {
          oFxNcL += '';
          oFyNcL += '';
        }
        if (oFxNcL < oFyNcL) return -1 * mult;
        if (oFxNcL > oFyNcL) return 1 * mult;
      }
      return 0;
    };

    /*
    var defaultSort = getSortFunction();

    module.exports = function(a, b, options) {
      if (arguments.length == 1) {
        options = a;
        return getSortFunction(options);
      } else {
        return defaultSort(a,b);
      }
    }
    */
  });
  require.register("javve-to-string/index.js", function(exports, require, module) {
    module.exports = function(s) {
      s = (s === undefined) ? "" : s;
      s = (s === null) ? "" : s;
      s = s.toString();
      return s;
    };

  });
  require.register("component-type/index.js", function(exports, require, module) {
    /**
     * toString ref.
     */

    var toString = Object.prototype.toString;

    /**
     * Return the type of `val`.
     *
     * @param {Mixed} val
     * @return {String}
     * @api public
     */

    module.exports = function(val) {
      switch (toString.call(val)) {
        case '[object Date]':
          return 'date';
        case '[object RegExp]':
          return 'regexp';
        case '[object Arguments]':
          return 'arguments';
        case '[object Array]':
          return 'array';
        case '[object Error]':
          return 'error';
      }

      if (val === null) return 'null';
      if (val === undefined) return 'undefined';
      if (val !== val) return 'nan';
      if (val && val.nodeType === 1) return 'element';

      return typeof val.valueOf();
    };

  });
  require.register("list.js/index.js", function(exports, require, module) {
    /*
    ListJS with beta 1.0.0
    By Jonny Strömberg (www.jonnystromberg.com, www.listjs.com)
    */
    (function(window, undefined) {
      "use strict";

      var document = window.document,
        getByClass = require('get-by-class'),
        extend = require('extend'),
        indexOf = require('indexof');

      var List = function(id, options, values) {

        var self = this,
          init,
          Item = require('./src/item')(self),
          addAsync = require('./src/add-async')(self),
          parse = require('./src/parse')(self);

        init = {
          start: function() {
            self.listClass = "list";
            self.searchClass = "search";
            self.sortClass = "sort";
            self.page = 200;
            self.i = 1;
            self.items = [];
            self.visibleItems = [];
            self.matchingItems = [];
            self.searched = false;
            self.filtered = false;
            self.handlers = {
              'updated': []
            };
            self.plugins = {};
            self.helpers = {
              getByClass: getByClass,
              extend: extend,
              indexOf: indexOf
            };

            extend(self, options);

            self.listContainer = (typeof(id) === 'string') ? document.getElementById(id) : id;
            if (!self.listContainer) {
              return;
            }
            self.list = getByClass(self.listContainer, self.listClass, true);

            self.templater = require('./src/templater')(self);
            self.search = require('./src/search')(self);
            self.filter = require('./src/filter')(self);
            self.sort = require('./src/sort')(self);

            this.items();
            self.update();
            this.plugins();
          },
          items: function() {
            parse(self.list);
            if (values !== undefined) {
              self.add(values);
            }
          },
          plugins: function() {
            for (var i = 0; i < self.plugins.length; i++) {
              var plugin = self.plugins[i];
              self[plugin.name] = plugin;
              plugin.init(self);
            }
          }
        };


        /*
         * Add object to list
         */
        this.add = function(values, callback) {
          if (callback) {
            addAsync(values, callback);
            return;
          }
          var added = [],
            notCreate = false;
          if (values[0] === undefined) {
            values = [values];
          }
          for (var i = 0, il = values.length; i < il; i++) {
            var item = null;
            if (values[i] instanceof Item) {
              item = values[i];
              item.reload();
            } else {
              notCreate = (self.items.length > self.page) ? true : false;
              item = new Item(values[i], undefined, notCreate);
            }
            self.items.push(item);
            added.push(item);
          }
          self.update();
          return added;
        };

        this.show = function(i, page) {
          this.i = i;
          this.page = page;
          self.update();
          return self;
        };

        /* Removes object from list.
         * Loops through the list and removes objects where
         * property "valuename" === value
         */
        this.remove = function(valueName, value, options) {
          var found = 0;
          for (var i = 0, il = self.items.length; i < il; i++) {
            if (self.items[i].values()[valueName] == value) {
              self.templater.remove(self.items[i], options);
              self.items.splice(i, 1);
              il--;
              i--;
              found++;
            }
          }
          self.update();
          return found;
        };

        /* Gets the objects in the list which
         * property "valueName" === value
         */
        this.get = function(valueName, value) {
          var matchedItems = [];
          for (var i = 0, il = self.items.length; i < il; i++) {
            var item = self.items[i];
            if (item.values()[valueName] == value) {
              matchedItems.push(item);
            }
          }
          return matchedItems;
        };

        /*
         * Get size of the list
         */
        this.size = function() {
          return self.items.length;
        };

        /*
         * Removes all items from the list
         */
        this.clear = function() {
          self.templater.clear();
          self.items = [];
          return self;
        };

        this.on = function(event, callback) {
          self.handlers[event].push(callback);
          return self;
        };

        this.off = function(event, callback) {
          var e = self.handlers[event];
          var index = indexOf(e, callback);
          if (index > -1) {
            e.splice(index, 1);
          }
          return self;
        };

        this.trigger = function(event) {
          var i = self.handlers[event].length;
          while (i--) {
            self.handlers[event][i](self);
          }
          return self;
        };

        this.reset = {
          filter: function() {
            var is = self.items,
              il = is.length;
            while (il--) {
              is[il].filtered = false;
            }
            return self;
          },
          search: function() {
            var is = self.items,
              il = is.length;
            while (il--) {
              is[il].found = false;
            }
            return self;
          }
        };

        this.update = function() {
          var is = self.items,
            il = is.length;

          self.visibleItems = [];
          self.matchingItems = [];
          self.templater.clear();
          for (var i = 0; i < il; i++) {
            if (is[i].matching() && ((self.matchingItems.length + 1) >= self.i && self.visibleItems.length < self.page)) {
              is[i].show();
              self.visibleItems.push(is[i]);
              self.matchingItems.push(is[i]);
            } else if (is[i].matching()) {
              self.matchingItems.push(is[i]);
              is[i].hide();
            } else {
              is[i].hide();
            }
          }
          self.trigger('updated');
          return self;
        };

        init.start();
      };

      module.exports = List;

    })(window);

  });
  require.register("list.js/src/search.js", function(exports, require, module) {
    var events = require('events'),
      getByClass = require('get-by-class'),
      toString = require('to-string');

    module.exports = function(list) {
      var item,
        text,
        columns,
        searchString,
        customSearch;

      var prepare = {
        resetList: function() {
          list.i = 1;
          list.templater.clear();
          customSearch = undefined;
        },
        setOptions: function(args) {
          if (args.length == 2 && args[1] instanceof Array) {
            columns = args[1];
          } else if (args.length == 2 && typeof(args[1]) == "function") {
            customSearch = args[1];
          } else if (args.length == 3) {
            columns = args[1];
            customSearch = args[2];
          }
        },
        setColumns: function() {
          columns = (columns === undefined) ? prepare.toArray(list.items[0].values()) : columns;
        },
        setSearchString: function(s) {
          s = toString(s).toLowerCase();
          s = s.replace(/[-[\]{}()*+?.,\\^$|#]/g, "\\$&"); // Escape regular expression characters
          searchString = s;
        },
        toArray: function(values) {
          var tmpColumn = [];
          for (var name in values) {
            tmpColumn.push(name);
          }
          return tmpColumn;
        }
      };
      var search = {
        list: function() {
          for (var k = 0, kl = list.items.length; k < kl; k++) {
            search.item(list.items[k]);
          }
        },
        item: function(item) {
          item.found = false;
          for (var j = 0, jl = columns.length; j < jl; j++) {
            if (search.values(item.values(), columns[j])) {
              item.found = true;
              return;
            }
          }
        },
        values: function(values, column) {
          if (values.hasOwnProperty(column)) {
            text = toString(values[column]).toLowerCase();
            if ((searchString !== "") && (text.search(searchString) > -1)) {
              return true;
            }
          }
          return false;
        },
        reset: function() {
          list.reset.search();
          list.searched = false;
        }
      };

      var searchMethod = function(str) {
        list.trigger('searchStart');

        prepare.resetList();
        prepare.setSearchString(str);
        prepare.setOptions(arguments); // str, cols|searchFunction, searchFunction
        prepare.setColumns();

        if (searchString === "") {
          search.reset();
        } else {
          list.searched = true;
          if (customSearch) {
            customSearch(searchString, columns);
          } else {
            search.list();
          }
        }

        list.update();
        list.trigger('searchComplete');
        return list.visibleItems;
      };

      list.handlers.searchStart = list.handlers.searchStart || [];
      list.handlers.searchComplete = list.handlers.searchComplete || [];

      events.bind(getByClass(list.listContainer, list.searchClass), 'keyup', function(e) {
        var target = e.target || e.srcElement, // IE have srcElement
          alreadyCleared = (target.value === "" && !list.searched);
        if (!alreadyCleared) { // If oninput already have resetted the list, do nothing
          searchMethod(target.value);
        }
      });

      // Used to detect click on HTML5 clear button
      events.bind(getByClass(list.listContainer, list.searchClass), 'input', function(e) {
        var target = e.target || e.srcElement;
        if (target.value === "") {
          searchMethod('');
        }
      });

      list.helpers.toString = toString;
      return searchMethod;
    };

  });
  require.register("list.js/src/sort.js", function(exports, require, module) {
    var naturalSort = require('natural-sort'),
      classes = require('classes'),
      events = require('events'),
      getByClass = require('get-by-class'),
      getAttribute = require('get-attribute');

    module.exports = function(list) {
      list.sortFunction = list.sortFunction || function(itemA, itemB, options) {
        options.desc = options.order == "desc" ? true : false; // Natural sort uses this format
        return naturalSort(itemA.values()[options.valueName], itemB.values()[options.valueName], options);
      };

      var buttons = {
        els: undefined,
        clear: function() {
          for (var i = 0, il = buttons.els.length; i < il; i++) {
            classes(buttons.els[i]).remove('asc');
            classes(buttons.els[i]).remove('desc');
          }
        },
        getOrder: function(btn) {
          var predefinedOrder = getAttribute(btn, 'data-order');
          if (predefinedOrder == "asc" || predefinedOrder == "desc") {
            return predefinedOrder;
          } else if (classes(btn).has('desc')) {
            return "asc";
          } else if (classes(btn).has('asc')) {
            return "desc";
          } else {
            return "asc";
          }
        },
        getInSensitive: function(btn, options) {
          var insensitive = getAttribute(btn, 'data-insensitive');
          if (insensitive === "true") {
            options.insensitive = true;
          } else {
            options.insensitive = false;
          }
        },
        setOrder: function(options) {
          for (var i = 0, il = buttons.els.length; i < il; i++) {
            var btn = buttons.els[i];
            if (getAttribute(btn, 'data-sort') !== options.valueName) {
              continue;
            }
            var predefinedOrder = getAttribute(btn, 'data-order');
            if (predefinedOrder == "asc" || predefinedOrder == "desc") {
              if (predefinedOrder == options.order) {
                classes(btn).add(options.order);
              }
            } else {
              classes(btn).add(options.order);
            }
          }
        }
      };
      var sort = function() {
        list.trigger('sortStart');
        options = {};

        var target = arguments[0].currentTarget || arguments[0].srcElement || undefined;

        if (target) {
          options.valueName = getAttribute(target, 'data-sort');
          buttons.getInSensitive(target, options);
          options.order = buttons.getOrder(target);
        } else {
          options = arguments[1] || options;
          options.valueName = arguments[0];
          options.order = options.order || "asc";
          options.insensitive = (typeof options.insensitive == "undefined") ? true : options.insensitive;
        }
        buttons.clear();
        buttons.setOrder(options);

        options.sortFunction = options.sortFunction || list.sortFunction;
        list.items.sort(function(a, b) {
          return options.sortFunction(a, b, options);
        });
        list.update();
        list.trigger('sortComplete');
      };

      // Add handlers
      list.handlers.sortStart = list.handlers.sortStart || [];
      list.handlers.sortComplete = list.handlers.sortComplete || [];

      buttons.els = getByClass(list.listContainer, list.sortClass);
      events.bind(buttons.els, 'click', sort);
      list.on('searchStart', buttons.clear);
      list.on('filterStart', buttons.clear);

      // Helpers
      list.helpers.classes = classes;
      list.helpers.naturalSort = naturalSort;
      list.helpers.events = events;
      list.helpers.getAttribute = getAttribute;

      return sort;
    };

  });
  require.register("list.js/src/item.js", function(exports, require, module) {
    module.exports = function(list) {
      return function(initValues, element, notCreate) {
        var item = this;

        this._values = {};

        this.found = false; // Show if list.searched == true and this.found == true
        this.filtered = false; // Show if list.filtered == true and this.filtered == true

        var init = function(initValues, element, notCreate) {
          if (element === undefined) {
            if (notCreate) {
              item.values(initValues, notCreate);
            } else {
              item.values(initValues);
            }
          } else {
            item.elm = element;
            var values = list.templater.get(item, initValues);
            item.values(values);
          }
        };
        this.values = function(newValues, notCreate) {
          if (newValues !== undefined) {
            for (var name in newValues) {
              item._values[name] = newValues[name];
            }
            if (notCreate !== true) {
              list.templater.set(item, item.values());
            }
          } else {
            return item._values;
          }
        };
        this.show = function() {
          list.templater.show(item);
        };
        this.hide = function() {
          list.templater.hide(item);
        };
        this.matching = function() {
          return (
            (list.filtered && list.searched && item.found && item.filtered) ||
            (list.filtered && !list.searched && item.filtered) ||
            (!list.filtered && list.searched && item.found) ||
            (!list.filtered && !list.searched)
          );
        };
        this.visible = function() {
          return (item.elm.parentNode == list.list) ? true : false;
        };
        init(initValues, element, notCreate);
      };
    };

  });
  require.register("list.js/src/templater.js", function(exports, require, module) {
    var getByClass = require('get-by-class');

    var Templater = function(list) {
      var itemSource = getItemSource(list.item),
        templater = this;

      function getItemSource(item) {
        if (item === undefined) {
          var nodes = list.list.childNodes,
            items = [];

          for (var i = 0, il = nodes.length; i < il; i++) {
            // Only textnodes have a data attribute
            if (nodes[i].data === undefined) {
              return nodes[i];
            }
          }
          return null;
        } else if (item.indexOf("<") !== -1) { // Try create html element of list, do not work for tables!!
          var div = document.createElement('div');
          div.innerHTML = item;
          return div.firstChild;
        } else {
          return document.getElementById(list.item);
        }
      }

      /* Get values from element */
      this.get = function(item, valueNames) {
        templater.create(item);
        var values = {};
        for (var i = 0, il = valueNames.length; i < il; i++) {
          var elm = getByClass(item.elm, valueNames[i], true);
          values[valueNames[i]] = elm ? elm.innerHTML : "";
        }
        return values;
      };

      /* Sets values at element */
      this.set = function(item, values) {
        if (!templater.create(item)) {
          for (var v in values) {
            if (values.hasOwnProperty(v)) {
              // TODO speed up if possible
              var elm = getByClass(item.elm, v, true);
              if (elm) {
                /* src attribute for image tag & text for other tags */
                if (elm.tagName === "IMG" && values[v] !== "") {
                  elm.src = values[v];
                } else {
                  elm.innerHTML = values[v];
                }
              }
            }
          }
        }
      };

      this.create = function(item) {
        if (item.elm !== undefined) {
          return false;
        }
        /* If item source does not exists, use the first item in list as
        source for new items */
        var newItem = itemSource.cloneNode(true);
        newItem.removeAttribute('id');
        item.elm = newItem;
        templater.set(item, item.values());
        return true;
      };
      this.remove = function(item) {
        list.list.removeChild(item.elm);
      };
      this.show = function(item) {
        templater.create(item);
        list.list.appendChild(item.elm);
      };
      this.hide = function(item) {
        if (item.elm !== undefined && item.elm.parentNode === list.list) {
          list.list.removeChild(item.elm);
        }
      };
      this.clear = function() {
        /* .innerHTML = ''; fucks up IE */
        if (list.list.hasChildNodes()) {
          while (list.list.childNodes.length >= 1) {
            list.list.removeChild(list.list.firstChild);
          }
        }
      };
    };

    module.exports = function(list) {
      return new Templater(list);
    };

  });
  require.register("list.js/src/filter.js", function(exports, require, module) {
    module.exports = function(list) {

      // Add handlers
      list.handlers.filterStart = list.handlers.filterStart || [];
      list.handlers.filterComplete = list.handlers.filterComplete || [];

      return function(filterFunction) {
        list.trigger('filterStart');
        list.i = 1; // Reset paging
        list.reset.filter();
        if (filterFunction === undefined) {
          list.filtered = false;
        } else {
          list.filtered = true;
          var is = list.items;
          for (var i = 0, il = is.length; i < il; i++) {
            var item = is[i];
            if (filterFunction(item)) {
              item.filtered = true;
            } else {
              item.filtered = false;
            }
          }
        }
        list.update();
        list.trigger('filterComplete');
        return list.visibleItems;
      };
    };

  });
  require.register("list.js/src/add-async.js", function(exports, require, module) {
    module.exports = function(list) {
      return function(values, callback, items) {
        var valuesToAdd = values.splice(0, 100);
        items = items || [];
        items = items.concat(list.add(valuesToAdd));
        if (values.length > 0) {
          setTimeout(function() {
            addAsync(values, callback, items);
          }, 10);
        } else {
          list.update();
          callback(items);
        }
      };
    };
  });
  require.register("list.js/src/parse.js", function(exports, require, module) {
    module.exports = function(list) {

      var Item = require('./item')(list);

      var getChildren = function(parent) {
        var nodes = parent.childNodes,
          items = [];
        for (var i = 0, il = nodes.length; i < il; i++) {
          // Only textnodes have a data attribute
          if (nodes[i].data === undefined) {
            items.push(nodes[i]);
          }
        }
        return items;
      };

      var parse = function(itemElements, valueNames) {
        for (var i = 0, il = itemElements.length; i < il; i++) {
          list.items.push(new Item(valueNames, itemElements[i]));
        }
      };
      var parseAsync = function(itemElements, valueNames) {
        var itemsToIndex = itemElements.splice(0, 100); // TODO: If < 100 items, what happens in IE etc?
        parse(itemsToIndex, valueNames);
        if (itemElements.length > 0) {
          setTimeout(function() {
            init.items.indexAsync(itemElements, valueNames);
          }, 10);
        } else {
          list.update();
          // TODO: Add indexed callback
        }
      };

      return function() {
        var itemsToIndex = getChildren(list.list),
          valueNames = list.valueNames;

        if (list.indexAsync) {
          parseAsync(itemsToIndex, valueNames);
        } else {
          parse(itemsToIndex, valueNames);
        }
      };
    };

  });

  require.alias("component-classes/index.js", "list.js/deps/classes/index.js");
  require.alias("component-classes/index.js", "classes/index.js");
  require.alias("component-indexof/index.js", "component-classes/deps/indexof/index.js");

  require.alias("segmentio-extend/index.js", "list.js/deps/extend/index.js");
  require.alias("segmentio-extend/index.js", "extend/index.js");

  require.alias("component-indexof/index.js", "list.js/deps/indexof/index.js");
  require.alias("component-indexof/index.js", "indexof/index.js");

  require.alias("javve-events/index.js", "list.js/deps/events/index.js");
  require.alias("javve-events/index.js", "events/index.js");
  require.alias("component-event/index.js", "javve-events/deps/event/index.js");

  require.alias("timoxley-to-array/index.js", "javve-events/deps/to-array/index.js");

  require.alias("javve-get-by-class/index.js", "list.js/deps/get-by-class/index.js");
  require.alias("javve-get-by-class/index.js", "get-by-class/index.js");

  require.alias("javve-get-attribute/index.js", "list.js/deps/get-attribute/index.js");
  require.alias("javve-get-attribute/index.js", "get-attribute/index.js");

  require.alias("javve-natural-sort/index.js", "list.js/deps/natural-sort/index.js");
  require.alias("javve-natural-sort/index.js", "natural-sort/index.js");

  require.alias("javve-to-string/index.js", "list.js/deps/to-string/index.js");
  require.alias("javve-to-string/index.js", "list.js/deps/to-string/index.js");
  require.alias("javve-to-string/index.js", "to-string/index.js");
  require.alias("javve-to-string/index.js", "javve-to-string/index.js");
  require.alias("component-type/index.js", "list.js/deps/type/index.js");
  require.alias("component-type/index.js", "type/index.js");
  if (typeof exports == "object") {
    module.exports = require("list.js");
  } else if (typeof define == "function" && define.amd) {
    define(function() {
      return require("list.js");
    });
  } else {
    this["List"] = require("list.js");
  }
})();




//     Furnax Plugin 09

/*  
 *  Furnax Drag and Drop Module
 *
 *  USAGE: add "draggable" class to the elements that should be dragged around
 *  you can also add data-drag-direction="horizontal" or data-drag-direction="vertical"
 *  to bind the movement only in the selected direction
 *
 */


(function($) {

  var targetElement, dragEvent = {
    "mousedown": !$.os.tablet && !$.os.phone ? "mousedown" : "touchstart",
    "mouseup": !$.os.tablet && !$.os.phone ? "mouseup" : "touchend",
    "mousemove": !$.os.tablet && !$.os.phone ? "mousemove" : "touchmove"
  }

  if (window.navigator.msPointerEnabled) {
    dragEvent.mousedown = "MSPointerDown";
    dragEvent.mouseup = "MSPointerUp";
    dragEvent.mousemove = "MSPointerMove";
  }


  $(document).on(dragEvent.mousedown, ".draggable", startDragElement);
  $(document).on(dragEvent.mouseup, ".draggable", stopDragElement);


  function startDragElement(e) {
    e.preventDefault(); // this is used by desktop

    targetElement = this;
    if ($(targetElement).attr("data-drag-direction") === "horizontal") {
      targetElement.horizontal = true;
    } else if ($(targetElement).attr("data-drag-direction") === "vertical") {
      targetElement.vertical = true;
    } else {
      targetElement.horizontal, targetElement.vertical = false;
    }

    var transform = $(this).css("-webkit-transform") || $(this).css("-moz-transform") || $(this).css("-ms-transform") || $(this).css("transform");

    var matrix = matrixToArray(transform);

    targetElement.initTrX = matrix[4];
    targetElement.initTrY = matrix[5];

    targetElement.initMouseX = e.pageX || e.touches[0].pageX;
    targetElement.initMouseY = e.pageY || e.touches[0].pageY;

    document.addEventListener(dragEvent.mousemove, eleMouseMove, false);
  }


  function eleMouseMove(e) {

    e.preventDefault(); // this is used by mobile

    var dX = dY = trX = trY = 0;

    if (!$.os.android) {
      if (!targetElement.vertical) {
        dX = e.pageX - targetElement.initMouseX;
      }
      if (!targetElement.horizontal) {
        dY = e.pageY - targetElement.initMouseY;
      }
    } else if ($.os.android) {
      if (!targetElement.vertical) {
        dX = e.changedTouches[0].pageX - targetElement.initMouseX;
      }
      if (!targetElement.horizontal) {
        dY = e.changedTouches[0].pageY - targetElement.initMouseY;
      }
    }

    var trX = Number(targetElement.initTrX) + dX;
    var trY = Number(targetElement.initTrY) + dY;

    var motion = 'matrix(1, 0, 0, 1, ' + trX + ', ' + trY + ')';

    targetElement.style.webkitTransform =
      targetElement.style.msTransform =
      targetElement.style.MozTransform =
      targetElement.style.transform = motion;
  }

  function stopDragElement(e) {
    document.removeEventListener(dragEvent.mousemove, eleMouseMove, false);
    document.removeEventListener(dragEvent.mouseup, stopDragElement, false);
  }

  function matrixToArray(matrix) {
    if (matrix == "none") {
      matrix = "matrix(1, 0, 0, 1, 0, 0)";
    }
    return matrix.match(/(-?[0-9\.]+)/g);
    // fourth value in array will be X transform, fifth Y
  }

})(window.jQuery || window.Zepto, window, document);

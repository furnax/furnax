/*! *******************************************
 *
 *  FURNAX v 1.5
 *  © 2015 FORNACE SRL
 *  http://fornacestudio.com
 *
 * *******************************************/
/* jshint undef: false, unused: false, browser: true, jquery: true, devel: true */
/* global config, pqty, makeTimeIntervals */
;
(function($) {

  if (window.furnax && window.furnax.inited) {
    // app is already inited
    $("#navbar").css({
      "top": "0"
    });
    return;
  }

  // texts
  window.localStrings = {};

  window.localStrings.furnaxEn = {
    doneText: "Ok",
    cancelText: "Undo",
    popupTitle: "Message",
    popupMessage: "No message",
    install: {
      title: "Install this app!",
      step1: "1. Touch this icon <span></span> in Safari",
      step2: "2. Touch \"Add to Home\"",
      step3: "3. Touch \"Add\""
    },
    ajaxError: "We're sorry but an error happened while comunicating with the server. Please try again later.",
    update: {
      title: "Update Available",
      message: "An update for this app has been downloaded in background. Do you wish to apply it?",
      cancel: "Undo",
      confirm: "Apply"
    }
  }

  window.localStrings.furnaxIt = {
    doneText: "Ok",
    cancelText: "Annulla",
    popupTitle: "Messaggio",
    popupMessage: "Nessun Messaggio",
    install: {
      title: "Installa questa app!",
      step1: "1. Tocca questa icona <span></span> in Safari",
      step2: "2. Tocca \"Aggiungi a Home\"",
      step3: "3. Tocca \"Aggiungi\""
    },
    ajaxError: "Siamo spiacenti ma si è verificato un errore di comunicazione con il server. Si prega di riprovare più tardi.",
    update: {
      title: "Aggiornamento disponibile",
      message: "Un aggiornamento di questa app è stato scaricato in background. Desideri applicarlo ora?",
      cancel: "Annulla",
      confirm: "Aggiorna"
    }
  }

  window.furnax = {
    locale: (function() {
      userLang = window.navigator.languages ? window.navigator.languages[0] : null;
      userLang = userLang || window.navigator.language || window.navigator.browserLanguage || window.navigator.userLanguage;
      return userLang;
    })(),
    scrollvalue: 0,
    texts: localStrings.furnaxEn,
    load: function(dest, anim, back, isInit) {

      console.log("loading " + dest);

      var next = $(dest),
        current = $(".view.active");

      if (current.attr("id") !== next.attr("id") || isInit) {

        furnax.lockUi();

        var wait = 300;
        // standard animation times

        // save for history
        if (!isInit && !back) {
          history.pushState(null, null, dest);
          tempDb.setItem("prev", tempDb.getItem("prev") + "," + current.attr("id"));
        }

        if (!anim) {
          wait = 0;
        }

        $("input").blur(); // hide keyboard

        next.show();

        if (anim === "pushright") {
          wait = 500;
          // setup
          next.css({
            "-webkit-transform": "translate3d(100%,0,0)",
            "-webkit-transition": "-webkit-transform .5s cubic-bezier(.225, .5, .165, 1), box-shadow .1s linear",
            "-moz-transform": "translate3d(100%,0,0)",
            "-moz-transition": "-moz-transform .5s cubic-bezier(.225, .5, .165, 1), box-shadow .1s linear",
            "-ms-transform": "translate3d(100%,0,0)",
            "-ms-transition": "-ms-transform .5s cubic-bezier(.225, .5, .165, 1), box-shadow .1s linear",
            "transform": "translate3d(100%,0,0)",
            "transition": "transform .5s cubic-bezier(.225, .5, .165, 1), box-shadow .1s linear",
            "box-shadow": "0 0 0 rgba(0,0,0,0)",
            "dispay": "block"
          });
          current.css({
            "-webkit-transition": "-webkit-transform .3s cubic-bezier(.225, .5, .165, 1)",
            "-moz-transition": "-moz-transform .3s cubic-bezier(.225, .5, .165, 1)",
            "-ms-transition": "-ms-transform .3s cubic-bezier(.225, .5, .165, 1)",
            "transition": "transform .3s cubic-bezier(.225, .5, .165, 1)",
            "z-index": "0"
          });

          // if we are not going back, users expect scroll positions to be reset
          if (!back) {
            next.scrollTop(0);
          }

          // trigger
          setTimeout(function() {
            next.css({
              "-webkit-transform": "translate3d(0,0,0)",
              "-moz-transform": "translate3d(0,0,0)",
              "-ms-transform": "translate3d(0,0,0)",
              "transform": "translate3d(0,0,0)"
            });
            if ($.os.ios) {
              next.css({
                "box-shadow": "0 0 30px rgba(0,0,0,1)" // the shadow in android causes issues
              });
            }
            current.css({
              "-webkit-transform": "translate3d(-50%,0,0)",
              "-moz-transform": "translate3d(-50%,0,0)",
              "-ms-transform": "translate3d(-50%,0,0)",
              "transform": "translate3d(-50%,0,0)"
            });
          }, 1);

          // cleanup (we add a little safetime)
          setTimeout(function() {
            next.css({
              "-webkit-transition": "none",
              "-moz-transition": "none",
              "-ms-transition": "none",
              "transition": "none"
            });
            current.hide().css({
              "-webkit-transform": "translate3d(0,0,0)",
              "-webkit-transition": "none",
              "-moz-transform": "translate3d(0,0,0)",
              "-moz-transition": "none",
              "-ms-transform": "translate3d(0,0,0)",
              "-ms-transition": "none",
              "transform": "translate3d(0,0,0)",
              "transition": "none",
              "z-index": "1"
            });
          }, wait + 100);
        }

        if (anim === "pushleft") {
          wait = 500;
          // setup
          next.css({
            "-webkit-transform": "translate3d(-50%,0,0)",
            "-webkit-transition": "-webkit-transform .5s cubic-bezier(.225, .5, .165, 1)",
            "-moz-transform": "translate3d(-50%,0,0)",
            "-moz-transition": "-webkit-moz .5s cubic-bezier(.225, .5, .165, 1)",
            "-ms-transform": "translate3d(-50%,0,0)",
            "-ms-transition": "-ms-transform .5s cubic-bezier(.225, .5, .165, 1)",
            "transform": "translate3d(-50%,0,0)",
            "transition": "transform .5s cubic-bezier(.225, .5, .165, 1)",
            "z-index": "1",
            "dispay": "block"
          });
          current.css({
            "z-index": "2",
            "-webkit-transition": "-webkit-transform .55s cubic-bezier(.225, .5, .3, 1), box-shadow .2s linear",
            "-moz-transition": "-moz-transform .55s cubic-bezier(.225, .5, .3, 1), box-shadow .2s linear",
            "-ms-transition": "-ms-transform .55s cubic-bezier(.225, .5, .3, 1), box-shadow .2s linear",
            "transition": "transform .55s cubic-bezier(.225, .5, .3, 1), box-shadow .2s linear"
          });

          // trigger
          setTimeout(function() {
            next.css({
              "-webkit-transform": "translate3d(0,0,0)",
              "-moz-transform": "translate3d(0,0,0)",
              "-ms-transform": "translate3d(0,0,0)",
              "transform": "translate3d(0,0,0)"
            });
            current.css({
              "-webkit-transform": "translate3d(100%,0,0)",
              "-moz-transform": "translate3d(100%,0,0)",
              "-ms-transform": "translate3d(100%,0,0)",
              "transform": "translate3d(100%,0,0)",
              "box-shadow": "0 0 0 rgba(0,0,0,0)"
            });
          }, 1);

          // cleanup (we add a little safetime)
          setTimeout(function() {
            next.css({
              "-webkit-transition": "none",
              "-moz-transition": "none",
              "-ms-transition": "none",
              "transition": "none",
              "z-index": "1"
            });
            current.hide().css({
              "-webkit-transform": "translate3d(0,0,0)",
              "-webkit-transition": "none",
              "-moz-transform": "translate3d(0,0,0)",
              "-moz-transition": "none",
              "-ms-transform": "translate3d(0,0,0)",
              "-ms-transition": "none",
              "transform": "translate3d(0,0,0)",
              "transition": "none"
            });
          }, wait + 300);
        }

        if (anim === "fade") {
          wait = 500;
          // setup
          next.css({
            "z-index": "0",
            "-webkit-transition": "none",
            "-moz-transition": "none",
            "-ms-transition": "none",
            "transition": "none"
          });
          current.css({
            "z-index": "50",
            "-webkit-transition": "opacity .55s",
            "-moz-transition": "opacity .55s",
            "-ms-transition": "opacity .55s",
            "transition": "opacity .55s"
          });

          // trigger
          setTimeout(function() {
            current.css({
              "opacity": "0"
            });
          }, 1);

          // cleanup (we add a little safetime)
          setTimeout(function() {
            next.css({
              "z-index": "1",
              "-webkit-transition": "none",
              "-moz-transition": "none",
              "-ms-transition": "none",
              "transition": "none"
            });
            current.hide().css({
              "opacity": "1",
              "-webkit-transform": "translate3d(0,0,0)",
              "-webkit-transition": "none",
              "-moz-transform": "translate3d(0,0,0)",
              "-moz-transition": "none",
              "-ms-transform": "translate3d(0,0,0)",
              "-ms-transition": "none",
              "transform": "translate3d(0,0,0)",
              "transition": "none",
              "z-index": "0"
            });
          }, wait + 300);
        }

        // end of animations. do cleanup and set new headers

        setTimeout(function() {
          if (!isInit) current.hide().removeClass("active");
          next.addClass("active");

          // fix scrolling positions

          if (back != true) {
            next.scrollTop(0);
            // some issue with ios7 demands that we scroll top the body too.. they are adding weird scrolls to fix the status bar
            $("body, #content, #fnApp").scrollTop(0);
          }

          furnax.unlockUi();
        }, wait + 200); // we must add the safe extra wait here too

        // check if we selected a tabbar item and clear history
        var nextIsTabbarItem = false;

        $("#tabbar a").each(function() {
          if ("#" + next.attr("id") === $(this).attr("href")) {
            $("#tabbar a").removeClass("selected");
            $(this).addClass("selected");
            nextIsTabbarItem = true;
            tempDb.setItem("prev", ",");
          }
        });

        // header (replace)
        if (next.find(".view-navbar").length > 0) {
          $("#navbar").html(next.find(".view-navbar").html());
          furnax.showNavbar();
        } else if (nextIsTabbarItem) {
          $("#navbar").html('<h1>' + next.data("title") + '</h1>');
        } else {
          var backbutton = isInit ? '' : '<a class="backbutton"></a>';
          $("#navbar").html(backbutton + '<h1>' + next.data("title") + '</h1>');
        }

        // show/hide tabbar
        if (next.data("tabbar") != "no" && next.data("tabbar") !== "none") {
          $("#tabbar").show();
          $("#toggleMenu").show();
        } else {
          $("#tabbar").hide();
          $("#toggleMenu").hide();
          next.css({
            "padding-bottom": 0
          });
        }

        // LEGACY execute onload/onleave functions. functions on global space and under furnax are callable and the argument passed is always the view itself.
        setTimeout(function() {
          if (next.data("load")) {
            if (window[next.data("load")]) window[next.data("load")](next);
            if (window["furnax"][next.data("load")]) window["furnax"][next.data("load").split(".")[1]](next);
            if (window["app"][next.data("load")]) window["app"][next.data("load").split(".")[1]](next);
            // if the function is in furnax object, we need to convert furnax.function to window[furnax][function]
          }
        }, wait);

        if (next.data("willappear")) {
          if (window[next.data("load")]) window[next.data("load")](next);
          if (window["furnax"][next.data("load")]) window["furnax"][next.data("load").split(".")[1]](next);
          if (window["app"][next.data("load")]) window["app"][next.data("load").split(".")[1]](next);
          // if the function is in furnax object, we need to convert furnax.function to window[furnax][function]
        }

        if (current.data("unload") && !isInit) {
          setTimeout(function() {
            if (window[current.data("unload")]) window[current.data("unload")](current);
            if (window["furnax"][current.data("unload")]) window["furnax"][current.data("unload").split(".")[1]](current);
            if (window["app"][current.data("unload")]) window["app"][current.data("unload").split(".")[1]](current);
          }, wait);
        }

        // UPDATED NAMES execute onload/onleave functions. functions on global space and under furnax are callable and the argument passed is always the view itself.

        if (next.data("onload")) {
          if (window[next.data("onload")]) window[next.data("onload")](next);
          if (next.data("onload").split(".")[1] && window["furnax"][next.data("onload").split(".")[1]]) window["furnax"][next.data("onload").split(".")[1]](next);
          if (next.data("onload").split(".")[1] && window["app"][next.data("onload").split(".")[1]]) window["app"][next.data("onload").split(".")[1]](next);
          // if the function is in furnax object, we need to convert furnax.function to window[furnax][function]
        }

        if (current.data("onleave") && !isInit) {
          setTimeout(function() {
            if (window[current.data("onleave")]) window[current.data("onleave")](current);
            if (current.data("onleave").split(".")[1] && window["furnax"][current.data("onleave").split(".")[1]]) window["furnax"][current.data("onleave").split(".")[1]](current);
            if (current.data("onleave").split(".")[1] && window["app"][current.data("onleave").split(".")[1]]) window["app"][current.data("onleave").split(".")[1]](current);
          }, wait);
        }

        // restored delayed class reference when leaving the current view

        if (!isInit) {
          setTimeout(function() {
            current.find(".ex-delayed").removeClass("ex-delayed").addClass("delayed");
          }, 500);
        } else {
          current.find(".ex-delayed").removeClass("ex-delayed").addClass("delayed");
        }

        // clear previous timers
        if (delayTimers.length > 0) {
          for (var i = 0; i < delayTimers.length; i++) {
            clearTimeout(delayTimers[i]);
          }
        }

        // show objectes delayed with class delayed-X where X is seconds
        var delayedObjects = next.find('.delayed');
        delayTimers = []; // save to array to clear later
        if (delayedObjects.length > 0) {
          delayedObjects.each(function() {
            var myclasses = $(this).attr("class").split(" ");
            var delay = 1000;
            // finding the delayed- class containing the delay value we need
            for (var j = 0; j < myclasses.length; j++) {
              if (myclasses[j].match("delayed-")) {
                var delayClass = myclasses[j];
                delay = delayClass.replace("delayed-", "");
              }
            }
            var that = $(this);
            var t1 = setTimeout(function() {
              that.removeClass("delayed").addClass("ex-delayed");
            }, delay);
            delayTimers.push(t1);
          });
        }

        // lazyload for images and videos
        next.find("img, source").each(function() {
          if ($(this).data("src") && !$(this).attr("src")) {
            this.src = $(this).data("src");
            $(this).parents("video").each(function() {
              this.load();
            });
          }
        });

        // stop all videos playing in the view
        current.find("video").each(function() {
          $(this)[0].pause();
        });

        // re-enable histrory popstate, disabled for first load to avoid webkit popping for no reason. also, the thing must be delayed.
        setTimeout(function() {
          furnax.popStoryEnabled = true
        }, 200);

      } else { // end if next !== current
        furnax.unlockUi();
      }

      if (typeof ga == 'function') { // allow tracking from google analytics
        ga('send', 'pageview', '/#' + next.attr("id"));
      }

    }, // end load function

    goBack: function() {
      var myHistory = tempDb.getItem("prev").split(",");
      var to = "";
      if (myHistory != "") {
        to = "#" + document.getElementById(myHistory[myHistory.length - 1]).id;
      } else {
        to = "#" + $(".view").first().attr("id");
      }
      furnax.load(to, "pushleft", true);
      myHistory.pop();
      tempDb.setItem("prev", myHistory.toString());
    },

    popup: function(prop) {

      if (furnax.popupVisible == "undefined" || !furnax.popupVisible) {

        var title, message, cancelText, doneText, cancelOnly, doneOnly, doneCallback, cancelCallback;
        furnax.showingTooltip = true;

        if (prop) {
          // default settings
          title = prop.title || furnax.texts.popupTitle;
          message = prop.message || furnax.texts.popupMessage;
          cancelText = prop.cancelText || furnax.texts.cancelText;
          doneText = prop.doneText || furnax.texts.doneText;
          cancelOnly = prop.cancelOnly || false;
          doneOnly = prop.doneOnly || false;
          doneCallback = prop.doneCallback || function() {};
          cancelCallback = prop.cancelCallback || function() {};

        } // end if prop

        // set contents
        $(".furnax-popup header").html(title);
        $(".furnax-popup .popup-contents").html(message);
        $(".furnax-popup #cancel").html(cancelText);
        $(".furnax-popup #action").html(doneText);

        if (cancelOnly) {
          $(".furnax-popup #cancel").show().addClass("center");
          $(".furnax-popup #action").hide().removeClass("center");
        } else if (doneOnly) {
          $(".furnax-popup #cancel").hide().removeClass("center");
          $(".furnax-popup #action").show().addClass("center");
        } else {
          $(".furnax-popup #cancel").show().removeClass("center");
          $(".furnax-popup #action").show().removeClass("center");
        }

        // link actions
        $(".furnax-popup footer a").off("tap").on("tap", function(e) {

          if ($(this).attr("id") === "cancel") {
            cancelCallback();
          }

          if ($(this).attr("id") === "action") {
            doneCallback();
          }

          furnax.hideMask();
        });

        // center
        $(".furnax-popup").css("top", ($(window).height() / 2 - 120));

        // show with animation
        furnax.showMask();

        // tell the app we are already showing a popup.. no queing just now, successive popups are annihilited
        furnax.popupVisible = true;
      }
    },

    updateBadge: function(badgeContainer, badgeText) {
      $(badgeContainer).find(".badge").show();
      $(badgeContainer).find(".badge").text(badgeText);
    },
    removeBadge: function(badgeContainer) {
      $(badgeContainer).find(".badge").hide();
    },

    showNavbar: function() {
      $("#navbar").show().removeClass("fadeOutUp").addClass("fast-animated fadeInDown");
    },
    hideNavbar: function() {
      $("#navbar").removeClass("fadeInDown").addClass("fast-animated fadeOutUp");
    },

    lockUi: function() {
      $("#uiblock").show();
    },

    unlockUi: function() {
      $("#uiblock").hide();
    },

    showMask: function() {
      $("#loading-container").hide(); /* in case the popup arrives when the loading is shown, we hide it */
      $(".furnax-popup").css({
        "display": "block"
      });
      setTimeout(function() {
        $(".furnax-popup").css({
          "opacity": "1",
          "-webkit-transform": "scale(1)",
          "-moz-transform": "scale(1)",
          "-ms-transform": "scale(1)",
          "transform": "scale(1)"
        });
      }, 1);
      setTimeout(function() {
        $("#content").addClass("loading");
        $("#mask").css({
          "display": "block"
        }).css({
          "opacity": ".5"
        });
      }, 0);
    },
    hideMask: function() {
      $(".furnax-popup").css({
        "-webkit-transform": "scale(0.75)",
        "-moz-transform": "scale(0.75)",
        "-ms-transform": "scale(0.75)",
        "transform": "scale(0.75)",
        "opacity": "0"
      });
      $("#mask").css({
        "opacity": "0"
      });
      $("#content").removeClass("loading");
      setTimeout(function() {
        // restore initial states
        $("#content").css({
          "-webkit-transition": "none",
          "-moz-transition": "none",
          "-ms-transition": "none",
          "transition": "none"
        });
        // if the loading is visible, it will still need the mask, so
        if ($("#loading-container").css('display') !== 'block') {
          $("#mask").hide();
        }
        $(".furnax-popup").css({
          "display": "none",
          "-webkit-transform": "scale(1.25)",
          "-moz-transform": "scale(1.25)",
          "-ms-transform": "scale(1.25)",
          "transform": "scale(1.25)"
        });
        furnax.popupVisible = false;
        furnax.showingTooltip = false;
      }, 500);
    },
    showLoading: function() {
      $("#content").addClass("loading");
      $("#mask").css({
        "display": "block",
        "opacity": ".5"
      });
      $("#loading-container").css({
        "display": "block",
        "opacity": ".9"
      });
    },
    hideLoading: function() {
      $("#content").removeClass("loading");
      $("#mask").css({
        "opacity": "0"
      });
      $("#loading-container").css({
        "opacity": "0"
      });
      setTimeout(function() {
        $("#content").css({
          "-webkit-transition": "none",
          "-moz-transition": "none",
          "-ms-transition": "none",
          "transition": "none"
        });
        $("#mask").hide();
        $("#loading-container").hide();
      }, 400);
    },
    sideMenu: function() {

      $("html").addClass("sidemenu");
      $("body").append('<div id="toggleMenu"><span></span><span></span></div>');
      $("body").on('mouseup touchend', '#toggleMenu', function(e) {
        document.activeElement.blur();
        $('body').removeClass('with-keyboard');
        $("html").toggleClass("openmenu");
        e.stopPropagation();
        e.stopImmediatePropagation();
        return false;
      });
      $("#fnApp").on("tap", function(e) {
        if ($("html").hasClass("openmenu")) {
          $("html").removeClass("openmenu");
          e.stopPropagation();
          e.stopImmediatePropagation();
          return false;
        }
      });
    },
    checkScrolls: function() {
      console.log("body: " + document.body.scrollTop);
      console.log("fnApp: " + document.getElementById("fnApp").scrollTop);
      console.log("content: " + document.getElementById("content").scrollTop);
      console.log("active view: " + $(".view.active").scrollTop());
    },
    resetScrolls: function() {
      document.body.scrollTop = 0;
      document.getElementById("fnApp").scrollTop = 0;
      document.getElementById("content").scrollTop = 0;
      $(".view.active")[0].scrollTop = 0;
    },
    getAndroidVersion: function(ua) {
      ua = ua || navigator.userAgent;
      var match = ua.match(/Android\s([0-9\.]*)/);
      return match ? match[1] : false;
    },
    showInstallPrompt: function() {
      if (($.os.ipad || $.os.iphone) && window.navigator.standalone === false && (!window.cordova || !window.PhoneGap || !window.phonegap)) {
        furnax.popup({
          title: furnax.texts.install.title,
          message: '<p class="install-step">' + furnax.texts.install.step1 + '</p><p class="install-step">' + furnax.texts.install.step2 + '</p><p class="install-step">' + furnax.texts.install.step3 + '</p>',
          cancelText: "Ok",
          cancelOnly: true
        });
      }
    },
    makeSafeForCSS: function(name) {
      return name.replace(/[^a-z0-9]/g, function(s) {
        var c = s.charCodeAt(0);
        if (c == 32) return '-';
        if (c >= 65 && c <= 90) return '_' + s.toLowerCase();
        return '__' + ('000' + c.toString(16)).slice(-4);
      });
    },
    alertErrors: function() {
      window.onerror = function(msg, url, linenumber) {
        alert('Error message: ' + msg + '<br>FILE: ' + url + '<br>Line Number: ' + linenumber);
        return true;
      }
    },
    executeFunctionByName: function(functionName, context, args) {
      var namespaces = functionName.split(".");
      var func = namespaces.pop();
      for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
      }
      if (args) {
        var args = [].slice.call(arguments).splice(2);
        return context[func].apply(this, args);
      } else {
        return context[func].apply(this);
      }
    },
    transitionEnd: (function() {
      var i,
        undefined,
        el = document.createElement('div'),
        transitions = {
          'transition': 'transitionend',
          'MozTransition': 'transitionend',
          'WebkitTransition': 'webkitTransitionEnd'
        };
      for (i in transitions) {
        if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
          return transitions[i];
        }
      }
    })(),
    animationEnd: (function() {
      var i,
        undefined,
        el = document.createElement('div'),
        animations = {
          'animation': 'animationend',
          'MozAnimation': 'animationend',
          'WebkitAnimation': 'webkitAnimationEnd'
        };
      for (i in animations) {
        if (animations.hasOwnProperty(i) && el.style[i] !== undefined) {
          return animations[i];
        }
      }
    })(),
    preloadImages: function(items, callback) {
      if (!items) {
        return;
      }
      if ("undefined" === items.length) {
        // if single item make it array
        items = [items];
      }
      var count = items.length;
      for (var i = 0; i < count; i++) {
        var onLoad = function(e) {
          e.target.removeEventListener("load", onLoad);
          count--;
          if (0 == count) {
            callback(items);
          }
        }
        var img = new Image();
        img.addEventListener("load", onLoad, false);
        img.src = items[i];
      }
    },
    hideSplash: function() {
      $("#furnax_splashscreen img").addClass("animated fadeOutUp");
      $("#furnax_splashscreen").addClass("animated fadeOut");
      setTimeout(function() {
        $("#furnax_splashscreen").remove()
      }, 1000);
    },
    deleteHistory: function() {
      sessionStorage.setItem("prev", "");
    },
    popStoryEnabled: false,
    randomInt: function(min, max) {
      // better randoms
      return ~~(Math.random() * (max - min + 1)) + min;
    },
    validateEmail: function(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    init: function(options) {

        // later on, we need to know if we are using windows
        if (navigator.userAgent.toLowerCase().indexOf("windows phone") > -1 || navigator.userAgent.match(/MSIE/) && $.os.phone) {
          $.os.winphone = true;
        } else {
          $.os.winphone = false;
        }

        if (window.furnax && window.furnax.inited) {
          // app is already inited
          console.log("Furnax already loaded");
          return;
        }
        window.verbose = false;
        window.debug = false;
        window.db = window.localStorage;
        window.tempDb = window.sessionStorage;
        window.delayTimers = [];

        // use the correct localization file
        furnax.texts = (function() {
          switch (furnax.locale.toLowerCase()) {
            case "en-us":
              return localStrings.furnaxEn;
            case "it-it":
              return localStrings.furnaxIt;
            default:
              return localStrings.furnaxEn;
          }
        })();
        /*
         * append modal masks and popups
         */

        $("#fnApp").append('<div id="mask" class="scrollable"></div><div id="uiblock"></div>');
        $("#fnApp").append('<div id="loading-container"><div id="furnax-loading"><svg version="1.1" class="svg-loader" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve" > <path fill="#D43B11" d="M10,40c0,0,0-0.4,0-1.1c0-0.3,0-0.8,0-1.3c0-0.3,0-0.5,0-0.8c0-0.3,0.1-0.6,0.1-0.9c0.1-0.6,0.1-1.4,0.2-2.1 c0.2-0.8,0.3-1.6,0.5-2.5c0.2-0.9,0.6-1.8,0.8-2.8c0.3-1,0.8-1.9,1.2-3c0.5-1,1.1-2,1.7-3.1c0.7-1,1.4-2.1,2.2-3.1 c1.6-2.1,3.7-3.9,6-5.6c2.3-1.7,5-3,7.9-4.1c0.7-0.2,1.5-0.4,2.2-0.7c0.7-0.3,1.5-0.3,2.3-0.5c0.8-0.2,1.5-0.3,2.3-0.4l1.2-0.1 l0.6-0.1l0.3,0l0.1,0l0.1,0l0,0c0.1,0-0.1,0,0.1,0c1.5,0,2.9-0.1,4.5,0.2c0.8,0.1,1.6,0.1,2.4,0.3c0.8,0.2,1.5,0.3,2.3,0.5 c3,0.8,5.9,2,8.5,3.6c2.6,1.6,4.9,3.4,6.8,5.4c1,1,1.8,2.1,2.7,3.1c0.8,1.1,1.5,2.1,2.1,3.2c0.6,1.1,1.2,2.1,1.6,3.1 c0.4,1,0.9,2,1.2,3c0.3,1,0.6,1.9,0.8,2.7c0.2,0.9,0.3,1.6,0.5,2.4c0.1,0.4,0.1,0.7,0.2,1c0,0.3,0.1,0.6,0.1,0.9 c0.1,0.6,0.1,1,0.1,1.4C74,39.6,74,40,74,40c0.2,2.2-1.5,4.1-3.7,4.3s-4.1-1.5-4.3-3.7c0-0.1,0-0.2,0-0.3l0-0.4c0,0,0-0.3,0-0.9 c0-0.3,0-0.7,0-1.1c0-0.2,0-0.5,0-0.7c0-0.2-0.1-0.5-0.1-0.8c-0.1-0.6-0.1-1.2-0.2-1.9c-0.1-0.7-0.3-1.4-0.4-2.2 c-0.2-0.8-0.5-1.6-0.7-2.4c-0.3-0.8-0.7-1.7-1.1-2.6c-0.5-0.9-0.9-1.8-1.5-2.7c-0.6-0.9-1.2-1.8-1.9-2.7c-1.4-1.8-3.2-3.4-5.2-4.9 c-2-1.5-4.4-2.7-6.9-3.6c-0.6-0.2-1.3-0.4-1.9-0.6c-0.7-0.2-1.3-0.3-1.9-0.4c-1.2-0.3-2.8-0.4-4.2-0.5l-2,0c-0.7,0-1.4,0.1-2.1,0.1 c-0.7,0.1-1.4,0.1-2,0.3c-0.7,0.1-1.3,0.3-2,0.4c-2.6,0.7-5.2,1.7-7.5,3.1c-2.2,1.4-4.3,2.9-6,4.7c-0.9,0.8-1.6,1.8-2.4,2.7 c-0.7,0.9-1.3,1.9-1.9,2.8c-0.5,1-1,1.9-1.4,2.8c-0.4,0.9-0.8,1.8-1,2.6c-0.3,0.9-0.5,1.6-0.7,2.4c-0.2,0.7-0.3,1.4-0.4,2.1 c-0.1,0.3-0.1,0.6-0.2,0.9c0,0.3-0.1,0.6-0.1,0.8c0,0.5-0.1,0.9-0.1,1.3C10,39.6,10,40,10,40z" > <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 40 40" to="360 40 40" dur="0.8s" repeatCount="indefinite" /> </path> <path fill="#D43B11" d="M62,40.1c0,0,0,0.2-0.1,0.7c0,0.2,0,0.5-0.1,0.8c0,0.2,0,0.3,0,0.5c0,0.2-0.1,0.4-0.1,0.7 c-0.1,0.5-0.2,1-0.3,1.6c-0.2,0.5-0.3,1.1-0.5,1.8c-0.2,0.6-0.5,1.3-0.7,1.9c-0.3,0.7-0.7,1.3-1,2.1c-0.4,0.7-0.9,1.4-1.4,2.1 c-0.5,0.7-1.1,1.4-1.7,2c-1.2,1.3-2.7,2.5-4.4,3.6c-1.7,1-3.6,1.8-5.5,2.4c-2,0.5-4,0.7-6.2,0.7c-1.9-0.1-4.1-0.4-6-1.1 c-1.9-0.7-3.7-1.5-5.2-2.6c-1.5-1.1-2.9-2.3-4-3.7c-0.6-0.6-1-1.4-1.5-2c-0.4-0.7-0.8-1.4-1.2-2c-0.3-0.7-0.6-1.3-0.8-2 c-0.2-0.6-0.4-1.2-0.6-1.8c-0.1-0.6-0.3-1.1-0.4-1.6c-0.1-0.5-0.1-1-0.2-1.4c-0.1-0.9-0.1-1.5-0.1-2c0-0.5,0-0.7,0-0.7 s0,0.2,0.1,0.7c0.1,0.5,0,1.1,0.2,2c0.1,0.4,0.2,0.9,0.3,1.4c0.1,0.5,0.3,1,0.5,1.6c0.2,0.6,0.4,1.1,0.7,1.8 c0.3,0.6,0.6,1.2,0.9,1.9c0.4,0.6,0.8,1.3,1.2,1.9c0.5,0.6,1,1.3,1.6,1.8c1.1,1.2,2.5,2.3,4,3.2c1.5,0.9,3.2,1.6,5,2.1 c1.8,0.5,3.6,0.6,5.6,0.6c1.8-0.1,3.7-0.4,5.4-1c1.7-0.6,3.3-1.4,4.7-2.4c1.4-1,2.6-2.1,3.6-3.3c0.5-0.6,0.9-1.2,1.3-1.8 c0.4-0.6,0.7-1.2,1-1.8c0.3-0.6,0.6-1.2,0.8-1.8c0.2-0.6,0.4-1.1,0.5-1.7c0.1-0.5,0.2-1,0.3-1.5c0.1-0.4,0.1-0.8,0.1-1.2 c0-0.2,0-0.4,0.1-0.5c0-0.2,0-0.4,0-0.5c0-0.3,0-0.6,0-0.8c0-0.5,0-0.7,0-0.7c0-1.1,0.9-2,2-2s2,0.9,2,2C62,40,62,40.1,62,40.1z" > <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 40 40" to="-360 40 40" dur="0.6s" repeatCount="indefinite" /> </path> </svg></div></div>');
        $("#fnApp").append('<div class="furnax-popup"><header>Message</header><div class="popup-contents">ciao</div><footer><a class="button center" id="cancel">' + furnax.texts.cancelText + '</a><a class="button" id="action" style="display: none;">' + furnax.texts.doneText + '</a></footer></div>');

        // call init on document ready to furnaxize the app
        $.Event('furnaxready', {
          bubbles: true
        });
        // options

        if (options) {
          if (options.resetLocation) furnax.resetLocations = true;
          if (options.alertErrors) furnax.alertErrors();
          if (options.sideMenu) furnax.sideMenu();
          if (options.autoHideSplash == "undefined" || options.autoHideSplash == true) { // default behavior is auto hide splash
            furnax.autoHideSplash = true;
          } else {
            furnax.autoHideSplash = false;
          }
        }

        // normalizing events
        furnax.touchStartEvent = document.ontouchstart !== null ? 'mousedown' : 'touchstart';

        // stealing alerts
        window.alert = function(m) {
          furnax.popup({
            message: m,
            doneOnly: true
          });
        };

        /*
         * our contents height
         */

        $("#content, #furnax_splashscreen").height(window.availHeight || window.innerHeight);

        var resizeTimer;
        $(window).on("orientationchange resize", function() {
          clearInterval(resizeTimer);
          resizeTimer = setInterval(function() {
            $("#content").height(window.availHeight || window.innerHeight);
          }, 1000);
        });

        // adding windows classes to body
        if ($.os.winphone && $.os.app) {
          $("#content").height(window.innerHeight - 20);
        }

        // Converting Mouse events to touch events
        if (!$.os.phone && !$.os.tablet) {
          $(document).on("click", function(e) {
            if (e.type == 'click') {
              $(e.target).trigger('tap');
              e.stopImmediatePropagation();
              //            e.preventDefault();
            }
          });
        }

        // add a class to tabbar items
        if (!$("html").hasClass("sidemenu")) {
          $("#tabbar a").addClass("tabBarButton");
        }

        // stealing all links
        $("body").on("tap longTap singleTap click singleClick", '#fnApp a:not(.do-not-steal):not(.tabBarButton):not([target="_blank"]):not([target="_system"])', function(e) {
          console.log(e.type);

          //        if (dest.attr("target") != "undefined") {
          //          return;
          //        }

          var dest = $(this).attr("href"); // http://maps.apple.com is used to open apple maps
          if (e.defaultPrevented || !(dest !== "undefined" && dest !== "#" && dest.indexOf("javascript:") == "-1" && dest !== "" && dest.indexOf("tel:") == "-1" && dest.indexOf("mailto:") == "-1" && dest.indexOf("http://maps.apple.com") == "-1" && dest.indexOf("callto:") == "-1" && dest)) {

            //        console.log("not blocked a " + e.type + " on " + e.currentTarget);
            return;
          } else {
            e.preventDefault();
          }
        });

        $("body").on("tap", '#fnApp a:not(.do-not-steal):not(.backbutton):not(.tabBarButton):not([target="_blank"]):not([target="_system"])', function(e) {

          var animation = "pushright",
            referral = $(this);

          if (referral.hasClass("animation-pushright")) animation = "pushright";
          if (referral.hasClass("animation-pushleft")) animation = "pushleft";
          if (referral.hasClass("animation-fade")) animation = "fade";
          if (referral.hasClass("animation-none")) animation = "none";

          var dest = referral.attr("href");

          if (referral.attr("target")) {

            if (cordova.InAppBrowser) {
              cordova.InAppBrowser.open(dest, referral.attr("target"), 'location=yes');
              return;
            } else {
              window.open(dest, referral.attr("target"));
            }

          }

          if (dest.indexOf("callto:") == 0 ||
            dest.indexOf("mailto:") == 0 ||
            dest.indexOf("tel:") == 0 ||
            dest.indexOf("http://maps.apple.com") == 0 ||
            dest.indexOf("javascript:") == 0
          ) {

            return;

          }

          if (dest !== "undefined" && dest !== "#" && dest.indexOf("javascript:") == "-1" && dest !== "" && dest.indexOf("tel:") == "-1" && dest.indexOf("mailto:") == "-1" && dest) {
            furnax.lockUi();
            // check if valid
            if (dest.indexOf("http://") == "-1" && dest.indexOf("https://") == "-1") {
              // if not absolute load the view
              furnax.load(dest, animation);
            } else if (dest.indexOf("http://") !== "-1" || dest.indexOf("https://") !== "-1") {
              // if absolute we load the content in a new view and store for later access
              var randomId = "external-" + (Math.random() + 1).toString(36).substring(2, 12);
              var whenload = referral.data("onload") || "";
              var tabbar = referral.data("tabbar") == "none" ? 'data-tabbar="none"' : '';
              // we probably should add something for unload too
              var title = referral.data("title") || "";
              var filterElement = referral.data("filter");
              if (filterElement) {
                filterElement = ' ' + filterElement;
              } else {
                filterElement = "";
              }
              var contents = "";
              $("#content").append('<div class="view" id="' + randomId + '" data-load="' + whenload + '"' + tabbar + ' data-title=' + title + '></div>');

              // get the stuff. if data-filter="css-selector" is set in the link, we will only use thats

              furnax.showLoading();

              if (filterElement !== "" && filterElement !== "undefined") {

                var destProxied = 'http://fornacestudio.com/remote.php?url=' + dest + '&mode=native';

                $("#" + randomId).load(destProxied + filterElement, function(data, status, xhr) {
                  referral.attr("href", "#" + randomId);
                  $("#" + randomId).append('<header class="view-navbar"><a class="backbutton"></a><h1>' + title + '</h1></header>');
                  // if its an entire page and no element is provided, we will load it in a iframe

                  furnax.load("#" + randomId, animation);
                  furnax.hideLoading();
                });

              } else {

                $("#" + randomId).html('<iframe src="' + dest + '" style="border:none;width:100%;height:100%;overflow:scroll"></iframe>');
                furnax.load("#" + randomId, animation);
                furnax.hideLoading();
              }

              // next time it will not have to load external
            } else {
              furnax.unlockUi();
              alert("Url not valid");
            }
          } else {
            return false;
            // was not a link
          }
        });

        $("body").on(furnax.touchStartEvent, ".tabBarButton", function(e) {
          dest = $(this).attr("href");
          $(this).addClass("active");
          $(".tabBarButton").not(this).removeClass("active");
          // console.log(dest);
          if (dest !== "undefined" && dest !== "#" && dest.indexOf("javascript:") == "-1" && dest !== "" && dest.indexOf("tel:") == "-1" && dest.indexOf("mailto:") == "-1" && dest) {
            // we give it a tic to update the classes
            setTimeout(function() {
              furnax.load(dest, "none", false);
            }, 0);
            e.stopPropagation();
            e.stopImmediatePropagation();
            furnax.popStoryEnabled = false;
            // absurdely, android is firing a random popstate...
            setTimeout(function() {
              furnax.popStoryEnabled = true
            }, 2000);
            return false;
          } else {
            console.log("Please check this tabbar url");
          }
        });

        $("body").on("tap", ".sidemenu #tabbar a", function(e) {
          dest = $(this).attr("href");
          $(this).addClass("active");
          $(".tabBarButton").not(this).removeClass("active");
          if (dest !== "undefined" && dest !== "#" && dest.indexOf("javascript:") == "-1" && dest !== "" && dest.indexOf("tel:") == "-1" && dest.indexOf("mailto:") == "-1" && dest) {
            $("html").removeClass("openmenu");
            furnax.load(dest, "none");
            e.stopPropagation();
            return false;
          }
        });

        // back buttons
        $("body").on("touchstart mousedown", ".backbutton", function(e) {
          furnax.goBack();
          return false;
        });

        // set a general purpose ajax error

        $.ajaxSettings.error = function(xhr, errorType, error) {
          furnax.hideLoading();
          var url = "";
          if (xhr.url) {
            url = xhr.url;
          }
          setTimeout(function() {
            alert(furnax.texts.ajaxError);
            furnax.unlockUi();
          }, 500);
          if (verbose) console.log(xhr);
          if (verbose) console.log(errorType);
          if (verbose) console.log(error);
        };

        // global ajax rules. if the app needs the current number of requests, can check "furnax.ajaxRequests"

        $(document).on('ajaxStart', function(e, xhr, options) {
          $("html").addClass("waiting-data");
        });

        $(document).on('ajaxStop', function(e, xhr, options) {
          $("html").removeClass("waiting-data");
        });

        $(document).on('scroll', function(e) {
          $("#navbar").css({
            "top": "0"
          });
        });

        $(document).on("ajaxBeforeSend", function(e, xhr, options) {
          if (!furnax.ajaxRequests) {
            furnax.ajaxRequests = 1;
          } else {
            furnax.ajaxRequests++;
          }
        });

        $(document).on('ajaxComplete', function(e, xhr, options) {
          furnax.ajaxRequests--;
        });

        // adding ios classes to body

        if ($.os.ios) {
          $("html").addClass("ios");
        }
        /* add a class only if playing in the browser */
        if (($.os.ipad || $.os.iphone) && window.navigator.standalone === false && !$.browser.webview && (!window.cordova || !window.PhoneGap || !window.phonegap)) {
          $("html").addClass("inbrowser");
        } else {
          $.os.app = true;
        }

        if ($.os.ios) {
          var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
          ver = [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
          furnax.iosVersion = ver[0];
        }

        // adding windows classes to body
        if ($.os.winphone) {
          $("html").addClass("winphone");
        }

        // adding android class to body
        if ($.os.android) {
          $("html").addClass("android");
          $("html").addClass("androidversion" + furnax.getAndroidVersion().replace(/\./g, ""));
          $("html").addClass("androidversion" + furnax.getAndroidVersion().replace(/\./g, "").slice(0, -1));
          $("html").addClass("androidversion" + furnax.getAndroidVersion().replace(/\./g, "").slice(0, -2));
        }

        // if desktop version, we force allowing scroll for webviews
        if ($.os.phone !== true && $.os.tablet !== true) {
          $("head").append("<style> .view { overflow: scroll !important} </style> ");
        }

        // add header and footer
        $("#fnApp").append('<header id="navbar"></header>');

        /**
         * keyboard up fixes
         */

        // prevent scroll when keyboard is up. needed because of ios7 bug that moves the fixed header
        if ($.os.ios && !$.os.winphone) {
          document.addEventListener('touchmove', function(event) {
            if ($("body").hasClass("with-keyboard")) {
              var target = $(event.target);
              if (target.is("textarea")) {
                target.children().toggle();
              } else {
                //              event.preventDefault();
              }
            }
          });
        }

        $("input, textarea").not("[type=radio]").not("[type=checkbox]").not("[type=file]").not(".no-keyboard").focus(function() {
          console.log("FOCUS");
          //        $(".scrollable").addClass("keyboard");
          // for tablets and phones we add class to handle keyboard. when addeded, no scroll will be enabled
          if (($.os.phone || $.os.tablet) && !$.os.winphone) {
            $("body").addClass("with-keyboard");

          }
          // ios issue: header remains on top of window
          if ($.os.ios && !$.os.winphone) {
            setTimeout(function() {
              // if we want the header to be visible in iOs we need to deal with the bug that causes the header to move with the window
              $("#navbar").css("top", document.body.scrollTop);
              if (!$(this).hasClass('isfixed') && $(':focus').closest('div.scrollable'));
              $(':focus').closest('div.scrollable').scrollTop(0);
              try {
                $(':focus').closest('div.scrollable').scrollTop($(':focus').offset().top - 74);
              } catch (e) {}
            }, 1400);
          }
        });

        $("input, textarea").not("[type=radio]").not("[type=checkbox]").not("[type=file]").not(".no-keyboard").on('keyup', function() {
          console.log("KEYUP");

          // ios issue: header remains on top of window
          if ($.os.ios && !$.os.winphone) {
            setTimeout(function() {
              // if we want the header to be visible in iOs we need to deal with the bug that causes the header to move with the window
              $("#navbar").css("top", document.body.scrollTop);
            }, 300);
          }
        });

        $("input, textarea").not("[type=radio]").not("[type=checkbox]").not("[type=file]").not(".no-keyboard").blur(function(e) {
          console.log("BLUR");
          console.log(e);
          // we are moving the header in iOs7. to avoid it to appear lazy when focusing an upper element, we hide it temporarily
          if ($.os.ios && !$.os.winphone) {
            $("#navbar").css({
              "top": "0"
            });
            if (document.activeElement === document.body) {
              // we have a weird new thing, where the input receives focus with a delay from a tap in a point it will occupy after tap (ex: button is right below input)
              $("ul li input").css("pointer-events", "none");
              setTimeout(function() {
                $("ul li input").css("pointer-events", "auto");
              }, 150);
            }
          }
          var that = $(this);
          setTimeout(function() {
            if (document.activeElement === document.body) {

              // no other item is focused at the moment
              if ($.os.ios) {
                $("#navbar").css({
                  "top": "0"
                });
                $("body").scrollTo(0);
              }
              //        if ($.os.android) {
              //          setTimeout(function () {
              //            $(".view.active").css("padding-bottom", "44px");
              //          }, 300);
              //        }
              if ($.os.phone || $.os.tablet) {
                $("body").removeClass("with-keyboard");
                $(".scrollable").removeClass("keyboard");
                $(".scrollable").css('opacity', '0');
                setTimeout(function() {
                  $(".scrollable").css('opacity', '1');
                }, 50)
              }

              // ultra-weird: we need the following timer to have android SHOW the keyboard when focusing the first element. you would not expect any blur to be triggered when that happens. but hey.
              setTimeout(function() {
                // if pointer events none is set before focusing, android wont focus properly
                $("ul li input").css("pointer-events", "auto");
              }, 200);
            }
          }, 150);
        });

        /*
         * building tabs
         */

        if ($(".furnax-tabs .tab").length > 0) {

          $.Event('tabloaded');
          /* en event is fired when the tabs are loaded. it has the loaded tab unique class name as argument.
          example use: $("body").on("tabloaded", function(x, elem) { alert(elem) }); */

          $(".furnax-tabs").each(function() {
            var navelems = "";
            var tabno = 0;
            $(this).find(".tab").each(function() {
              tabno++;

              /* add unique class to target */
              var target = "furnax-tab-id-" + tabno;
              $(this).addClass(target);

              /* build the nav menu inside nav-elems string */
              var name = $(this).data("name");
              var icon = "";
              if ($(this).data("icon")) {
                icon = '<img class="tabs-nav-element-icon" src="' + $(this).data("icon") + '"/>';
              }
              navelems = navelems + '<div class="tabs-nav-element" data-target="' + target + '">' + icon + name + '</div>';
            });
            /* append the nav menu */
            $(this).prepend('<nav class="tabs-nav">' + navelems + '</nav>');

          });

          $("body").on("tap", ".furnax-tabs .tabs-nav-element", function() {
            $(this).siblings(".tabs-nav-element").addClass("not-selected").removeClass("selected");
            $(this).removeClass("not-selected").addClass("selected").closest(".furnax-tabs").find(".tab").removeClass("visible-tab").addClass("hidden-tab");
            $(this).closest(".furnax-tabs").find(".tab." + $(this).data("target")).show().removeClass("hidden-tab").addClass("visible-tab");
            $(document.body).trigger('tabloaded', $(this).data("target"));
            setTimeout(function() {
              $(".hidden-tab").hide()
            }, 500);
          });

        }

        /*
         * building minimal tabs
         */

        // select first tab, if none is specified
        $(".tab-container").each(function() {
          if ($(this).find("div.selected").length == 0) {
            $(this).find("div").first().addClass("selected");
          }
        });

        $("body").on("tap", ".tab-bar-nav li", function(e) {
          var tabContainer = $(this).parents(".tab-container"),
            navItems = tabContainer.find(".tab-bar-nav li"),
            tabs = tabContainer.children("div");

          navItems.removeClass("selected");
          $(this).addClass("selected");

          tabs.removeClass("fadeIn").addClass("animated fast-animated fadeOut").removeClass("selected");
          $("#" + $(this).attr("data-tab-id")).removeClass("fadeOut").addClass("fadeIn").addClass("selected");

          e.preventDefault;
          return false;
        });

        /*
         * reload app if there is an update available of the app cache manifest
         */
        if (window.applicationCache) {
          applicationCache.addEventListener('updateready', function() {
            furnax.popup({
              title: furnax.texts.update.title,
              message: furnax.texts.update.message,
              cancelText: furnax.texts.update.cancel,
              doneText: furnax.texts.update.confirm,
              doneCallback: function() {
                window.location.reload();
              }
            });
          });
        }

        // making sure we have an active view (and one only). if there is an hashtag, we will set active to that, unless the option resetLocations is used

        if (window.location.hash != "" && !furnax.resetLocations) {
          $(".view.active").removeClass("active");
          $(window.location.hash).addClass("active first");
        } else if ($(".view.active").length === 0) {
          $(".view").first().addClass("active first");
        } else if ($(".view.active").length > 1) {
          $(".view.active").removeClass("active").first().addClass("active first");
        }

        // pushing the first state in history
        // window.history.pushState(null, null, "#" + $(".view.first").attr("id"));
        tempDb.setItem("prev", "");

        // loading first view
        furnax.load("#" + $(".view.active").attr("id"), "none", false, true);

        // most times, the body won't scroll. but there's still a few situations in which this could happen. to allow the user to continue using the app, we are resetting the scroll of the body on every tap.
        $("body").not(".with-keyboard").not(".with-keyboard body").on("tap", function(e) {
          if (!$("body").hasClass("with-keyboard") && !$(e.target).is(':focus')) {
            $("body").scrollTo(0);
            $('input, textarea').blur();
          }
        });

        // we enable the back only after the user has tapped somewhere in the window. this way we circumvent android dropping random popstates.      
        $("body").on("tap", function() {
          furnax.popStoryEnabled = true;
        });
        $(window).on("load", function() {
          if (!$.os.android || !$.os.winphone) {
            setTimeout(function() {
              $(window).on("popstate", function() {
                // setting the behavior for popstate (history back with browser). also avoid initial popstate in webkit with a delay and to be sure we set furnax.popStoryEnabled to true the first time a view is loaded
                if (furnax.showingTooltip) {
                  furnax.hideMask();
                } else if (furnax.popStoryEnabled) {
                  furnax.goBack();
                }
              });
            }, 200);
          } else {
            // if android instead of the popstate we use backbutton
            document.addEventListener("backbutton", function(e) {
              if (furnax.showingTooltip) {
                furnax.hideMask();
              } else {
                furnax.goBack();
              }
            }, false);

          }
        });

        // wait a sec, then hide splashscreen and trigger furnaxready

        if (furnax.autoHideSplash) {
          setTimeout(function() {
            furnax.hideSplash();
          }, 2000);
        }
        furnax.inited = true;
        $(document.body).trigger('furnaxready');

      } // end init function
  }; // end furnax

})(window.jQuery || window.Zepto, window, document);
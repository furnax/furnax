/*

REQUIREMENTS:

npm install
npm install gulp-concat gulp-uglify gulp-notify gulp-util gulp-minify-css gulp-connect gulp-autoprefixer --save-dev

CD TO PROJECT FOLDER AND RUN WITH:
gulp

*/


var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');
var gutil = require('gulp-util');
var minifyCSS = require('gulp-minify-css');
var connect = require('gulp-connect');
var autoprefixer = require('gulp-autoprefixer');
//var hexuglify = require('gulp-hexuglify');

gulp.task('json', function () {
    return gulp.src([
          "../www/mycircle-data/**/*.json",
        ]) //  if orderd not important, 'js/**/*.js' to select all javascript files in js/ and subdirectory
        .pipe(gulp.dest('../www/build')) // the destination folder
});

gulp.task('js', function () {
    return gulp.src([
          "js/zepto.js",
          "js/furnax.js",
          "js/plugins.js",
          "js/app.js"
        ]) //  if orderd not important, 'js/**/*.js' to select all javascript files in js/ and subdirectory
        .pipe(concat('app.min.js')) // the name of the resulting file
        .pipe(uglify())
        .pipe(gulp.dest('../www/build')) // the destination folder
        .pipe(notify({ message: 'Finished minifying JavaScript'}));
});

gulp.task('css', function () {
    return gulp.src([
           "css/normalize.css",
           "css/furnax.css",
           "css/plugins.css",
           "css/furnicon.css",
           "css/app.css"
        ])
        .pipe(concat('app.min.css')) // the name of the resulting file
        .pipe(autoprefixer({
            browsers: ['last 8 versions'],
            cascade: true
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('../www/build')) // the destination folder
        .pipe(notify({ message: 'Finished minifying CSS'}))
        .pipe(connect.reload());
});

gulp.task('html', function () {
  gulp.src('../www/*.html')
    .pipe(connect.reload());
});

gulp.task('webserver', function() {
  connect.server({
    port: 8082,
    livereload: true,
    root: "../www/"
  })        
});


gulp.task('watch', function() {
    gulp.watch('../www/index.html', ['html']);
    gulp.watch('js/**/*.js', ['js']);
    gulp.watch('css/**/*.css', ['css']);
    gulp.watch('../www/**/*.json', ['json']);
});


gulp.task('default', ['json', 'js', 'css', 'html', 'watch', 'webserver']);


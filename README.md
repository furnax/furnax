# clonare la repo

Video how-to per sourcetree: https://cloudup.com/cDajKopxLQz
Sourcetree è un client git gratuito. Malauguratamente per qualche motivo richiede la registrazione di un account, per il resto è relativamente buono, semplice e completo.

# test cordova

Dopo aver clonato la repo, per prima cosa assicuriamoci che cordova sia ok. Da terminale, prima vai alla repo e poi digita `cordova`.
Se da command not found ci sono due soluzioni:
1. http://stackoverflow.com/questions/17975330/bash-cordova-command-not-found-or-bash-phonegap-command-not-found/19113963#19113963
2. chiudere e riaprire il terminale
3. aggiungere il path dove cordova è stato installato al file ~/.bash_profile ()

Nota: per andare velocemente alla repo puoi fare così:
Nel terminale digita `cd ` (con uno spazio dopo) e poi trascina la cartella della repo dal finder dentro al terminale

# gulp

Con il trucco di prima, apriamo un nuovo terminale e andiamo alla cartella "gulp". Qui puoi digitare `npm install` per inizializzare il progetto.
Verrà installato automaticamente gulp con tutte le dipendenze necessarie.
A questo punto possiamo lasciare questo terminale ad eseguire gulp in automatico, basta digitare `gulp`.
Gulp si occupa di unire tutti i file javascript e css in uno solo, comprimerli per caricamenti più rapidi e aggiungere tutti i prefissi richiesti dai browser per le proprietà css più recenti (in particolare, risulta utilissimo per usare i `flex`).

# css e javascript

Le cartelle con CSS e Javascript non concatenati e minificati si trovano nella cartella /gulp. Possono essere spostate in www, basta ricordarsi di cambiare anche il percorso dei file in /gulp/gulpfile.js.

# testare la app

Eseguendo `gulp`, appare in terminale un url locale tipo http://localhost:8082. Copialo e incollalo nel browser. Si aggiornerà automaticamente via via che aggiorni la app.
Sostituendo l'ip locale del computer a localhost, è possibile vedere in contemporanea l'anteprima su più dispositivi. Basta andare col cellulare su 192.168.1.XX:8082 e lasciare la finestra aperta per avere sempre una anteprima aggiornata.

# orrori

Se ci sono brutti errori di css, o javascript, gulp si inchioda e smette di aggiornare l'anteprima. Se succede occorre risolvere gli errori e poi eseguire nuovamente `gulp`.

# eseguire cordova e testare su telefono

Basta eseguire, da /www o dalla cartella principale, il comando `cordova build ios` o `cordova run android`. È necessario installare separatamente Android Studio e XCode.

# testare nel browser

Sia chrome che safari hanno una modalità che emula i dispositivi. Per safari occorre abilitare il menù sviluppo dalle preferenze > avanzate. Appare il menù sviluppo, che avrà la voce "Enter responsive design mode" (non so in italiano).
Per chrome basta cliccare col tasto destro, scegliere "ispeziona" e poi l'icona del telefonino nella barra alta dell'inspector.
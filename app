#!/usr/bin/php
<?php

while(true)
{
	$choice = menu("
I'm Martin, what do you want?
	1) Run IOS App
	2) Run Android App
	3) Build App (All)
	4) Generate Signed APK
");
	
	if($choice == "1")
	{
		$ios = menu("
	1) Run on Default Simulator
	2) Run on Other Simulator
	3) Run on Device
");
		if($ios == "1")
		{
			execute("cordova run ios");
		}elseif($ios == "3")
		{
			execute("cordova run ios --device");
		}elseif($ios == "2")
		{
			exec("cd " . dirname(__FILE__) . " && cordova run ios --list", $out);
			$devices = [];
			$start = false;
			foreach($out as $o){
				if($o == "Available ios virtual devices:")
				{
					$start = true;
					continue;
				}

				if($start)
				{
					$devices[] = (count($devices) + 1) . ") " . $o;
				}
			}
			$target = menu(implode("\n", $devices));

			$target = $devices[$target - 1];
			$target = explode(") ", $target);
			$target = $target[1];
			$target = str_replace(" ", "\\ ", $target);

			execute("cordova run ios --target=" . $target);
		}
	}elseif($choice == "2")
	{
		execute("cordova run android");
	}elseif($choice == "3")
	{
		execute("cordova build");
	}elseif($choice == "4")
	{
		execute("cordova build android --release");

		echo "APK path: " . dirname(__FILE__) . "/platforms/android/build/outputs/apk/";
	}else{
		echo "(=.=\")";
	}
}

function menu($str){
	echo <<<EOD
\033[31m
$str
\033[0m
EOD;
	return readline();
}

function execute($cmd) {
    $proc = proc_open($cmd, [['pipe','r'],['pipe','w'],['pipe','w']], $pipes, dirname(__FILE__));
    while(($line = fgets($pipes[1])) !== false) {
        fwrite(STDOUT,$line);
    }
    while(($line = fgets($pipes[2])) !== false) {
        fwrite(STDERR,$line);
    }
    fclose($pipes[0]);
    fclose($pipes[1]);
    fclose($pipes[2]);
    return proc_close($proc);
}